<?php

namespace App\Models\Popbox;

use Illuminate\Database\Eloquent\Model;

class WebsiteMerchant extends Model
{
    protected $connection = 'popbox_db';
    protected $table = 'tb_website_merchants';
}
