<?php

namespace App\Http\Controllers\Popbox;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Popbox\LockerActivitiesReturn;
use App\Models\Popbox\LockerLocation;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class ReturnController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $merchant = LockerActivitiesReturn::distinct()
        ->orderBy('merchant_name','asc')
        ->get(['merchant_name'])->pluck('merchant_name');
        unset($merchant[0]); // remove the blank (should I ?)
        
        $location = LockerActivitiesReturn::distinct()
        ->orderBy('locker_name','asc')
        ->get(['locker_name'])->pluck('locker_name');

        $status = LockerActivitiesReturn::distinct()
        ->orderBy('status','asc')
        ->get(['status'])->pluck('status');

        $data = [
            'merchants' => $merchant,
            'location' => $location,
            'status' => $status
        ];
        return view('popbox.return.list', $data);
    }

    public function getAjaxReturnActivity(Request $request)
    {
        $columns = array(
            0 => 'tracking_no',
            1 => 'merchant_name',
            2 => 'phone_number',
            3 => 'locker_name',
            4 => 'locker_size',
            5 => 'storetime',
            6 => 'taketime',
            7 => 'status',
            8 => 'groupName',
            9 => 'courier_name'
        );

        $tracking_no = $request->input('tracking_no', null);
        if(!empty($request->input('columns.0.search.value'))) {
            $tracking_no = $request->input('columns.0.search.value');
        }
        $merchant_name = $request->input('merchant_name', null);
        if(!empty($request->input('columns.1.search.value'))) {
            $merchant_name = $request->input('columns.1.search.value');
        }
        $phone_number = $request->input('phone', null);
        if(!empty($request->input('columns.2.search.value'))) {
            $phone_number = $request->input('columns.2.search.value');
        }
        $locker_name = $request->input('location', null);
        if(!empty($request->input('columns.3.search.value'))) {
            $locker_name = $request->input('columns.3.search.value');
        }
        $storetime = $request->input('storetime', null);
        if(!empty($request->input('columns.5.search.value'))) {
            $storetime = $request->input('columns.5.search.value');
        } else {
            $storetime = date('Y-m')."-01 - ".date('Y-m-d');
        }
        $status = $request->input('status', null);
        if(!empty($request->input('columns.7.search.value'))) {
            $status = $request->input('columns.7.search.value');
        }
        $groupName = $request->input('groupName', null);
        if(!empty($request->input('columns.8.search.value'))) {
            $groupName = $request->input('columns.8.search.value');
        }
        $courier_name = $request->input('courier_name', null);
        if(!empty($request->input('columns.9.search.value'))) {
            $courier_name = $request->input('columns.9.search.value');
        }

        $startDate = null; $endDate = null;
        if (!empty($storetime)) {
            $date = explode(" - ", $storetime);
            $startDate = $date[0].' 00:00:00';
            $endDate = $date[1].' 23:59:59';
        }

        $totalData = LockerActivitiesReturn::leftJoin('return_statuses','locker_activities_return.tracking_no','=','return_statuses.number')->count();

        if($request->input('length') == -1) {
            $limit = $totalData;
        } else {
            $limit = $request->input('length');
        }

        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')) && empty($tracking_no) && empty($merchant_name) && empty($phone_number) && empty($locker_name) && empty($storetime) && empty($status) && empty($groupName) && empty($courier_name)) {
            $posts = LockerActivitiesReturn::leftJoin('return_statuses','locker_activities_return.tracking_no','=','return_statuses.number')
            ->limit($limit)
            ->offset($start)
            ->orderBy($order, $dir)
            ->get();
            
            $totalFiltered = $totalData;
        } else {
            $search = $request->input('search.value');

            $posts = LockerActivitiesReturn::leftJoin('return_statuses','locker_activities_return.tracking_no','=','return_statuses.number')
            ->when($tracking_no, function ($query) use ($tracking_no){
                return $query->where('locker_activities_return.tracking_no','LIKE',"%$tracking_no%");
            })
            ->when($merchant_name, function ($query) use ($merchant_name){
                return $query->where('locker_activities_return.merchant_name','=',$merchant_name);
            })
            ->when($phone_number, function ($query) use ($phone_number){
                return $query->where('locker_activities_return.phone_number','LIKE',"%$phone_number%");
            })
            ->when($locker_name, function ($query) use ($locker_name){
                return $query->where('locker_activities_return.locker_name','LIKE',"%$locker_name%");
            })
            ->when($storetime, function ($query) use ($startDate, $endDate){
                return $query->whereBetween('locker_activities_return.storetime', [$startDate, $endDate]);
            })
            ->when($status, function ($query) use ($status){
                return $query->where('locker_activities_return.status','=',$status);
            })
            ->when($groupName, function ($query) use ($groupName){
                return $query->where('locker_activities_return.groupname','LIKE',"%$groupName%");
            })
            ->when($courier_name, function ($query) use ($courier_name){
                return $query->where('locker_activities_return.courier_name','LIKE',"%$courier_name%");
            })
            ->when($search, function ($query) use ($search){
                return $query->where('locker_activities_return.tracking_no', 'LIKE', '%'.$search.'%')
                ->orWhere('locker_activities_return.merchant_name', 'LIKE', '%'.$search.'%')
                ->orWhere('locker_activities_return.phone_number', 'LIKE', '%'.$search.'%')
                ->orWhere('locker_activities_return.locker_name', 'LIKE', '%'.$search.'%')
                ->orWhere('locker_activities_return.status', 'LIKE', '%'.$search.'%')
                ->orWhere('locker_activities_return.groupname', 'LIKE', '%'.$search.'%')
                ->orWhere('locker_activities_return.courier_name', 'LIKE', '%'.$search.'%');
            })
            ->limit($limit)
            ->offset($start)
            ->orderBy($order, $dir)
            ->get();

            $totalFiltered = LockerActivitiesReturn::leftJoin('return_statuses','locker_activities_return.tracking_no','=','return_statuses.number')
            ->when($tracking_no, function ($query) use ($tracking_no){
                return $query->where('locker_activities_return.tracking_no','LIKE',"%$tracking_no%");
            })
            ->when($merchant_name, function ($query) use ($merchant_name){
                return $query->where('locker_activities_return.merchant_name','=',$merchant_name);
            })
            ->when($phone_number, function ($query) use ($phone_number){
                return $query->where('locker_activities_return.phone_number','LIKE',"%$phone_number%");
            })
            ->when($locker_name, function ($query) use ($locker_name){
                return $query->where('locker_activities_return.locker_name','LIKE',"%$locker_name%");
            })
            ->when($storetime, function ($query) use ($startDate, $endDate){
                return $query->whereBetween('locker_activities_return.storetime', [$startDate, $endDate]);
            })
            ->when($status, function ($query) use ($status){
                return $query->where('locker_activities_return.status','=',$status);
            })
            ->when($groupName, function ($query) use ($groupName){
                return $query->where('locker_activities_return.groupname','LIKE',"%$groupName%");
            })
            ->when($courier_name, function ($query) use ($courier_name){
                return $query->where('locker_activities_return.courier_name','LIKE',"%$courier_name%");
            })
            ->when($search, function ($query) use ($search){
                return $query->where('locker_activities_return.tracking_no', 'LIKE', '%'.$search.'%')
                ->orWhere('locker_activities_return.merchant_name', 'LIKE', '%'.$search.'%')
                ->orWhere('locker_activities_return.phone_number', 'LIKE', '%'.$search.'%')
                ->orWhere('locker_activities_return.locker_name', 'LIKE', '%'.$search.'%')
                ->orWhere('locker_activities_return.status', 'LIKE', '%'.$search.'%')
                ->orWhere('locker_activities_return.groupname', 'LIKE', '%'.$search.'%')
                ->orWhere('locker_activities_return.courier_name', 'LIKE', '%'.$search.'%');
            })
            ->count();
        }

        $data = array();
        if(!empty($posts))
        {
            foreach ($posts as $post)
            {
                $nestedData['tracking_no'] = $post->tracking_no;
                $nestedData['merchant_name'] = $post->merchant_name;
                $nestedData['phone_number'] = $post->phone_number;
                $nestedData['locker_name'] = $post->locker_name;
                $nestedData['locker_size'] = $post->locker_size;
                $nestedData['locker_number'] = $post->locker_number;
                $nestedData['storeTime'] = $post->storetime;
                $nestedData['takeTime'] = $post->taketime;
                $nestedData['status'] = $post->status;
                $nestedData['groupName'] = $post->groupname;
                $nestedData['courier_name'] = $post->courier_name;

                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );
        return($json_data);
    }

    public function downloadExcel(Request $request)
    {
        $tracking_no = $request->input('tracking_no', null);
        $merchant_name = $request->input('merchant_name', null);
        $phone_number = $request->input('phone', null);
        $locker_name = $request->input('location', null);
        $storetime = $request->input('storetime', null);
        $status = $request->input('status', null);
        $groupName = $request->input('groupName', null);
        $courier_name = $request->input('courier_name', null);

        $startDate = null;
        $endDate = null;
        if (!empty($storetime)) {
            $date = explode(" - ", $storetime);
            $start = $date[0];
            $end = $date[1];
        }
        $startDate = $start.' 00:00:00';
        $endDate = $end.' 23:59:59';
        
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        
        // style
        $bold = [
            'font' => [
                'bold' => true,
                'size' => 20
            ]
        ];

        // title
        $sheet->mergeCells('A1:E1');
        $sheet->setCellValue('A1', 'General Return Activity List');
        $sheet->getStyle('A1')->applyFromArray($bold);
        $sheet->mergeCells('A2:E2');
        if (!empty($storetime)) {
        $sheet->setCellValue('A2', $start.' - '.$end);
        }

        // header
        $count = 4;
        $sheet->setCellValue('A'.$count, 'No');
        $sheet->setCellValue('B'.$count, 'Return Number');
        $sheet->setCellValue('C'.$count, 'Online Store');
        $sheet->setCellValue('D'.$count, 'Customer Phone');
        $sheet->setCellValue('E'.$count, 'Location');
        $sheet->setCellValue('F'.$count, 'Locker');
        $sheet->setCellValue('G'.$count, 'Store Time');
        $sheet->setCellValue('H'.$count, 'Take Time');
        $sheet->setCellValue('I'.$count, 'Status');
        $sheet->setCellValue('J'.$count, 'Group Name');
        $sheet->setCellValue('K'.$count, 'Courier Name');

        $data = LockerActivitiesReturn::leftJoin('return_statuses','locker_activities_return.tracking_no','=','return_statuses.number')
        ->when($tracking_no, function ($query) use ($tracking_no){
            return $query->where('locker_activities_return.tracking_no','LIKE',"%$tracking_no%");
        })
        ->when($merchant_name, function ($query) use ($merchant_name){
            return $query->where('locker_activities_return.merchant_name','=',$merchant_name);
        })
        ->when($phone_number, function ($query) use ($phone_number){
            return $query->where('locker_activities_return.phone_number','LIKE',"%$phone_number%");
        })
        ->when($locker_name, function ($query) use ($locker_name){
            return $query->where('locker_activities_return.locker_name','LIKE',"%$locker_name%");
        })
        ->when($storetime, function ($query) use ($startDate, $endDate){
            return $query->whereBetween('locker_activities_return.storetime', [$startDate, $endDate]);
        })
        ->when($status, function ($query) use ($status){
            return $query->where('locker_activities_return.status','=',$status);
        })
        ->when($groupName, function ($query) use ($groupName){
            return $query->where('locker_activities_return.groupname','LIKE',"%$groupName%");
        })
        ->when($courier_name, function ($query) use ($courier_name){
            return $query->where('locker_activities_return.courier_name','LIKE',"%$courier_name%");
        })
        // ->when($search, function ($query) use ($search){
        //     return $query->where('locker_activities_return.tracking_no', 'LIKE', '%'.$search.'%')
        //     ->orWhere('locker_activities_return.merchant_name', 'LIKE', '%'.$search.'%')
        //     ->orWhere('locker_activities_return.phone_number', 'LIKE', '%'.$search.'%')
        //     ->orWhere('locker_activities_return.locker_name', 'LIKE', '%'.$search.'%')
        //     ->orWhere('locker_activities_return.status', 'LIKE', '%'.$search.'%')
        //     ->orWhere('locker_activities_return.groupname', 'LIKE', '%'.$search.'%')
        //     ->orWhere('locker_activities_return.courier_name', 'LIKE', '%'.$search.'%');
        // })
        ->orderBy('locker_activities_return.storetime', 'desc')
        ->get();
        
        if($data->isEmpty()) {
            // data kosong
            $count = $count+1;
            $sheet->mergeCells('A'.$count.':K'.$count);
            $sheet->setCellValue('A'.$count, 'No records found');
        } else { 
            // data ready
            $no = 1;
            foreach($data as $key => $row) {
                $count = $count+1;
                $sheet->setCellValue('A'.$count, $no++);
                $sheet->setCellValue('B'.$count, $row['tracking_no']);
                $sheet->setCellValue('C'.$count, $row['merchant_name']);
                $sheet->setCellValue('D'.$count, $row['phone_number']);
                $sheet->setCellValue('E'.$count, $row['locker_name']);
                $sheet->setCellValue('F'.$count, $row['locker_size'].' / '.$row['locker_number']);
                $sheet->setCellValue('G'.$count, $row['storetime']);
                $sheet->setCellValue('H'.$count, $row['taketime']);
                $sheet->setCellValue('I'.$count, $status);
                $sheet->setCellValue('J'.$count, $row['groupname']);
                $sheet->setCellValue('K'.$count, $row['courier_name']);
            }
        }

        $dateNow = date("ymd");
        $filename = $dateNow.' Return Activities.xlsx';
        
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xlsx");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'.$filename.'"');
        $writer->save("php://output");
    }

    public function showAnalytics(Request $request)
    {
        $lockerName = LockerLocation::orderBy('name','asc')->get()->pluck('name','locker_id')->toArray();
        $countries = ['Indonesia', 'Malaysia'];
        $year = range(date("Y"), 2018);
        
        $data = [
            'lockerNames'   => $lockerName,
            'countries'     => $countries,
            'year'          => $year
        ];
        return view('popbox.return.analytics', $data);
    }

    public function getAnalytics(Request $request)
    {
        $locker_name = $request->locker_name;
        if($request->year && $request->year != date('Y')) {
            $month = 12;
            $year = $request->year;
        } else {
            $month = date('n');
            $year = date('Y');
        }
        $status = LockerActivitiesReturn::distinct()->get(['status'])->pluck('status');
        
        foreach($status as $stat) {
            for($row = 1; $row <= $month; $row++) {
                $count = LockerActivitiesReturn::select(DB::raw('COUNT(id) as count'))
                ->where(DB::raw('YEAR(storetime)'), $year)
                ->where(DB::raw('MONTH(storetime)'), $row)
                ->when($locker_name, function($query) use($locker_name){
                    return $query->where('locker_name', $locker_name);
                })
                ->where('status', $stat)
                ->get('count')->pluck('count')->toArray();
                
                $usage[$stat][] = $count[0];
            }
        }
        
        $topLocation = LockerActivitiesReturn::select('locker_name',DB::raw('COUNT(locker_name) as count'))
        ->where(DB::raw('YEAR(storetime)'), $year)
        ->groupBy('locker_name')
        ->orderBy(DB::raw('COUNT(locker_name)'), 'desc')
        ->limit(10)
        ->get();

        $merchant = LockerActivitiesReturn::select('merchant_name',DB::raw('COUNT(id) as count'))
        ->where(DB::raw('YEAR(storetime)'), $year)
        ->when($locker_name, function($query) use($locker_name){
            return $query->where('locker_name', $locker_name);
        })
        ->groupBy('merchant_name')
        ->orderBy(DB::raw('COUNT(merchant_name)'), 'desc')
        ->get();

        $merchantData = [];
        foreach ($merchant as $key => $row) {
            $merchantData[] = [
                'name' => $row->merchant_name,
                'y' => $row->count
            ];
        }
        
        $data = [
            'completed' => $usage['COMPLETED'],
            'courier_taken' => $usage['COURIER_TAKEN'],
            'operator_taken'  => $usage['OPERATOR_TAKEN'],
            'instore'=> $usage['IN_STORE'],
            'catTopLocation' => $topLocation->map->only(['locker_name'])->pluck('locker_name'),
            'valTopLocation' => $topLocation->map->only(['count'])->pluck('count'),
            'merchant' => $merchantData
        ];

        echo json_encode($data);
    }
}
