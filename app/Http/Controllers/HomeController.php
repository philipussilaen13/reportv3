<?php

namespace App\Http\Controllers;

use Hash;
use Validator;
use App\Library\Client;
use App\Models\User;
use App\Models\UserRoute;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;
use App\Http\Controllers\Popapps\PopsafeController;


class HomeController extends PopsafeController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        // $this->middleware('operator.access');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = Auth::user()->id;
        Redis::del('client:'.date('Ymd').$user_id);
        $id_redis = date('Ymd').$user_id;
        $redisData = Redis::hget('client:'.$id_redis, "menu");
        if (!$redisData) {
            $data = $this->storeToRedis();
            $client = new Client($id_redis, json_encode($data->data));
            $client->store();
        }
        return redirect('home');
    }

    private function storeToRedis()
    {
        $response = new \stdClass;
        $response->isSuccess = false;
        $response->data = null;

        $user_id = Auth::user()->id;
        $menu = UserRoute::where('u_id', $user_id)
            ->where('type', '=', 'menu')
            ->where('parent_id', 0)
            ->groupBy('id')
            ->orderBy('route_order')
            ->get();
        
        $item = [];
        $child = [];
        $child__ = [];
        $grandchild__ = [];
        foreach ($menu as $key => $row) {

            $checkChild = DB::table('user_route')
            ->where('u_id', $user_id)
            ->where('type', '=', 'menu')
            ->where('parent_id', $row->id)
            ->groupBy('id')
            ->get();
            
            if($checkChild) {
                $child = [];
                foreach($checkChild as $key => $dataChild) {
                    $child__[] = $dataChild->id;
                    $checkGrandChild = DB::table('user_route')
                    ->where('u_id', $user_id)
                    ->where('type', '=', 'menu')
                    ->where('parent_id', $dataChild->id)
                    ->groupBy('id')
                    ->get();

                    if($checkGrandChild) {
                        $grandChild = [];

                        foreach($checkGrandChild as $key => $dataGrandChild) {
                            $grandchild__[] = $dataGrandChild->id; 
                            $grandChild[] = [
                                'id' => $dataGrandChild->id,
                                'name' => $dataGrandChild->name,
                                'route' => $dataGrandChild->route,
                                'route_name' => $dataGrandChild->route_name,
                                'icon' => $dataGrandChild->icon,
                                'child' => ""
                            ];
                        }
                    }
                    
                    $child[] = [
                        'id' => $dataChild->id,
                        'name' => $dataChild->name,
                        'route' => $dataChild->route,
                        'route_name' => $dataChild->route_name,
                        'icon' => $dataChild->icon,
                        'child' => empty($grandChild) ? false : $grandChild
                    ];
                }
            } 

            if (!in_array($row->id, $child__) && !in_array($row->id, $grandchild__)) {
                $item[] = [
                    'id' => $row->id,
                    'name' => $row->name,
                    'route' => $row->route,
                    'route_name' => $row->route_name,
                    'icon' => $row->icon,
                    'child' => empty($child) ? false : $child
                ];   
            }
        }

        $response->data = $item;
        return $response;
    }

    public function showProfile(Request $request)
    {
        $userData = Auth::user();
        $data = [
            'id'    => $userData->id,
            'name'  => $userData->name,
            'email' => $userData->email,
        ];
        
        return view('user.profile', $data);
    }

    public function updatePassword(Request $request)
    {
        $id = $request->id;
        $newPassword = $request->new_password;
        
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'new_password' => 'required|min:6|alpha_num',
            'new_password_confirmation' => 'required|same:new_password'
        ]);
        
        if (!$validator->fails()) {
            // if valid
            $data = User::where('id', $id)->first();
            $data->password = Hash::make($newPassword);
            $data->save();

            return redirect('profile')->with(['success'=> "Password has been reset"]);
        } else {
            return redirect('profile')->with(['error'=> "Try again. Password failed to update"]);
        }
    }
}
