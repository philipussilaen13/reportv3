<?php

namespace App\Http\Controllers\Popapps;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Popapps\Popsafe;
use App\Models\NewLocker\Express;
use App\Models\Popapps\PopsafeHistory;
use App\Models\NewLocker\SmsHistory;
use App\Models\Popapps\Transaction;
use App\Models\NewLocker\Box;
use App\Models\Popbox\LockerLocation;
use App\Http\Helpers\ProxApi;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class PopsafeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groupName = 'POPBOX';
        $status = Popsafe::distinct()->get(['status'])->pluck('status'); // popsafe status
        $location = Popsafe::distinct()->orderBy('locker_name','asc')->get(['locker_name'])->pluck('locker_name');
        // $arr = ['IN STORE' => 'IN STORE'];
        // $status = array_merge(end($status), $arr);
        // foreach($status as $row) {
        //     $data['index'][str_slug($row)] = Popsafe::leftJoin('transactions as t', 't.transaction_id_reference', '=', 'popsafes.id')
        //     ->where('t.transaction_type', '=', 'popsafe')
        //     ->where('locker_name', 'like', 'DPS %')
        //     ->where('popsafes.status', $row)
        //     ->groupBy('popsafes.status')
        //     ->count(DB::raw('DISTINCT popsafes.id'));
        // }
        
        return view('popapps.popsafe.list', compact('groupName','status','location'));
    }

    /**
     * Display a listing of the resource.
     *
     * Datatables Server Side
     */
    public function getAjaxServerPopsafeList(Request $request)
    {
        $parameter = $request->route()->parameters();
        $groupName = $parameter['groupName'];

        $columns = array( 
            0 =>'invoice_code', 
            1 =>'status',
            2=> 'name',
            3=> 'locker_name',
            4=> 'country',
            5=> 'locker_size',
            6=> 'duration',
            7=> 'transaction_date',
            8=> 'total_price',
            9=> 'campaign_name',
            10=> 'code',
            11=> 'pinCode',
            12=> 'phone'
        );
        
        $search = $request->input('search.value');
        $invoiceId = $request->input('invoiceId', null);
        $status = $request->input('status', null);
        $user = $request->input('user', null);
        $location = $request->input('location', null);
        $transactionDate = $request->input('transactionDate', null);
        $codePin = $request->input('codePin', null);
        $phone = $request->input('phone', null);
        
        // dd($invoiceId, $status, $user, $location, $transactionDate, $codePin, $search);
        if(!empty($request->input('columns.0.search.value'))) {
            $invoiceId = $request->input('columns.0.search.value');
        }

        if(!empty($request->input('columns.1.search.value'))) {
            $status = $request->input('columns.1.search.value');
        }
        
        if(!empty($request->input('columns.2.search.value'))) {
            $user = $request->input('columns.2.search.value');
        }

        if(!empty($request->input('columns.3.search.value'))) {
            $location = $request->input('columns.3.search.value');
        }

        if(!empty($request->input('columns.7.search.value'))) {
            $transactionDate = $request->input('columns.7.search.value');
        } else {
            $transactionDate = date('Y-m')."-01 - ".date('Y-m-d');
        }

        if(!empty($request->input('columns.11.search.value'))) {
            $codePin = $request->input('columns.11.search.value');
        }

        if(!empty($request->input('columns.12.search.value'))) {
            $phone = $request->input('columns.12.search.value');
        }
        
        $startDate = null;
        $endDate = null;
        if (!empty($transactionDate)) {
            $date = explode(" - ", $transactionDate);
            $startDate = $date[0].' 00:00:00';
            $endDate = $date[1].' 23:59:59';
        }

        $allData = Popsafe::select('u.member_id', 'u.name', 'u.phone', 'u.country', 'popsafes.id as popsafes_id', 'popsafes.user_id', 'popsafes.invoice_code', 'popsafes.status', 'popsafes.locker_name', 'popsafes.locker_size', 'popsafes.locker_number', 'popsafes.transaction_date', 't.total_price', 't.promo_amount','t.paid_amount', 'v.code', 'c.name AS campaign_name', 'c.id AS campaign_id', DB::raw('sum(t.total_price) as total_price'), 'popsafes.status_taken_by', 'popsafes.code_pin')
        ->leftjoin('users as u','popsafes.user_id','=','u.id')
        ->leftJoin('transactions as t', 'popsafes.id', '=', 't.transaction_id_reference')
        ->leftJoin('campaign_usages as cu', 't.id', '=', 'cu.transaction_id')
        ->leftJoin('campaigns as c', 'cu.campaign_id', '=', 'c.id')
        ->leftJoin('vouchers as v', 'cu.voucher_id', '=', 'v.id')
        ->where('t.transaction_type', '=', 'popsafe')
        ->when($groupName == 'APS', function ($query) use($groupName) {
            return $query->whereRaw("(`popsafes`.`locker_name` LIKE 'DPS %' or `popsafes`.`locker_name` LIKE 'SRG %')");
        })
        ->groupBy('u.member_id', 'u.name', 'u.phone', 'u.country', 'popsafes.id', 'popsafes.user_id', 'popsafes.invoice_code', 'popsafes.status', 'popsafes.locker_name', 'popsafes.locker_size', 'popsafes.locker_number', 'popsafes.transaction_date', 'v.code', 'c.name', 'c.id', 'popsafes.status_taken_by')
        ->get();

        $totalData = count($allData);
        
        // kondisi limit ketika show all data
        if($request->input('length') == -1) {
            $limit = $totalData;
        } else {
            $limit = $request->input('length');
        }

        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        
        if(empty($request->input('search.value')) && empty($invoiceId) && empty($status) && empty($user) && empty($location) && empty($codePin) && empty($transactionDate) && empty($phone))
        // if(empty($request->input('search.value')) && empty($request->input('columns.0.search.value')) && empty($request->input('columns.1.search.value')) && empty($request->input('columns.3.search.value')) && empty($request->input('columns.7.search.value')) )
        {            
            $posts = Popsafe::select('u.member_id', 'u.name', 'u.phone', 'u.country', 'popsafes.id as popsafes_id', 'popsafes.user_id', 'popsafes.invoice_code', 'popsafes.status', 'popsafes.locker_name', 'popsafes.locker_size', 'popsafes.locker_number', 'popsafes.transaction_date', 't.total_price', 't.promo_amount','t.paid_amount', 'v.code', 'c.name AS campaign_name', 'c.id AS campaign_id', DB::raw('sum(t.total_price) as total_price'), 'popsafes.status_taken_by', 'popsafes.code_pin')
            ->leftjoin('users as u','popsafes.user_id','=','u.id')
            ->leftJoin('transactions as t', 't.transaction_id_reference', '=', 'popsafes.id')
            ->leftJoin('campaign_usages as cu', 'cu.transaction_id', '=', 't.id')
            ->leftJoin('campaigns as c', 'c.id', '=', 'cu.campaign_id')
            ->leftJoin('vouchers as v', 'v.id', '=', 'cu.voucher_id')
            ->where('t.transaction_type', '=', 'popsafe')
            ->when($groupName == 'APS', function ($query) use($groupName) {
                return $query->whereRaw("(`popsafes`.`locker_name` LIKE 'DPS %' or `popsafes`.`locker_name` LIKE 'SRG %')");
            })
            ->when($startDate != null && $endDate != null, function ($query) use ($startDate, $endDate){
                return $query->whereBetween('popsafes.created_at', [$startDate, $endDate]);
            })
            ->groupBy('u.member_id', 'u.name', 'u.phone', 'u.country', 'popsafes.id', 'popsafes.user_id', 'popsafes.invoice_code', 'popsafes.status', 'popsafes.locker_name', 'popsafes.locker_size', 'popsafes.locker_number', 'popsafes.transaction_date', 't.total_price', 't.promo_amount','t.paid_amount', 'v.code', 'c.name', 'c.id', 'popsafes.status_taken_by')
            ->limit($limit)
            ->offset($start)
            ->orderBy($order, $dir)
            ->get();

            $allFiltered = Popsafe::select('u.member_id', 'u.name', 'u.phone', 'u.country', 'popsafes.id as popsafes_id', 'popsafes.user_id', 'popsafes.invoice_code', 'popsafes.status', 'popsafes.locker_name', 'popsafes.locker_size', 'popsafes.locker_number', 'popsafes.transaction_date', 't.total_price', 't.promo_amount','t.paid_amount', 'v.code', 'c.name AS campaign_name', 'c.id AS campaign_id', DB::raw('sum(t.total_price) as total_price'), 'popsafes.status_taken_by')
            ->leftjoin('users as u','popsafes.user_id','=','u.id')
            ->leftJoin('transactions as t', 't.transaction_id_reference', '=', 'popsafes.id')
            ->leftJoin('campaign_usages as cu', 'cu.transaction_id', '=', 't.id')
            ->leftJoin('campaigns as c', 'c.id', '=', 'cu.campaign_id')
            ->leftJoin('vouchers as v', 'v.id', '=', 'cu.voucher_id')
            ->where('t.transaction_type', '=', 'popsafe')
            ->when($groupName == 'APS', function ($query) use($groupName) {
                return $query->whereRaw("(`popsafes`.`locker_name` LIKE 'DPS %' or `popsafes`.`locker_name` LIKE 'SRG %')");
            })
            ->when($startDate != null && $endDate != null, function ($query) use ($startDate, $endDate){
                return $query->whereBetween('popsafes.created_at', [$startDate, $endDate]);
            })
            ->groupBy('u.member_id', 'u.name', 'u.phone', 'u.country', 'popsafes.id', 'popsafes.user_id', 'popsafes.invoice_code', 'popsafes.status', 'popsafes.locker_name', 'popsafes.locker_size', 'popsafes.locker_number', 'popsafes.transaction_date', 't.total_price', 't.promo_amount','t.paid_amount', 'v.code', 'c.name', 'c.id', 'popsafes.status_taken_by')
            ->orderBy($order, $dir)
            ->get();
            
            $totalFiltered = count($allFiltered);
        }
        else {
                $search = $request->input('search.value'); 

                $posts = Popsafe::select('u.member_id', 'u.name', 'u.phone', 'u.country', 'popsafes.id as popsafes_id', 'popsafes.user_id', 'popsafes.invoice_code', 'popsafes.status', 'popsafes.locker_name', 'popsafes.locker_size', 'popsafes.locker_number', 'popsafes.transaction_date', 'v.code', 'c.name AS campaign_name', 'c.id AS campaign_id', DB::raw('sum(t.total_price) as total_price'), 'popsafes.status_taken_by', 'popsafes.code_pin')
                ->leftjoin('users as u','popsafes.user_id','=','u.id')
                ->leftJoin('transactions as t', 'popsafes.id', '=', 't.transaction_id_reference')
                ->leftJoin('campaign_usages as cu', 't.id', '=', 'cu.transaction_id')
                ->leftJoin('campaigns as c', 'cu.campaign_id', '=', 'c.id')
                ->leftJoin('vouchers as v', 'cu.voucher_id', '=', 'v.id')
                ->where('t.transaction_type', '=', 'popsafe')
                ->when($groupName == 'APS', function ($query) use($groupName) {
                    return $query->whereRaw("(`popsafes`.`locker_name` LIKE 'DPS %' or `popsafes`.`locker_name` LIKE 'SRG %')");
                })
                ->when($invoiceId != null, function ($query) use($invoiceId) {
                    return $query->where('popsafes.invoice_code','LIKE','%'.$invoiceId.'%');
                })
                ->when($status != null, function ($query) use($status) {
                    return $query->where('popsafes.status', $status);
                })
                ->when($location != null, function ($query) use($location) {
                    return $query->where('popsafes.locker_name', 'LIKE','%'.$location.'%');
                })
                ->when($user != null, function ($query) use($user) {
                    return $query->where('u.name', 'LIKE', '%'.$user.'%');
                })
                ->when($phone != null, function ($query) use($phone) {
                    return $query->where('u.phone', 'LIKE', '%'.$phone.'%');
                })
                ->when($transactionDate != null, function ($query) use ($startDate, $endDate){
                    return $query->whereBetween('popsafes.created_at', [$startDate, $endDate]);
                })
                ->when($codePin != null, function ($query) use ($codePin){
                    return $query->where('popsafes.code_pin', 'LIKE', '%'.$codePin.'%');
                })
                ->when($search != null, function ($query) use ($search){
                    return $query->where('popsafes.invoice_code', 'LIKE', '%'.$search.'%')
                    ->orWhere('popsafes.status', 'LIKE', '%'.$search.'%')
                    ->orWhere('popsafes.locker_name', 'LIKE', '%'.$search.'%')
                    ->orWhere('popsafes.code_pin', 'LIKE', '%'.$search.'%')
                    ->orWhere('u.name', 'LIKE', '%'.$search.'%')
                    ->orWhere('u.phone', 'LIKE', '%'.$search.'%')
                    ->orWhere('v.code', 'LIKE', '%'.$search.'%');
                })
                // ->when($q != null, function ($query) use($q) {
                //     $query->where('popsafes.invoice_code','LIKE','%'.$q.'%');
                //     $query->orwhere('u.name', 'LIKE','%'.$q.'%');
                //     $query->orwhere('popsafes.locker_name', 'LIKE','%'.$q.'%');
                // })
                ->groupBy('u.member_id', 'u.name', 'u.phone', 'u.country', 'popsafes.id', 'popsafes.user_id', 'popsafes.invoice_code', 'popsafes.status', 'popsafes.locker_name', 'popsafes.locker_size', 'popsafes.locker_number', 'popsafes.transaction_date', 'v.code', 'c.name', 'c.id', 'popsafes.status_taken_by')
                ->limit($limit)
                ->offset($start)
                ->orderBy($order,$dir)
                ->get();

                $allFiltered = Popsafe::select('u.member_id', 'u.name', 'u.phone', 'u.country', 'popsafes.id as popsafes_id', 'popsafes.user_id', 'popsafes.invoice_code', 'popsafes.status', 'popsafes.locker_name', 'popsafes.locker_size', 'popsafes.locker_number', 'popsafes.transaction_date', 'v.code', 'c.name AS campaign_name', 'c.id AS campaign_id', DB::raw('sum(t.total_price) as total_price'), 'popsafes.status_taken_by')
                ->leftjoin('users as u','popsafes.user_id','=','u.id')
                ->leftJoin('transactions as t', 'popsafes.id', '=', 't.transaction_id_reference')
                ->leftJoin('campaign_usages as cu', 't.id', '=', 'cu.transaction_id')
                ->leftJoin('campaigns as c', 'cu.campaign_id', '=', 'c.id')
                ->leftJoin('vouchers as v', 'cu.voucher_id', '=', 'v.id')
                ->where('t.transaction_type', '=', 'popsafe')
                ->when($groupName == 'APS', function ($query) use($groupName) {
                    return $query->whereRaw("(`popsafes`.`locker_name` LIKE 'DPS %' or `popsafes`.`locker_name` LIKE 'SRG %')");
                })
                ->when($invoiceId != null, function ($query) use($invoiceId) {
                    return $query->where('popsafes.invoice_code','LIKE','%'.$invoiceId.'%');
                })
                ->when($status != null, function ($query) use($status) {
                    return $query->where('popsafes.status', $status);
                })
                ->when($location != null, function ($query) use($location) {
                    return $query->where('popsafes.locker_name', 'LIKE','%'.$location.'%');
                })
                ->when($user != null, function ($query) use($user) {
                    return $query->where('u.name', 'LIKE', '%'.$user.'%');
                })
                ->when($phone != null, function ($query) use($phone) {
                    return $query->where('u.phone', 'LIKE', '%'.$phone.'%');
                })
                ->when($transactionDate != null, function ($query) use ($startDate, $endDate){
                    return $query->whereBetween('popsafes.created_at', [$startDate, $endDate]);
                })
                ->when($codePin != null, function ($query) use ($codePin){
                    return $query->where('popsafes.code_pin', 'LIKE', '%'.$codePin.'%');
                })
                ->when($search != null, function ($query) use ($search){
                    return $query->where('popsafes.invoice_code', 'LIKE', '%'.$search.'%')
                    ->orWhere('popsafes.status', 'LIKE', '%'.$search.'%')
                    ->orWhere('popsafes.locker_name', 'LIKE', '%'.$search.'%')
                    ->orWhere('popsafes.code_pin', 'LIKE', '%'.$search.'%')
                    ->orWhere('u.name', 'LIKE', '%'.$search.'%')
                    ->orWhere('u.phone', 'LIKE', '%'.$search.'%')
                    ->orWhere('v.code', 'LIKE', '%'.$search.'%');
                })
                ->groupBy('u.member_id', 'u.name', 'u.phone', 'u.country', 'popsafes.id', 'popsafes.user_id', 'popsafes.invoice_code', 'popsafes.status', 'popsafes.locker_name', 'popsafes.locker_size', 'popsafes.locker_number', 'popsafes.transaction_date', 'v.code', 'c.name', 'c.id', 'popsafes.status_taken_by')
                ->orderBy($order,$dir)
                ->get();

                $totalFiltered = count($allFiltered);
        }

        $data = array();
        if(!empty($posts))
        {
            foreach ($posts as $post)
            {
                $nestedData['invoice_code'] = $post->invoice_code;
                $nestedData['status'] = $post->status;
                $nestedData['name'] = $post->name;
                $nestedData['phone'] = $post->phone;
                $nestedData['member_id'] = $post->member_id;
                $nestedData['locker_name'] = $post->locker_name;
                $nestedData['country'] = $post->country;
                $nestedData['locker_size'] = $post->locker_size;
                $nestedData['locker_number'] = $post->locker_number;
                $nestedData['transaction_date'] = $post->transaction_date;
                $nestedData['total_price'] = $post->total_price;
                $nestedData['promo_amount'] = $post->promo_amount;
                $nestedData['paid_amount'] = $post->paid_amount;
                $nestedData['campaign_name'] = $post->campaign_name;
                $nestedData['code'] = $post->code;
                $nestedData['duration'] = $this->getDuration($post->popsafes_id);
                $nestedData['sub_status'] = $post->status_taken_by;
                $nestedData['code_pin'] = $post->code_pin;

                $data[] = $nestedData;
            }
        }
          
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );
        return($json_data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getDetail(Request $request, $invoice_code=null, $invoice_id=null)
    {
        $data = Popsafe::getByInvoiceCode($invoice_code)->first();
        if($invoice_id != null) {
            $activeTransaction = $invoice_id;
        } else {
            $activeTransaction = '';
        }
        $tracking = PopsafeHistory::getByPopsafeId($data->popsafe_id);
        $transactions = Transaction::getByPopsafeId($data->popsafe_id);
        $express = Express::where('expressNumber', $invoice_code)
        ->orWhere('customerStoreNumber', $invoice_code)
        ->orderBy('storeTime','desc')
        ->first();
        $sms['data'] = SmsHistory::where('express_id', $express->id)->get();
        $sms['sms_count'] = SmsHistory::countSMS($express->id);
        return view('popapps.popsafe.detail', compact('data','tracking','transactions','activeTransaction', 'sms'));
    }

    public function getDetailPin(Request $request, $invoice_code=null)
    {
        // $data = User::where('member_id', $member_id)->first(); // model users popsend
        $code_pin = Popsafe::getPinByInvoiceCode($invoice_code);
        // dd($code_pin);
        // return json_encode($code_pin);
        return response()->json([
            'invoice_code' => $invoice_code,
            'code_pin' => $code_pin
        ]);
    }

    public function getLandingAPS()
    {
        $groupName = 'APS';
        $status = Popsafe::distinct()->get(['status'])->pluck('status','status'); // popsafe status
        $arr = ['IN STORE' => 'IN STORE'];
        $status = array_merge(end($status), $arr);
        foreach($status as $row) {
            $data['index'][str_slug($row)] = Popsafe::leftJoin('transactions as t', 't.transaction_id_reference', '=', 'popsafes.id')
            ->where('t.transaction_type', '=', 'popsafe')
            ->whereRaw("(`popsafes`.`locker_name` LIKE 'DPS %' or `popsafes`.`locker_name` LIKE 'SRG %')")
            ->where('popsafes.status', $row)
            ->groupBy('popsafes.status')
            ->count(DB::raw('DISTINCT popsafes.id'));
        }
        $index = $data['index']; // counter summary status
        $location = ['DPS 01', 'DPS 02', 'SRG 01']; // locker location, locker name
        return view('popapps.popsafe.list', compact('groupName','status','index','location'));
    }

    public function addRemarks(Request $request)
    {
        $parameter = $request->route()->parameters();
        $data = Express::where('expressNumber', $parameter['invoice_code'])->first();
        if ($data) {
            $popsafe = new Popsafe;
            $add_history = $popsafe->addRemarks($parameter['invoice_code'], $request->input('remarks'));
            
            if (!$add_history->isSuccess) {
                return back()->with('failed','Failed Add Remarks caused by '. $res->errorMessage);
            } else {
                return back()->with('success','Success Add Remarks');
            }
        }
    }

    public function resendSms(Request $request)
    {
        $parameter = $request->route()->parameters();
        $express = Express::where('expressNumber', $parameter['invoice_code'])
        ->orWhere('customerStoreNumber', $parameter['invoice_code'])->first();
        
        $smsHistory = new SmsHistory;
        $countSms = $smsHistory->countSMS($express->id);
        
        if ($countSms < 3) {
            $proxApi = new ProxApi;
            $res = $proxApi->reSendSMSbyParcelId($express->id);
            
            if (property_exists((object)$res, 'statusCode')) {
                return back()->with('failed','Failed Send SMS caused by '. $res->errorMessage);
            } else {
                return back()->with('success','Success Send SMS');
            }
        } else {
            return back()->with('failed','SMS Resend already reach maximum quota');
        }

    }

    public function getAjaxUser(Request $request)
    {
        $invoiceId = $request->input('invoiceId', null);
        $status = $request->input('status', null);
        $location = $request->input('location', null);
        $transactionDate = $request->input('transactionDate', null);
        
        $startDate = null;
        $endDate = null;
        if (!empty($transactionDate)) {
            $date = explode(" - ", $transactionDate);
            $startDate = $date[0].' 00:00:00';
            $endDate = $date[1].' 23:59:59';
        }
        
        $allData = Popsafe::select('u.name')
        ->leftjoin('users as u','popsafes.user_id','=','u.id')
        ->leftJoin('transactions as t', 't.transaction_id_reference', '=', 'popsafes.id')
        ->where('t.transaction_type', '=', 'popsafe')
        ->whereRaw("(`popsafes`.`locker_name` LIKE 'DPS %' or `popsafes`.`locker_name` LIKE 'SRG %')")
        ->when($invoiceId != null, function ($query) use($invoiceId) {
            return $query->where('popsafes.invoice_code','LIKE','%'.$invoiceId.'%');
        })
        ->when($status != null, function ($query) use($status) {
            return $query->where('popsafes.status', $status);
        })
        ->when($location != null, function ($query) use($location) {
            return $query->where('popsafes.locker_name', 'LIKE','%'.$location.'%');
        })
        ->when($transactionDate, function ($query) use ($startDate, $endDate){
            return $query->whereBetween('popsafes.created_at', [$startDate, $endDate]);
        })
        ->groupBy('u.name')
        ->get();

        $user = array();
        if(!empty($allData))
        {
            foreach ($allData as $post)
            {
                $user[] = $post->name;
            }
        }
        
        echo json_encode($user);
    }

    public function getAjaxAllPhone(Request $request)
    {
        $invoiceId = $request->input('invoiceId', null);
        $status = $request->input('status', null);
        $location = $request->input('location', null);
        $transactionDate = $request->input('transactionDate', null);
        
        $startDate = null;
        $endDate = null;
        if (!empty($transactionDate)) {
            $date = explode(" - ", $transactionDate);
            $startDate = $date[0].' 00:00:00';
            $endDate = $date[1].' 23:59:59';
        }
        
        $allData = Popsafe::select('u.phone')
        ->leftjoin('users as u','popsafes.user_id','=','u.id')
        ->leftJoin('transactions as t', 't.transaction_id_reference', '=', 'popsafes.id')
        ->where('t.transaction_type', '=', 'popsafe')
        ->when($invoiceId != null, function ($query) use($invoiceId) {
            return $query->where('popsafes.invoice_code','LIKE','%'.$invoiceId.'%');
        })
        ->when($status != null, function ($query) use($status) {
            return $query->where('popsafes.status', $status);
        })
        ->when($location != null, function ($query) use($location) {
            return $query->where('popsafes.locker_name', 'LIKE','%'.$location.'%');
        })
        ->when($transactionDate, function ($query) use ($startDate, $endDate){
            return $query->whereBetween('popsafes.created_at', [$startDate, $endDate]);
        })
        ->groupBy('u.phone')
        ->get();

        $phone = array();
        if(!empty($allData))
        {
            foreach ($allData as $post)
            {
                $phone[] = $post->phone;
            }
        }
        
        echo json_encode($phone);
    }

    public function getAjaxAllUser(Request $request)
    {
        $invoiceId = $request->input('invoiceId', null);
        $status = $request->input('status', null);
        $location = $request->input('location', null);
        $transactionDate = $request->input('transactionDate', null);
        
        $startDate = null;
        $endDate = null;
        if (!empty($transactionDate)) {
            $date = explode(" - ", $transactionDate);
            $startDate = $date[0].' 00:00:00';
            $endDate = $date[1].' 23:59:59';
        }
        
        $allData = Popsafe::select('u.name')
        ->leftjoin('users as u','popsafes.user_id','=','u.id')
        ->leftJoin('transactions as t', 't.transaction_id_reference', '=', 'popsafes.id')
        ->where('t.transaction_type', '=', 'popsafe')
        ->when($invoiceId != null, function ($query) use($invoiceId) {
            return $query->where('popsafes.invoice_code','LIKE','%'.$invoiceId.'%');
        })
        ->when($status != null, function ($query) use($status) {
            return $query->where('popsafes.status', $status);
        })
        ->when($location != null, function ($query) use($location) {
            return $query->where('popsafes.locker_name', 'LIKE','%'.$location.'%');
        })
        ->when($transactionDate, function ($query) use ($startDate, $endDate){
            return $query->whereBetween('popsafes.created_at', [$startDate, $endDate]);
        })
        ->groupBy('u.name')
        ->get();

        $user = array();
        if(!empty($allData))
        {
            foreach ($allData as $post)
            {
                $user[] = $post->name;
            }
        }
        
        echo json_encode($user);
    }

    public function postAjaxSummaryPopsafeList(Request $request)
    {
        $parameter = $request->route()->parameters();
        $invoiceId = $request->input('invoiceId', null);
        $status = $request->input('status', null);
        $user = $request->input('user', null);
        $phone = $request->input('phone', null);
        $location = $request->input('location', null);
        if($request->input('transactionDate') == null) {
            $transactionDate = date('Y-m')."-01 - ".date('Y-m-d');
        } else {
            $transactionDate = $request->input('transactionDate');
        }
        $codePin = $request->input('codePin', null);

        $startDate = null;
        $endDate = null;
        if (!empty($transactionDate)) {
            $date = explode(" - ", $transactionDate);
            $startDate = $date[0].' 00:00:00';
            $endDate = $date[1].' 23:59:59';
        }
        
        $status = Popsafe::distinct()->get(['status'])->pluck('status','status'); // popsafe status
        $arr = ['IN STORE' => 'IN STORE'];
        $status = array_merge(end($status), $arr);

        foreach($status as $row) {
            $data['index'][str_slug($row)] = Popsafe::leftjoin('users as u','popsafes.user_id','=','u.id')
            ->leftJoin('transactions as t', 'popsafes.id', '=', 't.transaction_id_reference')
            ->leftJoin('campaign_usages as cu', 't.id', '=', 'cu.transaction_id')
            ->leftJoin('campaigns as c', 'cu.campaign_id', '=', 'c.id')
            ->leftJoin('vouchers as v', 'cu.voucher_id', '=', 'v.id')
            ->where('t.transaction_type', '=', 'popsafe')
            ->where('popsafes.status', $row)
            ->when($parameter['groupName'] == 'APS', function ($query) {
                return $query->whereRaw("(`popsafes`.`locker_name` LIKE 'DPS %' or `popsafes`.`locker_name` LIKE 'SRG %')");
            })
            ->when($invoiceId != null, function ($query) use($invoiceId) {
            return $query->where('popsafes.invoice_code','LIKE','%'.$invoiceId.'%');
            })
            ->when($location != null, function ($query) use($location) {
            return $query->where('popsafes.locker_name', 'LIKE','%'.$location.'%');
            })
            ->when($user != null, function ($query) use($user) {
            return $query->where('u.name', 'LIKE', '%'.$user.'%');
            })
            ->when($phone != null, function ($query) use($phone) {
                return $query->where('u.phone', 'LIKE', '%'.$phone.'%');
            })
            ->when($transactionDate != null, function ($query) use ($startDate, $endDate){
            return $query->whereBetween('popsafes.created_at', [$startDate, $endDate]);
            })
            ->when($codePin != null, function ($query) use ($codePin){
                return $query->where('popsafes.code_pin', 'LIKE', '%'.$codePin.'%');
            })
            ->groupBy('popsafes.status')
            ->count(DB::raw('DISTINCT popsafes.id'));
        }

        $countSummary = $data['index'];
        
        echo json_encode($countSummary);
    }

    public function downloadExcel (Request $request)
    {
        $parameter = $request->route()->parameters();
        // param
        $invoiceId = $request->input('invoiceId', null);
        $status = $request->input('status', null);
        $user = $request->input('user', null);
        $phone = $request->input('phone', null);
        $location = $request->input('location', null);
        $transactionDate = $request->input('transactionDate', null);
        $codePin = $request->input('codePin', null);
        $startDate = '';
        $endDate = '';

        if (!empty($transactionDate)) {
            $date = explode(" - ", $transactionDate);
            $startDate = $date[0]." 00:00:00";
            $endDate = $date[1]." 23:59:59";
            $start = date("d M Y", strtotime($date[0]));
            $end = date("d M Y", strtotime($date[1]));
        }
        
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        // style
        $bold = [
            'font' => [
                'bold' => true,
                'size' => 20
            ]
        ];
        
        // title
        $sheet->mergeCells('A1:E1');
        $sheet->setCellValue('A1', 'Order List');
        $sheet->getStyle('A1')->applyFromArray($bold);
        $sheet->mergeCells('A2:E2');
        if (!empty($transactionDate)) {
        $sheet->setCellValue('A2', $start.' - '.$end);
        }
        // header
        $count = 4;
        $sheet->setCellValue('A'.$count, 'No');
        $sheet->setCellValue('B'.$count, 'Order ID');
        $sheet->setCellValue('C'.$count, 'Status');
        $sheet->setCellValue('D'.$count, 'Transaction Date');
        $sheet->setCellValue('E'.$count, 'Phone');
        $sheet->setCellValue('F'.$count, 'Email');
        $sheet->setCellValue('G'.$count, 'Location');
        $sheet->setCellValue('H'.$count, 'Locker No');
        $sheet->setCellValue('I'.$count, 'Duration');
        $sheet->setCellValue('J'.$count, 'Price');
        $sheet->setCellValue('K'.$count, 'Promo Amount');
        $sheet->setCellValue('L'.$count, 'Paid Amount');

        $data = Popsafe::select('u.member_id', 'u.name', 'u.phone', 'u.email', 'u.country', 'popsafes.id as popsafes_id', 'popsafes.user_id', 'popsafes.invoice_code', 'popsafes.status', 'popsafes.locker_name', 'popsafes.locker_size', 'popsafes.locker_number', 'popsafes.transaction_date', 'v.code', 'c.name AS campaign_name', 'c.id AS campaign_id', DB::raw('sum(t.total_price) as total_price'), DB::raw('sum(t.paid_amount) as total_paid'), DB::raw('sum(t.promo_amount) as total_promo'), 'popsafes.status_taken_by')
        ->leftjoin('users as u','popsafes.user_id','=','u.id')
        ->leftJoin('transactions as t', 'popsafes.id', '=', 't.transaction_id_reference')
        ->leftJoin('campaign_usages as cu', 't.id', '=', 'cu.transaction_id')
        ->leftJoin('campaigns as c', 'cu.campaign_id', '=', 'c.id')
        ->leftJoin('vouchers as v', 'cu.voucher_id', '=', 'v.id')
        ->where('t.transaction_type', '=', 'popsafe')
        ->when($parameter['groupName'] == 'APS', function ($query) {
            return $query->whereRaw("(`popsafes`.`locker_name` LIKE 'DPS %' or `popsafes`.`locker_name` LIKE 'SRG %')");
        })
        ->when($invoiceId != null, function ($query) use($invoiceId) {
            return $query->where('popsafes.invoice_code','LIKE','%'.$invoiceId.'%');
        })
        ->when($status != null, function ($query) use($status) {
            return $query->where('popsafes.status', $status);
        })
        ->when($location != null, function ($query) use($location) {
            return $query->where('popsafes.locker_name', 'LIKE','%'.$location.'%');
        })
        ->when($user != null, function ($query) use($user) {
            return $query->where('u.name', 'LIKE', '%'.$user.'%');
        })
        ->when($phone != null, function ($query) use($phone) {
            return $query->where('u.phone', 'LIKE', '%'.$phone.'%');
        })
        ->when($transactionDate != null, function ($query) use ($startDate, $endDate){
            return $query->whereBetween('popsafes.created_at', [$startDate, $endDate]);
        })
        ->when($codePin != null, function ($query) use ($codePin){
            return $query->where('popsafes.code_pin', 'LIKE', '%'.$codePin.'%');
        })
        ->groupBy('u.member_id', 'u.name', 'u.phone', 'u.country', 'popsafes.id', 'popsafes.user_id', 'popsafes.invoice_code', 'popsafes.status', 'popsafes.locker_name', 'popsafes.locker_size', 'popsafes.locker_number', 'popsafes.transaction_date', 'v.code', 'c.name', 'c.id', 'popsafes.status_taken_by')
        ->orderBy('popsafes.transaction_date','desc')
        ->get();
        // dd($data);
        if($data->isEmpty()) {
            // data kosong
            $count = $count+1;
            $sheet->mergeCells('A'.$count.':Q'.$count);
            $sheet->setCellValue('A'.$count, 'No records found');
        } else { 
            // data ready
            $no = 1;
            foreach($data as $key => $row) {
                $count = $count+1;
                if($row['service_charge'] == null) {
                    $mdr = '-';
                    $revenue = '-';
                    $settlement = '-';
                } else {
                    $mdr = $row['service_charge'];
                    $revenue = round(($row['total_price'] - $row['service_charge'])*5/100);
                    $settlement = $row['total_price'] - $row['service_charge'] - $revenue;
                }

                $sheet->setCellValue('A'.$count, $no++);
                $sheet->setCellValue('B'.$count, $row['invoice_code']);
                $sheet->setCellValue('C'.$count, $row['status']);
                $sheet->setCellValue('D'.$count, $row['transaction_date']);
                $sheet->setCellValue('E'.$count, $row['phone']);
                $sheet->setCellValue('F'.$count, $row['email']);
                $sheet->setCellValue('G'.$count, $row['locker_name']);
                $sheet->setCellValue('H'.$count, $row['locker_size'].' / '.$row['locker_number']);
                $sheet->setCellValue('I'.$count, $this->getDuration($row['popsafes_id']));
                $sheet->setCellValue('J'.$count, $row['total_price']);
                $sheet->setCellValue('K'.$count, $row['total_promo']);
                $sheet->setCellValue('L'.$count, $row['total_paid']);
            }
        }

        $dateNow = date("ymd");
        $filename = $dateNow.' Order List.xlsx';
        
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xlsx");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'.$filename.'"');
        $writer->save("php://output");
        die();
    }

    public function getDuration($popsafeId)
    {
        $order = Popsafe::where('id', $popsafeId)->first();
        $currentStatus = $order->status;
        
        if($currentStatus == 'CREATED' || $currentStatus == 'CANCEL' || $currentStatus == 'EXPIRED') {
            $duration = ' ';
        } else {
            $sTime = PopsafeHistory::select('locker_time')
            ->where('popsafe_id', $popsafeId)
            ->where('status', 'IN STORE')
            ->first();
            
            if($sTime['locker_time'] == null) {
                $sTime = PopsafeHistory::select('created_at')
                ->where('popsafe_id', $popsafeId)
                ->where('status', 'IN STORE')
                ->first();
                $storeTime = $sTime['created_at'];
            } else {
                $storeTime = $sTime['locker_time'];
            }

            if($currentStatus == 'COMPLETE') {
                $takeTime = PopsafeHistory::where('popsafe_id', $popsafeId)
                ->where('status', $currentStatus)
                ->first()->created_at;
            } else {
                // IN STORE & OVERDUE
                $takeTime = date('Y-m-d H:i:s');
            }
            
            $diff = date_diff(date_create($storeTime), date_create($takeTime));
            if($diff->d > 0) {
                $hour = ($diff->d*24) + $diff->h;
            } else {
                $hour = $diff->h;
            }
            if($hour == 1) { $h = 'hour'; } else { $h = 'hours'; }
            if($diff->i == 1) { $i = 'minute'; } else { $i = 'minutes'; }
            $duration = $hour.' '.$h.' '.$diff->i.' '.$i;
        }
        
        return $duration;
    }
    
    public function showAnalytics(Request $request)
    {
        $lockerName = LockerLocation::orderBy('name','asc')->get()->pluck('name','locker_id')->toArray();
        $countries = ['Indonesia', 'Malaysia'];
        $year = range(date("Y"), 2018);
        
        $data = [
            'lockerNames'   => $lockerName,
            'countries'     => $countries,
            'year'          => $year
        ];
        return view('popapps.popsafe.analytics', $data);
    }

    public function dataAnalytics(Request $request)
    {
        /* foreach status, get popsafe transaction monthly
        SELECT status, count(invoice_code), MONTH(transaction_date) as Month 
        FROM popsend.popsafes
        WHERE YEAR(transaction_date) = '2019'
        GROUP BY status, MONTH(transaction_date)
         */
        $locker_name = $request->locker_name;
        if($request->year && $request->year != date('Y')) {
            $month = 12;
            $year = $request->year;
        } else {
            $month = date('n');
            $year = date('Y');
        }
        $date = $request->date;
        if($year != null) {
            $startDate = date($year.'-01-01 00:00:00');
            $endDate = date($year.'-12-31 23:59:59');
        } else {
            $date = explode(" - ", $date);
            $startDate = date($date[0].' 00:00:00');
            $endDate = date($date[1].' 23:59:59');
        }
        $status = Popsafe::distinct()->get(['status'])->pluck('status');
        
        foreach($status as $stat) {
            for($row = 1; $row <= $month; $row++) {
                $count = Popsafe::select(DB::raw('COUNT(invoice_code) as count'))
                ->where(DB::raw('YEAR(transaction_date)'), $year)
                ->where(DB::raw('MONTH(transaction_date)'), $row)
                ->when($locker_name, function($query) use($locker_name){
                    return $query->where('locker_name', $locker_name);
                })
                ->where('status', $stat)
                ->get('count')->pluck('count')->toArray();

                $usage[$stat][] = $count[0];
            }
        }

        $topLocation = Popsafe::select('locker_name',DB::raw('COUNT(locker_name) as count'))
        ->when($year, function($query) use($startDate, $endDate){
            return $query->whereBetween('transaction_date', [$startDate, $endDate]);
        })
        ->groupBy('locker_name')
        ->orderBy(DB::raw('COUNT(locker_name)'), 'desc')
        ->limit(10)
        ->get();
        
        $data = [
            'created' => $usage['CREATED'],
            'expired' => $usage['EXPIRED'],
            'cancel'  => $usage['CANCEL'],
            'instore'=> $usage['IN STORE'],
            'overdue' => $usage['OVERDUE'],
            'complete'=> $usage['COMPLETE'],
            'catTopLocation' => $topLocation->map->only(['locker_name'])->pluck('locker_name'),
            'valTopLocation' => $topLocation->map->only(['count'])->pluck('count')
        ];

        echo json_encode($data);
    }
}
