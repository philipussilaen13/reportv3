<?php

namespace App\Models\Popbox;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $connection = 'popbox_db';
    protected $table = 'cities';
}
