<?php

namespace App\Http\Controllers\Popbox;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\NewLocker\Box;
use App\Models\NewLocker\Company;
use App\Models\NewLocker\Express;
use App\Models\NewLocker\MachineStat;
use App\Models\NewLocker\Mouth;
use App\Models\NewLocker\SmsHistory;
use App\Models\Popbox\BuildingType;
use App\Models\Popbox\Country;
use App\Models\Popbox\District;
use App\Models\Popbox\LockerActivitiesAll;
use App\Models\Popbox\LockerDetailsData;
use App\Models\Popbox\LockerLocation;
use App\Models\Popbox\Province;
use App\Models\Popapps\Popsafe;
use App\Models\Popapps\User;
use App\Http\Helpers\Helper;
use App\Http\Helpers\ProxApi;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use App\Lib\Audit;
class LockerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $dateRange = $request->input('dateRange', null);
        // get all transaction this month *Default
        $startDate = date('Y-m-1');
        $endDate = date('Y-m-t');
        if (!empty($dateRange)) {
            $tmpDate = explode('-', $dateRange);
            $startDate = date('Y-m-d', strtotime($tmpDate[0]));
            $endDate = date('Y-m-d', strtotime($tmpDate[1]));
        }
        $startDate .= " 00:00:00";
        $endDate .= " 23:59:59";

        $param['beginDate'] = $startDate;
        $param['endDate'] = $endDate;

        $data['parameter'] = $param;

        return view('popbox.dashboard.aps', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|unique:popbox_db.locker_locations|max:255',
            'isPublished' => 'required'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $building_type = BuildingType::orderBy('building_type','asc')->get();
        $district = District::orderBy('district','asc')->get();
        // dd($district);
        $locker = LockerLocation::with(['buildingType','district'])->where('locker_id', $id)->first();
        $data = [
            'id' => $id,
            'building_type' => $building_type,
            'district' => $district,
            'data' => $locker
        ];
        return view('popbox.locker.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $locker_id = $request->locker_id;
        $validatedData = $request->validate([
            'name' => 'required|max:255',
            'building_type_id' => 'required',
            'operational_hours' => 'required',
            'locker_capacity' => 'required',
            'address' => 'required',
            'address_2' => 'required',
            'district_id' => 'required',
            'zip_code' => 'required|max:5',
            'latitude' => 'required',
            'longitude' => 'required',
            'image' => 'image|mimes:jpg,jpeg,png|max:1024',
            'floor_map' => 'image|mimes:jpg,jpeg,png|max:1024',
            'isPublished' => 'required'
        ]);

        if( $request->hasFile('image')){ 
            $image = $request->file('image'); 
            $fileName = $image->getClientOriginalName();
            $fileExtension = $image->getClientOriginalExtension();
            $string = str_replace(' ', '-', strtolower($request->name));
            $string = preg_replace('/[^\p{L}\p{N}\s]/u', '', $string);
            $locker = "locker_".$string.'.'.$fileExtension;
            $image->move(public_path('uploads/locker'), $locker);
        } else {
            $locker = $request->old_image;
        }

        if( $request->hasFile('floor_map')){ 
            $image = $request->file('floor_map'); 
            $fileName = $image->getClientOriginalName();
            $fileExtension = $image->getClientOriginalExtension();
            $string = str_replace(' ', '-', strtolower($request->name));
            $string = preg_replace('/[^\p{L}\p{N}\s]/u', '', $string);
            $floorMap = "floormap_".$string.'.'.$fileExtension;
            $image->move(public_path('uploads/floor_map'), $floorMap);
        } else {
            $floorMap = $request->old_floor_map;
        }
        
        $update = [
            'name' => $request->name,
            'building_type_id' => $request->building_type_id,
            'operational_hours' => $request->operational_hours,
            'locker_capacity' => $request->locker_capacity,
            'address' => $request->address,
            'address_2' => $request->address_2,
            'district_id' => $request->district_id,
            'zip_code' => $request->zip_code,
            'latitude' => $request->latitude,
            'longitude' => $request->longitude,
            'image' => $locker,
            'floor_map' => $floorMap,
            'isPublished' => $request->isPublished
        ];
        $query = LockerLocation::where('locker_id', $locker_id)->update($update);
        if($query) {
            return redirect('locker/edit/'.$locker_id)->with(['success'=> 'Locker data has been updated']);
        } else {
            return redirect('locker/edit/'.$locker_id)->with(['error'=> 'Updating locker data failed']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    // ========================================================================
    // ============================= Angkasa Pura =============================
    // ========================================================================

    public function APSLockerLocation(Request $request)
    {
        $input = $request->input();
        $name = $request->input('name',null);
        $type = $request->input('type',null);
        $country = $request->input('country',null);
        $status = $request->input('status');
      
        $user = Auth::user();

        $dataExpress = Express::select('box_id')->distinct('box_id')->where('groupName', 'AP_EXPRESS')->get();        
        
        // $lockerID = [];
        // foreach ($dataExpress as $key => $value) {
        //     $lockerID[] = $value->box_id;
        // }

        $lockerID = [
            '6073a2ebc6cfebfe7cba16ecc1b80294',
            '3404703e47ac8cb1ca645f87bee3c0b3',
            '92457d6a5e86c3c2c09e3b4898eee9c6'
        ];

        // get from box where delete flag 0
        $boxDb = Box::leftJoin('tb_newlocker_machinestat','tb_newlocker_machinestat.locker_id','=','tb_newlocker_box.id')
            ->whereIn('id', $lockerID)
            ->where('deleteFlag',0)
            ->get();

        $boxLockerIdList = [];
        foreach ($boxDb as $item) {
            $boxLockerIdList[] = $item->id;
        }
        
        // Get Locker Data versi Inka
        $lockerList = [];
        foreach($boxDb as $key => $boxLocker) {
            $getDataLocation = $this->getDataLocation($boxLocker->id);
            $lockerLocationDb = $getDataLocation->location;
            $countProvinceDistrict = $getDataLocation->province_district;
            
            // set lockerList
            $tmp = new \stdClass();
            $tmp->lockerId = $boxLocker->id;
            $tmp->name = $boxLocker->name;
            $tmp->type = $lockerLocationDb->building_type;
            $tmp->country = $lockerLocationDb->country;
            $tmp->address = $lockerLocationDb->address;
            $tmp->city = $lockerLocationDb->district.",".$lockerLocationDb->province;
            $tmp->operationalHour = $lockerLocationDb->operational_hours;
            $tmp->capacity = $lockerLocationDb->locker_capacity;
            $tmp->status = $boxLocker->conn_status;

            $lockerList[] = $tmp;
        }
        // dd($lockerList);

        // Get data Locker 
        // $getDataLocation = $this->getDataLocation($boxLockerIdList);
        // $lockerLocationDb = $getDataLocation->location;
        // $countProvinceDistrict = $getDataLocation->province_district;
        
        // $lockerList = [];
        // foreach ($boxDb as $boxLocker){
        //     $tmp = new \stdClass();
        //     $tmp->lockerId = $boxLocker->id;
        //     $tmp->name = $boxLocker->name;
        //     $tmp->type = null;
        //     $tmp->country = null;
        //     $tmp->address = null;
        //     $tmp->city = null;
        //     $tmp->operationalHour = null;
        //     $tmp->capacity = null;
        //     $tmp->status = $boxLocker->conn_status;

        //     // search on locker location DB
        //     $filtered = $lockerLocationDb->where('locker_id',$boxLocker->id);

        //     $filtered = $filtered->first();
        //     if ($filtered){
        //         $lockerName = $filtered->name;
        //         $lockerName = str_replace('PopBox @ ','',$lockerName);
        //         $tmp->name = $lockerName;
        //         $tmp->type =  $filtered->building_type;
        //         $tmp->country = $filtered->country;
        //         $tmp->address = $filtered->address;
        //         $tmp->city = $filtered->district.",".$filtered->province;
        //         $tmp->operationalHour = $filtered->operational_hours;
        //         $tmp->capacity = $filtered->locker_capacity;
        //     }

        //     $lockerList[] = $tmp;
        // }

        // get building type list
        $buildingTypeDb = BuildingType::get();

        $countryList = ['Indonesia','Malaysia'];

        $statusList = [
            '1' => 'Online',
            '0' => 'Offline'
        ];

        $param = [];
        foreach ($input as $key => $item) {
            $param[$key] = $item;
        }

        $compartmen = 0;
        $location = [];
        foreach ($lockerList as $key => $value) {
            $compartmen += $value->capacity;
            $location[] = $value->name;
        }

        $data = [];
        $data['parameter'] = $param;
        $data['lockerList'] = $lockerList;
        $data['buildingTypeList'] = $buildingTypeDb;
        $data['countryList'] = $countryList;
        $data['statusList'] = $statusList;
        $data['compartmen'] = $compartmen;
        $data['city'] = $countProvinceDistrict[0]['total_district'];
        $data['location'] = count($location);
        $data['province'] = $countProvinceDistrict[0]['total_province'];
        $data['user'] = $user;
        $data['groupUser'] = ['APXID'];
        
        return view('popbox.locker.location', $data);
    }

    public function getPaymentTransactions(Request $request)
    {
        $input = $request->input();
        $dateRange = $request->input('dateRange', null);
        // get all transaction this month *Default
        $startDate = date('Y-m-1');
        $endDate = date('Y-m-t');
        if (!empty($dateRange)) {
            $tmpDate = explode('-', $dateRange);
            $startDate = date('Y-m-d', strtotime($tmpDate[0]));
            $endDate = date('Y-m-d', strtotime($tmpDate[1]));
        }
        $startDate .= " 00:00:00";
        $endDate .= " 23:59:59";

        // get all parameter
        $lockerId = $request->input('locker', null);
        $status = $request->input('status', null);
        $type = $request->input('type', null);
        $transactionId = $request->input('transactionId', null);
        $itemParam = $request->input('itemParam', null);
        $itemReference = $request->input('itemReference', null);

        // create parameter to send to view for data processing
        $param = [];
        foreach ($input as $key => $item) {
            $param[$key] = $item;
        }
        $param['beginDate'] = $startDate;
        $param['endDate'] = $endDate;
        $param['dateRange'] = "$startDate - $endDate";

        // parsing data to view
        $data = [];
        $data['parameter'] = $param;
        // $data['lockerData'] = $lockerDb;

        return view('payment.summaryAPS', $data);
    }

    public function getAjaxGraph(Request $request)
    {
        set_time_limit(300);
        ini_set('memory_limit', '20480M');
        $input = $request->input();
        $dateRange = $request->input('dateRange', null);
        // get all transaction this month *Default
        $startDate = date('Y-m-1');
        $endDate = date('Y-m-t');
        if (!empty($dateRange)) {
            $tmpDate = explode('-', $dateRange);
            $startDate = date('Y-m-d', strtotime($tmpDate[0]));
            $endDate = date('Y-m-d', strtotime($tmpDate[1]));
        }
        $startDate .= " 00:00:00";
        $endDate .= " 23:59:59";

        $dayLabels = [];
        $emoneyDay = [];
        $ottoDay = [];
        $bniDay = [];
        $tcashDay = [];
        $gopayDay = [];
        $emoneyWeek = [];
        $ottoWeek = [];
        $bniWeek = [];
        $tcashWeek = [];
        $gopayWeek = [];
        $emoneyMonthly = [];
        $ottoMonthly = [];
        $bniMonthly = [];
        $tcashMonthly = [];
        $gopayMonthly = [];
        $dayCountTransaction = [];
        $daySumTransaction = [];
        $weekLabels = [];
        $weekCountTransaction = [];
        $weekSumTransaction = [];
        $weekPaymentMethod = [];
        $monthLabels = [];
        $monthCountTransaction = [];
        $monthSumTransaction = [];
        $monthPaymentMethod = [];
        $transactionList = [];
        $transactionCount = 0;
        $transactionAmount = 0;
        $topupAmount = 0;
        $topupCount = 0;
        $rewardAmount = 0;
        $rewardCount = 0;
        $pulsaCount = 0;
        $pulsaSum = 0;
        $paymentCount = 0;
        $paymentSum = 0;
        $popshopCount = 0;
        $popshopSum = 0;
        $deliveryCount = 0;
        $deliverySum = 0;

        $emoneyDayCount = [];
        $ottoDayCount = [];
        $tcashDayCount = [];
        $bniDayCount = [];
        $gopayDayCount = [];

        $emoneyWeekCount = [];
        $ottoWeekCount = [];
        $tcashWeekCount = [];
        $bniWeekCount = [];
        $gopayWeekCount = [];

        $emoneyMonthlyCount = [];
        $ottoMonthlyCount = [];
        $tcashMonthlyCount = [];
        $bniMonthlyCount = [];
        $gopayMonthlyCount = [];

        $dataTableDaily = [];
        $dataTableWeekly = [];
        $dataTableMonthly = [];

        $parcelDayCount = [];
        $parcelWeeklyCount = [];
        $parcelMonthlyCount = [];

        $typeTransaction = ['popshop', 'pulsa', 'electricity', 'admin_fee', 'electricity_postpaid', 'bpjs_kesehatan', 'pdam', 'telkom_postpaid', 'delivery'];
        $typeTopUp = ['topup'];
        $typeReward = ['reward', 'commission','referral'];

        $typePulsa = ['pulsa'];
        $typePopShop = ['popshop'];
        $typePayment = ['electricity', 'admin_fee', 'electricity_postpaid', 'bpjs_kesehatan', 'pdam', 'commission', 'telkom_postpaid'];
        $typeRefund = ['refund', 'deduct'];
        $typeDelivery = ['delivery'];

        $transactionsDb = DB::connection('popsend')
            ->table('transactions')
            ->leftJoin('transaction_items', 'transaction_items.transaction_id', '=', 'transactions.id')
            ->whereBetween('transactions.created_at', [$startDate, $endDate])
            ->where('transactions.status', 'PAID')
            ->where('transactions.transaction_type','popsafe')
            ->where('transactions.client_id_companies','!=','001')
            ->orderBy('transactions.created_at', 'desc')
            ->select(DB::raw('COUNT(transactions.id) as count, SUM(transaction_items.price) as sum'))
            ->first();
        
        if ($transactionsDb){
            $deliverySum = $transactionsDb->sum;
            $deliveryCount = $transactionsDb->count;
        }

        $transactionDayGroupDb = DB::connection('popsend')
            ->table('transactions')
            ->leftjoin('payments', 'payments.transactions_id', '=', 'transactions.id')
            ->leftjoin('transaction_items', 'transaction_items.transaction_id', '=', 'transactions.id')
            ->leftjoin('popsafes', 'popsafes.id', '=', 'transaction_items.item_reference')
            ->whereBetween('transactions.created_at', [$startDate, $endDate])
            ->where('transactions.status', 'PAID')
            ->where('transactions.client_id_companies','!=','001')
            ->whereIn('popsafes.locker_name',['DPS 01','DPS 02','SRG 01'])
            ->whereIn('transactions.transaction_type',['popsafe'])
            ->select(DB::raw('DATE(transactions.created_at) as date'), DB::raw('count(*) as count'), DB::raw('SUM(transactions.total_price) as sum'), DB::raw('SUM(CASE WHEN payments.payment_methods_id = \'4\' THEN payments.amount END) as emoney_payment, COUNT(CASE WHEN payments.payment_methods_id = \'4\' THEN payments.id END) as emoney_count'), DB::raw('SUM(transactions.total_price) as sum'), DB::raw('SUM(CASE WHEN payments.payment_methods_id = \'2\' THEN payments.amount END) as otto_payment, COUNT(CASE WHEN payments.payment_methods_id = \'2\' THEN payments.id END) as otto_count'),DB::raw('SUM(CASE WHEN payments.payment_methods_id = \'6\' THEN payments.amount END) as tcash_payment, COUNT(CASE WHEN payments.payment_methods_id = \'6\' THEN payments.id END) as tcash_count'),DB::raw('SUM(CASE WHEN payments.payment_methods_id = \'8\' THEN payments.amount END) as gopay_payment, COUNT(CASE WHEN payments.payment_methods_id = \'8\' THEN payments.id END) as gopay_count'), DB::raw('SUM(CASE WHEN payments.payment_methods_id = \'10\' THEN payments.amount END) as bni_payment, COUNT(CASE WHEN payments.payment_methods_id = \'10\' THEN payments.id END) as bni_count'), DB::raw('COUNT(distinct(popsafes.invoice_code)) as popsafe_invoice'))
            ->groupBy('date')
            ->get();

        foreach ($transactionDayGroupDb as $item){
            $dayCountTransaction[]=$item->count;
            $daySumTransaction[] = $item->sum;
            $dayLabels[] = $item->date;
            $parcelDayCount[] = $item->popsafe_invoice;

            $emoneyDay[] = ($item->emoney_payment == null) ? 0 : (int)$item->emoney_payment;
            $emoneyDayCount[] = ($item->emoney_count == null) ? 0 : (int)$item->emoney_count;

            $ottoDay[] = ($item->otto_payment == null) ? 0 : (int)$item->otto_payment;
            $ottoDayCount[] = ($item->otto_count == null) ? 0 : (int)$item->otto_count;

            $tcashDay[] = ($item->tcash_payment == null) ? 0 : (int)$item->tcash_payment;
            $tcashDayCount[] = ($item->tcash_count == null) ? 0 : (int)$item->tcash_count;

            $bniDay[] = ($item->bni_payment == null) ? 0 : (int)$item->bni_payment;
            $bniDayCount[] = ($item->bni_count == null) ? 0 : (int)$item->bni_count;

            $gopayDay[] = ($item->gopay_payment == null) ? 0 : (int)$item->gopay_payment;
            $gopayDayCount[] = ($item->gopay_count == null) ? 0 : (int)$item->gopay_count;
        }

        $transactionWeekGroupDb = DB::connection('popsend')
            ->table('transactions')
            ->leftjoin('payments', 'payments.transactions_id', '=', 'transactions.id')
            ->leftjoin('transaction_items', 'transaction_items.transaction_id', '=', 'transactions.id')
            ->leftjoin('popsafes', 'popsafes.id', '=', 'transaction_items.item_reference')
            ->whereBetween('transactions.created_at', [$startDate, $endDate])
            ->where('transactions.status', 'PAID')
            ->where('transactions.client_id_companies','!=','001')
            ->whereIn('popsafes.locker_name',['DPS 01','DPS 02','SRG 01'])
            ->whereIn('transactions.transaction_type',['popsafe'])
            ->select(DB::raw("CONCAT(YEAR(transactions.created_at), '/', WEEK(transactions.created_at,3)) as date"), DB::raw('count(*) as count'), DB::raw('SUM(CASE WHEN payments.payment_methods_id = \'4\' THEN payments.amount END) as emoney_payment, COUNT(CASE WHEN payments.payment_methods_id = \'4\' THEN payments.id END) as emoney_count'), DB::raw('SUM(transactions.total_price) as sum'), DB::raw('SUM(CASE WHEN payments.payment_methods_id = \'2\' THEN payments.amount END) as otto_payment, COUNT(CASE WHEN payments.payment_methods_id = \'2\' THEN payments.id END) as otto_count'),DB::raw('SUM(CASE WHEN payments.payment_methods_id = \'6\' THEN payments.amount END) as tcash_payment, COUNT(CASE WHEN payments.payment_methods_id = \'6\' THEN payments.id END) as tcash_count'),DB::raw('SUM(CASE WHEN payments.payment_methods_id = \'8\' THEN payments.amount END) as gopay_payment, COUNT(CASE WHEN payments.payment_methods_id = \'8\' THEN payments.id END) as gopay_count'), DB::raw('SUM(CASE WHEN payments.payment_methods_id = \'10\' THEN payments.amount END) as bni_payment, COUNT(CASE WHEN payments.payment_methods_id = \'10\' THEN payments.id END) as bni_count'), DB::raw('COUNT(distinct(popsafes.invoice_code)) as popsafe_invoice'))
            ->groupBy(DB::raw('date'))
            ->get();

        foreach ($transactionWeekGroupDb as $item){
            $weekCountTransaction[] = $item->count;
            $weekSumTransaction[] = $item->sum;
            list($year,$week) = explode('/',$item->date);
            $tmpLabel = Helper::getStartAndEndDate($week,$year);
            $tmpLabel = date('j M',strtotime($tmpLabel['week_start']))." - ".date('j M',strtotime($tmpLabel['week_end']));
            $weekLabels[] = $tmpLabel;
            // $weekPaymentMethod[] = $item->payment_method;
            $emoneyWeek[] = ($item->emoney_payment == null) ? 0 : (int)$item->emoney_payment;
            $emoneyWeekCount[] = ($item->emoney_count == null) ? 0 : (int)$item->emoney_count;
           
            $ottoWeek[] = ($item->otto_payment == null) ? 0 : (int)$item->otto_payment;
            $ottoWeekCount[] = ($item->otto_count == null) ? 0 : (int)$item->otto_count;
           
            $tcashWeek[] = ($item->tcash_payment == null) ? 0 : (int)$item->tcash_payment;
            $tcashWeekCount[] = ($item->tcash_count == null) ? 0 : (int)$item->tcash_count;

            $bniWeek[] = ($item->bni_payment == null) ? 0 : (int)$item->bni_payment;
            $bniWeekCount[] = ($item->bni_count == null) ? 0 : (int)$item->bni_count;

            $gopayWeek[] = ($item->gopay_payment == null) ? 0 : (int)$item->gopay_payment;
            $gopayWeekCount[] = ($item->gopay_count == null) ? 0 : (int)$item->gopay_count;

            $parcelWeeklyCount[] = $item->popsafe_invoice;
        }

        $transactionMonthGroupDb = DB::connection('popsend')
            ->table('transactions')
            ->leftjoin('payments', 'payments.transactions_id', '=', 'transactions.id')
            ->leftjoin('transaction_items', 'transaction_items.transaction_id', '=', 'transactions.id')
            ->leftjoin('popsafes', 'popsafes.id', '=', 'transaction_items.item_reference')
            ->whereBetween('transactions.created_at', [$startDate, $endDate])
            ->where('transactions.status', 'PAID')
            ->where('transactions.client_id_companies','!=','001')
            ->whereIn('popsafes.locker_name',['DPS 01','DPS 02','SRG 01'])
            ->whereIn('transactions.transaction_type',['popsafe'])
            ->select(DB::raw("MONTH(transactions.created_at) as month, YEAR(transactions.created_at) as year"), DB::raw('count(*) as count'), DB::raw('SUM(transactions.total_price) as sum'), DB::raw('SUM(CASE WHEN payments.payment_methods_id = \'4\' THEN payments.amount END) as emoney_payment, COUNT(CASE WHEN payments.payment_methods_id = \'4\' THEN payments.id END) as emoney_count'), DB::raw('SUM(transactions.total_price) as sum'), DB::raw('SUM(CASE WHEN payments.payment_methods_id = \'2\' THEN payments.amount END) as otto_payment, COUNT(CASE WHEN payments.payment_methods_id = \'2\' THEN payments.id END) as otto_count'),DB::raw('SUM(CASE WHEN payments.payment_methods_id = \'6\' THEN payments.amount END) as tcash_payment, COUNT(CASE WHEN payments.payment_methods_id = \'6\' THEN payments.id END) as tcash_count'),DB::raw('SUM(CASE WHEN payments.payment_methods_id = \'8\' THEN payments.amount END) as gopay_payment, COUNT(CASE WHEN payments.payment_methods_id = \'8\' THEN payments.id END) as gopay_count'), DB::raw('SUM(CASE WHEN payments.payment_methods_id = \'10\' THEN payments.amount END) as bni_payment, COUNT(CASE WHEN payments.payment_methods_id = \'10\' THEN payments.id END) as bni_count'), DB::raw('COUNT(distinct(popsafes.invoice_code)) as popsafe_invoice'))
            ->groupBy(DB::raw('YEAR(transactions.created_at) ASC,MONTH(transactions.created_at) ASC'))
            ->get();

        foreach ($transactionMonthGroupDb as $item){
            $monthCountTransaction[] = $item->count;
            $monthSumTransaction[] = $item->sum;
            $year = $item->year;
            $month = $item->month;
            $tmpLabel = Helper::getNameFromMonth($month);
            $tmpLabel = "$tmpLabel $year";
            $monthLabels[] = $tmpLabel;
            // $monthPaymentMethod[] = $item->payment_method;
            $emoneyMonthly[] = ($item->emoney_payment == null) ? 0 : (int)$item->emoney_payment;
            $emoneyMonthlyCount[] = ($item->emoney_count == null) ? 0 : (int)$item->emoney_count;

            $ottoMonthly[] = ($item->otto_payment == null) ? 0 : (int)$item->otto_payment;
            $ottoMonthlyCount[] = ($item->otto_count == null) ? 0 : (int)$item->otto_count;
            
            $tcashMonthly[] = ($item->tcash_payment == null) ? 0 : (int)$item->tcash_payment;
            $tcashMonthlyCount[] = ($item->tcash_count == null) ? 0 : (int)$item->tcash_count;
           
            $bniMonthly[] = ($item->bni_payment == null) ? 0 : (int)$item->bni_payment;
            $bniMonthlyCount[] = ($item->bni_count == null) ? 0 : (int)$item->bni_count;

            $gopayMonthly[] = ($item->gopay_payment == null) ? 0 : (int)$item->gopay_payment;
            $gopayMonthlyCount[] = ($item->gopay_count == null) ? 0 : (int)$item->gopay_count;

            $parcelMonthlyCount[] = $item->popsafe_invoice;
        }

        // get data float
        $floatingDeposit = 0;
        $countActiveAgent = 0;
        $countAllAgent = 0;

        $paidTransactionItems = DB::connection('popsend')
            ->table('transactions')
            ->leftJoin('transaction_items', 'transactions.id', '=', 'transaction_items.transaction_id')
            ->whereBetween('transactions.created_at', [$startDate, $endDate])
            ->where('transactions.status', 'PAID')
            ->orderBy('transactions.created_at', 'desc')
            ->select('transaction_items.transaction_id','transaction_items.price','transaction_items.created_at','transactions.transaction_type as trans_type','transactions.total_price')
            ->get();

        $rewardCount = 0;
        $rewardCount = 0;
        $transactionAmount = 0;
        $transactionCount = 0;
        $pulsaCount = 0;
        $pulsaSum = 0;
        $paymentSum = 0;
        $paymentCount = 0;
        $topupAmount = 0;
        $topupCount = 0;

        foreach ($dayLabels as $key => $value) {
            $dataTableDaily[] = [
                'periode' => $value,
                'totalParcel' => $parcelDayCount[$key],
                'totalTrx' => $bniDayCount[$key] + $emoneyDayCount[$key] + $tcashDayCount[$key] + $ottoDayCount[$key] + $gopayDayCount[$key],
                'bni' => $bniDay[$key],
                'emoney' => $emoneyDay[$key],
                'tcash' => $tcashDay[$key],
                'ottopay' => $ottoDay[$key],
                'gopay' => $gopayDay[$key],
                'total' => $bniDay[$key] + $emoneyDay[$key] + $tcashDay[$key] + $ottoDay[$key] + $gopayDay[$key]

            ];
        }

        foreach ($weekLabels as $key => $value) {
            $dataTableWeekly[] = [
                'periode' => $value,
                'totalParcel' => $parcelWeeklyCount[$key],
                'totalTrx' => $bniWeekCount[$key] + $emoneyWeekCount[$key] + $tcashWeekCount[$key] + $ottoWeekCount[$key] + $gopayWeekCount[$key],
                'bni' => $bniWeek[$key],
                'emoney' => $emoneyWeek[$key],
                'tcash' => $tcashWeek[$key],
                'ottopay' => $ottoWeek[$key],
                'gopay' => $gopayWeek[$key],
                'total' => $bniWeek[$key] + $emoneyWeek[$key] + $tcashWeek[$key] + $ottoWeek[$key] + $gopayWeek[$key]

            ];
        }
        
        foreach ($monthLabels as $key => $value) {
            $dataTableMonthly[] = [
                'periode' => $value,
                'totalParcel' => $parcelMonthlyCount[$key],
                'totalTrx' => $bniMonthlyCount[$key] + $emoneyMonthlyCount[$key] + $tcashMonthlyCount[$key] + $ottoMonthlyCount[$key] + $gopayMonthlyCount[$key],
                'bni' => $bniMonthly[$key],
                'emoney' => $emoneyMonthly[$key],
                'tcash' => $tcashMonthly[$key],
                'ottopay' => $ottoMonthly[$key],
                'gopay' => $gopayMonthly[$key],
                'total' => $bniMonthly[$key] + $emoneyMonthly[$key] + $tcashMonthly[$key] + $ottoMonthly[$key] + $gopayMonthly[$key]

            ];
        }

        // parse for response
        $data['dayLabels'] = $dayLabels;
        $data['dayCountTransaction'] = $dayCountTransaction;
        $data['daySumTransaction'] = $daySumTransaction;
       
        $data['weekLabels'] = $weekLabels;
        $data['weekCountTransaction'] = $weekCountTransaction;
        $data['weekSumTransaction'] = $weekSumTransaction;
        $data['weekMethodPayment'] = $weekPaymentMethod;
       
        $data['monthLabels'] = $monthLabels;
        $data['monthCountTransaction'] = $monthCountTransaction;
        $data['monthSumTransaction'] = $monthSumTransaction;
        $data['monthPaymentMethod'] = $monthPaymentMethod;
    
        $data['transactionCount'] = $transactionCount;
        $data['transactionAmount'] = $transactionAmount;
        $data['topupAmount'] = $topupAmount;
        $data['topupCount'] = $topupCount;
        $data['rewardAmount'] = $rewardAmount;
        $data['rewardCount'] = $rewardCount;

        $data['pulsaCount'] = $pulsaCount;
        $data['pulsaSum'] = $pulsaSum;
        $data['paymentCount'] = $paymentCount;
        $data['paymentSum'] = $paymentSum;
        $data['popshopCount'] = $popshopCount;
        $data['popshopSum'] = $popshopSum;
        $data['deliveryCount'] = $deliveryCount;
        $data['deliverySum'] = $deliverySum;

        $data['emoneyDay'] = $emoneyDay;
        $data['ottoDay'] = $ottoDay;
        $data['tcashDay'] = $tcashDay;
        $data['bniDay'] = $bniDay;
        $data['gopayDay'] = $gopayDay;

        $data['emoneyDayCount'] = $emoneyDayCount;
        $data['ottoDayCount'] = $ottoDayCount;
        $data['tcashDayCount'] = $tcashDayCount;
        $data['bniDayCount'] = $bniDayCount;
        $data['gopayDayCount'] = $gopayDayCount;

        $data['emoneyWeek'] = $emoneyWeek;
        $data['ottoWeek'] = $ottoWeek;
        $data['tcashWeek'] = $tcashWeek;
        $data['bniWeek'] = $bniWeek;
        $data['gopayWeek'] = $gopayWeek;

        $data['emoneyWeekCount'] = $emoneyWeekCount;
        $data['ottoWeekCount'] = $ottoWeekCount;
        $data['tcashWeekCount'] = $tcashWeekCount;
        $data['bniWeekCount'] = $bniWeekCount;
        $data['gopayWeekCount'] = $gopayWeekCount;

        $data['emoneyMonthly'] = $emoneyMonthly;
        $data['ottoMonthly'] = $ottoMonthly;
        $data['tcashMonthly'] = $tcashMonthly;
        $data['bniMonthly'] = $bniMonthly;
        $data['gopayMonthly'] = $gopayMonthly;

        $data['emoneyMonthlyCount'] = $emoneyMonthlyCount;
        $data['ottoMonthlyCount'] = $ottoMonthlyCount;
        $data['tcashMonthlyCount'] = $tcashMonthlyCount;
        $data['bniMonthlyCount'] = $bniMonthlyCount;
        $data['gopayMonthlyCount'] = $gopayMonthlyCount;

        $data['floatingDeposit'] = $floatingDeposit;
        $data['countActiveAgent'] = $countActiveAgent;
        $data['countAllAgent'] = $countAllAgent;

        // DATATABLES
        $data['dataTableDaily'] = $dataTableDaily;
        $data['dataTableWeekly'] = $dataTableWeekly;
        $data['dataTableMonthly'] = $dataTableMonthly;

        $data['isSuccess'] = true;

        return response()->json($data);
    }

    public function changePhoneNumber(Request $request)
    {

        $parameter = $request->route()->parameters();
        $checkUserExist = $this->checkUserByPhone($request->input('old-phone'), $request->input('new-phone'), $parameter['invoice_id']);
        
        $data = Express::where('expressNumber', $parameter['invoice_id'])->first();
        
        $audit = [
            "before"=>json_encode([
                "invoice_id"=>$parameter['invoice_id'],
                "parcel_number"=>$data->id,
                "phone"=>$request->input('old-phone'),
            ]),
            "after"=>json_encode([
                "invoice_id"=>$parameter['invoice_id'],
                "parcel_number"=>$data->id,
                "phone"=>$request->input('new-phone'),
                "reason"=>$request->input('reason')
            ])
        ];
        
        if ($data) {
            $proxApi = new ProxApi;
            $res = $proxApi->changePhoneNumber($data->id, $request->input('new-phone'));
            
            if (property_exists((object)$res, 'statusCode')) {
                return back()->with('failed','Failed Update Phone');
            } else {
                $popsafe = new Popsafe;
                $add_history = $popsafe->addHistoryChangePhoneNumber($parameter['invoice_id'], $request->input('new-phone'), $request->input('old-phone'), $request->input('reason'));
                Audit::update($data->id,'Delivery Activity Detail',$audit,'Change phone number');
                $request->session()->flash('success','Success Update Phone');
                return back()->with('success','Success Update Phone');
            }
        }
    }

    public function checkUserByPhone($oldPhoneNumber, $newPhoneNumber, $invoiceCode)
    {
        $userModel = new User();
        $userDB = $userModel->checkUserExist($newPhoneNumber);
        
        if ($userDB) {
            $userNew = $userModel->getUserByPhone($newPhoneNumber);
        } else {
            $createNew = $userModel->insertNewUser($newPhoneNumber, $newPhoneNumber, $newPhoneNumber."@email.com",$newPhoneNumber);

            $userNew = new \stdClass();
            $userNew->id = $createNew->id;
        }

        $popsafeModel = new Popsafe;
        $popsafe = $popsafeModel->switchUserParcel($invoiceCode, $userNew);
        
        return $popsafe;
        
    }

    public function getDataLocation($boxLockerIdList)
    {
        $response = new \stdClass();
        $response->location = null;
        $response->province_district = null;

        if (!is_array($boxLockerIdList)) {
            $boxLockerIdList = [$boxLockerIdList];
        }

        // get based on locker location
        $lockerLocationDb = LockerLocation::join('districts','locker_locations.district_id','=','districts.id')
            ->join('provinces','provinces.id','=','districts.province_id')
            ->join('countries','countries.id','=','provinces.country_id')
            ->join('buildingtypes','buildingtypes.id_building','=','locker_locations.building_type_id')
            ->whereIn('locker_id',$boxLockerIdList)
            ->first();
        
        // count based on locker location
        $countProvinceDistrict = LockerLocation::join('districts','locker_locations.district_id','=','districts.id')
            ->join('provinces','provinces.id','=','districts.province_id')
            ->whereIn('locker_id',$boxLockerIdList)
            ->select(DB::raw('COUNT(DISTINCT(district)) as total_district'), DB::raw('COUNT(DISTINCT(districts.province_id)) as total_province'))
            ->get()->toArray();

        $response->location = $lockerLocationDb;
        $response->province_district = $countProvinceDistrict;
        return $response;
    }

    public function boxInfo(Request $request)
    {
        $parameter = $request->route()->parameters();
        $box_id = $parameter['box_id'];
        $doorLocker = [];
        $currentStatus = [];


        $proxApi = new ProxApi;
        $res = $proxApi->getInfoBox($box_id); 
        // dd($res);
        if ($res){
            // All Locker Cabinets (Door)
            $doorLocker = $res->cabinets;
            $currentStatus = $res->currentStatus;
        }

        $mouth = [];
        foreach ($doorLocker as $keySort => $mouthSort) {
            $mouth[] = (array)$mouthSort;
        }
        usort($mouth, function($a, $b) {
            return $a['number'] <=> $b['number'];
        });
        $doorLocker = [];
        $doorLocker = json_decode(json_encode($mouth));        
        foreach ($doorLocker as $key => $mouthData) {
            $mouth[] = (array)$mouthData;
            $cabinet = [];
            foreach ($mouthData->mouths as $keyCabinet => $dataCabinet) {
                $cabinet[] = (array)$dataCabinet;
            }
            $doorLocker[$key]->mouths = [];
            usort($cabinet, function($a, $b) {
                return $a['number'] <=> $b['number'];
            });
            $doorLocker[$key]->mouths = json_decode(json_encode($cabinet));
        }
        
        // count door status summary
        $usedXS = 0;
        $usedS = 0;
        $usedM = 0;
        $usedL = 0;
        $usedXL = 0;

        $totalXS = 0;
        $totalS = 0;
        $totalM = 0;
        $totalL = 0;
        $totalXL = 0;

        $countXS = 0;
        $countS = 0;
        $countM = 0;
        $countL = 0;
        $countXL = 0;

        $locked = 0;
        $used = 0;
        $total = 0;

        foreach ($doorLocker as $row) {
            foreach($row->mouths as $mouth) {
                // count used
                if($mouth->status == "USED") {
                    if($mouth->mouthType->name == "MINI") {
                        $usedXS++;
                    } else if($mouth->mouthType->name == "S") {
                        $usedS++;
                    } else if($mouth->mouthType->name == "M") {
                        $usedM++;
                    } else if($mouth->mouthType->name == "L") {
                        $usedL++;
                    } else if($mouth->mouthType->name == "XL") {
                        $usedXL++;
                    } else {
                        // do nothing
                    }
                    $used++;
                }
                // count total
                if($mouth->status != "LOCKED") {
                    if($mouth->mouthType->name == "MINI") {
                        $totalXS++;
                    } else if($mouth->mouthType->name == "S") {
                        $totalS++;
                    } else if($mouth->mouthType->name == "M") {
                        $totalM++;
                    } else if($mouth->mouthType->name == "L") {
                        $totalL++;
                    } else if($mouth->mouthType->name == "XL") {
                        $totalXL++;
                    } else {
                        // do nothing
                    }
                    $total++;
                } else {
                    $locked++;
                }
            }
        }

        $countXS = ceil($usedXS/max($totalXS, 1)*100);
        $countS = ceil($usedS/max($totalS, 1)*100);
        $countM = ceil($usedM/max($totalM, 1)*100);
        $countL = ceil($usedL/max($totalL, 1)*100);
        $countXL = ceil($usedXL/max($totalXL, 1)*100);

        if($countXS % 5 != 0) { $modXS = $countXS + (5 - ($countXS % 5)); } else { $modXS = $countXS; }
        if($countS % 5 != 0) { $modS = $countS + (5 - ($countS % 5)); } else { $modS = $countS; }
        if($countM % 5 != 0) { $modM = $countM + (5 - ($countM % 5)); } else { $modM = $countM; }
        if($countL % 5 != 0) { $modL = $countL + (5 - ($countL % 5)); } else { $modL = $countL; }
        if($countXL % 5 != 0) { $modXL = $countXL + (5 - ($countXL % 5)); } else { $modXL = $countXL; }

        $summary = array(
            'usedXS' => $usedXS,
            'usedS' => $usedS,
            'usedM' => $usedM,
            'usedL' => $usedL,
            'usedXL' => $usedXL,
            'totalXS' => $totalXS,
            'totalS' => $totalS,
            'totalM' => $totalM,
            'totalL' => $totalL,
            'totalXL' => $totalXL,
            'countXS' => $countXS,
            'countS' => $countS,
            'countM' => $countM,
            'countL' => $countL,
            'countXL' => $countXL,
            'modXS' => $modXS,
            'modS' => $modS,
            'modM' => $modM,
            'modL' => $modL,
            'modXL' => $modXL,
            'used' => $used,
            'total' => $total,
            'locked' => $locked
        );
        
        // Get Data Locker
        $getDataLocation = $this->getDataLocation($box_id);
        $location = $getDataLocation->location;

        $summary_locker_activity = LockerActivitiesAll::select(DB::raw('count(locker_activities_all.id) as delivery, ROUND((count(locker_activities_all.id)/(DATEDIFF(CURRENT_DATE(), locker_locations.activation_date))),2) as avg_daily_usage'))
        ->leftJoin('locker_locations','locker_activities_all.locker_name','=','locker_locations.name')
        ->where('locker_name', $location->name)
        ->groupBy('locker_name')->first();
        
        $data['location'] = $location;
        $data['lockerStatus'] = $currentStatus->online;
        $data['doorLocker'] = $doorLocker;
        $data['summary'] = $summary;
        $data['lockerActivity'] = $summary_locker_activity;
        // dd($doorLocker);
        return view('popbox.locker.detail', $data);        
    }

    public function getLockerList(Request $request)
    {
        $boxDb = Box::with(['location','machineStat'])
        ->where('deleteFlag',0)
        ->get();
        
        $lockerList = [];
        foreach ($boxDb as $box) {
            // get type
            $type = BuildingType::where('id_building', $box->location['building_type_id'])->first();
            // check country
            if($box->currencyUnit == 'IDR') {
                $country = 'Indonesia';
            } else {
                $country = 'Malaysia';
            }

            $lockerList[] = array(
                'locker_id' => $box->id,
                'name' => $box->name,
                'type' => $type['building_type'],
                'country' => $country,
                'address' => $box->location['address'],
                'operationalHours' => $box->location['operational_hours'],
                'capacity' => $box->location['locker_capacity'],
                'status' => $box->machineStat['conn_status'],
                'availability' => $box->location['isPublished'],
            );
        }
        // return $lockerList;
        echo json_encode($lockerList);
    }

    public function getAjaxLockerList(Request $request)
    {
        $columns = array(
            0   => 'name',
            1   => 'country',
            2   => 'address',
            3   => 'kecamatan',
            4   => 'city',
            5   => 'province',
            6   => 'building_type',
            7   => 'operational_hours',
            8   => 'capacity',
            9   => 'availability',
            10  => 'activation'
        );

        $start = $request->start;
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $limit = isset($request->length) ? $request->length : 10;
        $currentPage = isset($start) ? ($start/$limit)+1 : 1;
        
        $name = $request->name;
        $locker_id = $request->locker_id;
        $type = $request->type;
        $country = $request->country;
        $availability = $request->availability;
        $activation = $request->activation;
        $zip_code = $request->zip_code;
        $province = $request->province;
        $district = $request->district;
        // $activationDate = $request->date;

        $totalData = LockerLocation::with(['availabilityReport','buildingType','district'])->count();
        $allData = LockerLocation::with(['availabilityReport','buildingType','district'])
        ->when($name,function($query) use($name){
            return $query->where('locker_locations.name','LIKE',"%$name%");
        })
        ->when($locker_id,function($query) use($locker_id){
            return $query->where('locker_locations.locker_id','LIKE',"%$locker_id%");
        })
        ->when($district,function($query) use($district){
            return $query->whereIn('locker_locations.district_id', $district);
        })
        ->when($province,function($query) use($province){
            return $query->whereHas('district', function ($q) use($province) {
                $q->whereIn('province_id', $province);
            });
        })
        ->when($zip_code,function($query) use($zip_code){
            return $query->where('locker_locations.zip_code','LIKE',"%$zip_code%");
        })
        ->when($country,function($query) use($country){
            return $query->whereHas('availabilityReport', function ($q) use($country) {
                $q->where('country', $country);
            });
        })
        ->when($availability,function($query) use($availability){
            return $query->whereHas('availabilityReport', function ($q) use($availability) {
                if($availability == "Online") {
                    $q->where('status', 'ENABLE');
                } else {
                    $q->where('status', 'EXCEPTION');
                } 
            });
        })
        ->when($activation,function($query) use($activation){
            if($activation == "Active") {
                return $query->where('isPublished', '1');
            } else {
                return $query->where('isPublished', '0');
            } 
        })
        ->when($type,function($query) use($type){
            return $query->whereHas('buildingType', function ($q) use($type) {
                $q->where('building_type', $type);
            });
        })
        ->orderBy($order, $dir)
        ->paginate($limit,['*'],'draw',$currentPage);

        $data = [];
        foreach($allData->items() as $post) {
            $nestedData['locker_id'] = $post->locker_id;
            $nestedData['name'] = $post->name;
            $nestedData['address'] = $post->address;
            $nestedData['kecamatan'] = $post->kecamatan;
            $nestedData['operational_hours'] = $post->operational_hours;
            $nestedData['capacity'] = $post->locker_capacity;
            $nestedData['activation'] = $post->isPublished;

            if($post->availabilityReport != null) {
                $nestedData['country'] = $post->availabilityReport->country;
                $nestedData['city'] = $post->availabilityReport->city;
                $nestedData['province'] = $post->availabilityReport->province;
                $nestedData['availability'] = $post->availabilityReport->status;
            } else {
                $nestedData['country'] = '';
                $nestedData['city'] = '';
                $nestedData['province'] = '';
                $nestedData['availability'] = '';
            }

            if($post->buildingType != null) {
                $nestedData['building_type'] = $post->buildingType->building_type;
            } else {
                $nestedData['building_type'] = '';
            }

            $data[] = $nestedData;
        }
        $totalFiltered = $allData->total();
    
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );
        return($json_data);
    }

    public function viewLockerList(Request $request)
    {
        $type = BuildingType::orderBy('building_type')->get();
        $district = District::orderBy('district')->get();
        $province = Province::orderBy('province')->get();
        $country = Country::all();
        $availability = [
            '1' => 'Online',
            '0' => 'Offline'
        ];
        $activation = [
            '1' => 'Active',
            '0' => 'Not Active'
        ];

        $data = array(
            'types' => $type,
            'districts' => $district,
            'provinces' => $province,
            'countries' => $country,
            'availability' => $availability,
            'activation' => $activation,
        );

        return view('popbox.locker.list', $data);
    }
    
    public function postAjaxSummaryLockerList(Request $request)
    {
        $name = $request->name;
        $locker_id = $request->locker_id;
        $type = $request->type;
        $country = $request->country;
        $availability = $request->availability;
        $activation = $request->activation;
        $zip_code = $request->zip_code;
        $province = $request->province;
        $district = $request->district;

        /* $lockerID = Box::with(['machineStat'])
        ->where('deleteFlag',0)
        ->when($name,function($query) use($name){
            return $query->where('tb_newlocker_box.name','LIKE',"%$name%");
        })
        ->when($country,function($query) use($country){
            if($country == "Indonesia") {
                return $query->where('tb_newlocker_box.currencyUnit','IDR');
            } else {
                return $query->where('tb_newlocker_box.currencyUnit','MYR');
            }
        })
        ->when($status,function($query) use($status){
            return $query->whereHas('machineStat', function ($q) use($status) {
                if($status == "Active") {
                    $q->where('conn_status', 1);
                } else {
                    $q->where('conn_status', 0);
                } 
            });
        })
        ->pluck('tb_newlocker_box.id');
        // dd($lockerID);
        $countSummary = LockerLocation::join('districts','locker_locations.district_id','=','districts.id')
        ->join('provinces','provinces.id','=','districts.province_id')
        ->join('buildingtypes','locker_locations.building_type_id','=','buildingtypes.id_building')
        ->whereIn('locker_locations.locker_id', $lockerID)
        ->when($type,function($query) use($type){
            return $query->where('popbox_db.buildingtypes.building_type','LIKE',"%$type%");
        })
        ->when($availability,function($query) use($availability){
            if($availability == "Online") {
                return $query->where('popbox_db.locker_locations.isPublished', 1);
            } else {
                return $query->where('popbox_db.locker_locations.isPublished', 0);
            }  
        })
        ->select(DB::raw('SUM(locker_locations.locker_capacity) as total_compartment'), DB::raw('COUNT(DISTINCT(locker_locations.name)) as total_location'), DB::raw('COUNT(DISTINCT(district)) as total_district'), DB::raw('COUNT(DISTINCT(districts.province_id)) as total_province'))
        ->get()->toArray(); */

        $countSummary = LockerLocation::with(['availabilityReport','buildingType'])
        ->join('districts','locker_locations.district_id','=','districts.id')
        ->select(DB::raw('SUM(locker_locations.locker_capacity) as total_compartment'), DB::raw('COUNT(DISTINCT(locker_locations.locker_id)) as total_location'), DB::raw('COUNT(DISTINCT(districts.district)) as total_district'), DB::raw('COUNT(DISTINCT(districts.province_id)) as total_province'))
        ->when($name,function($query) use($name){
            return $query->where('locker_locations.name','LIKE',"%$name%");
        })
        ->when($locker_id,function($query) use($locker_id){
            return $query->where('locker_locations.locker_id','LIKE',"%$locker_id%");
        })
        ->when($district,function($query) use($district){
            return $query->whereIn('locker_locations.district_id', $district);
        })
        ->when($province,function($query) use($province){
            // return $query->whereIn('districts.province_id', $province);
            return $query->whereHas('district', function ($q) use($province) {
                $q->whereIn('province_id', $province);
            });
        })
        ->when($zip_code,function($query) use($zip_code){
            return $query->where('locker_locations.zip_code','LIKE',"%$zip_code%");
        })
        ->when($country,function($query) use($country){
            return $query->whereHas('availabilityReport', function ($q) use($country) {
                $q->where('country', $country);
            });
        })
        ->when($availability,function($query) use($availability){
            return $query->whereHas('availabilityReport', function ($q) use($availability) {
                if($availability == "Online") {
                    $q->where('status', 'ENABLE');
                } else {
                    $q->where('status', 'EXCEPTION');
                } 
            });
        })
        ->when($activation,function($query) use($activation){
            if($activation == "Active") {
                return $query->where('isPublished', '1');
            } else {
                return $query->where('isPublished', '0');
            } 
        })
        ->when($type,function($query) use($type){
            return $query->whereHas('buildingType', function ($q) use($type) {
                $q->where('building_type', $type);
            });
        })
        ->get()->toArray();

        echo json_encode($countSummary);
    }

    public function parcelDetail(Request $request)
    {
        $param = $request->route()->parameters();
       
        $mouth = Mouth::join('tb_newlocker_mouthtype','tb_newlocker_mouth.mouthType_id','=','tb_newlocker_mouthtype.id_mouthtype')
        ->where('id_mouth',$param['mouth_id'])
        ->first();
        $locker = Box::where('id',$mouth['box_id'])->first();
        // if($mouth['name'] == 'MINI') {
        //     $size = 'XS';
        // } else {
        //     $size = $mouth['name'];
        // }
        $data['locker'] = array(
            'id' => $locker['id'],
            'name' => $locker['name'],
            'size' => $mouth['name'],
            'number' => $mouth['number'],
            'status' => $mouth['status'],
            'mouth_id' => $mouth['id_mouth']
        );

        if($mouth['status'] == "USED") {
            $data['express'] = Express::where('mouth_id', $param['mouth_id'])
            ->where('status','IN_STORE')
            ->first();
        
        } else {
            $data['express'] = [];
        }

        $data['locker_size'] = array(
            'MINI' => 'MINI',
            'S' => 'S',
            'M' => 'M',
            'L' => 'L',
            'XL' => 'XL'
        );
        // dd($data);
        return view('popbox.locker.parcel_detail', $data);
    }

    public function remoteOpenDoor(Request $request)
    {
        $param = $request->route()->parameters();
        $proxApi = new ProxApi;
        $res = $proxApi->remoteUnlock($param['mouth_id']); 

        if($res->response->code == 200) {
            $message = "Locker door is open.";
            // return redirect('locker/cabinet'.'/'.$param['mouth_id'])->with(['success'=> "Locker door is open."]);
        } else {
            $message = "You don't have permission to open the locker door.";
        }
        echo json_encode($message);
    }

    public function updateDoorStatus(Request $request)
    {
        $mouth_id = $request->input('mouth_id');
		$status = $request->input('status');
		$old_size = $request->input('old_size');
        $new_size = $request->input('new_size');
        
        $proxApi = new ProxApi;
        if(!empty($new_size) && $old_size !== $new_size) {
            // change locker size
            $res = $proxApi->changeDoorSize($mouth_id, $new_size);
            if($res->response->code == 200) {
                $message = "Locker door size successfully changed.";
            }
        } else {
            // change locker status
            if($status == "ENABLE") {
                $new_status = "LOCKED";
            } else {
                $new_status = "ENABLE";
            }
            
            $res = $proxApi->changeDoorStatus($mouth_id, $new_status);
            if($res->response->code == 200) {
                $message = "Locker door status successfully changed.";
            }
        }
        echo json_encode($message);
        // return redirect('locker/cabinet'.'/'.$mouth_id)->with(['success'=> $message]);
    }

    public function remoteOpenDoorByExpress(Request $request)
    {
        $param = $request->route()->parameters();
        
        $proxApi = new ProxApi;
        $res = $proxApi->remoteUnlockByExpress($param['parcel_id']); 

        if($res->response->code == 200) {
            $message = "Locker door is open.";
            // return redirect('locker/cabinet'.'/'.$param['mouth_id'])->with(['success'=> "Locker door is open."]);
        } else {
            $message = "You don't have permission to open the locker door.";
        }

        $audit = json_encode([
            "express_id"=>$param['parcel_id'],
            "message"=>$message,
        ]);
        Audit::create($param['parcel_id'],'Delivery Activity Detail',$audit,'Open Locker Door');

        echo json_encode($message);
    }
    
    public function postChangeOverdue(Request $request){
        $rules = [
            'parcelId' => 'required',
            'overdueDate' => 'required|date'
        ];
        
        $this->validate($request,$rules);

        $parcelId = $request->input('parcelId');
        $overdueDate = $request->input('overdueDate');
        $data = Express::where('id', $parcelId)->first();
        $oldOverdue = $data->overdueTime;
        if($data->expressNumber != null) {
            $expressNumber = $data->expressNumber;
        } else {
            $expressNumber = $data->customerStoreNumber;
        }
        // check on locker activities
        /*$lockerActivitiesDb = LockerActivities::find($parcelId);
        if (!$lockerActivitiesDb){
            $request->session()->flash('error','Parcel Id Not Found on Locker Activities');
            return back();
        }*/

        // generate unix milisecond overdue date time
        $overdueDate = date('Y-m-d 23:00:00',strtotime($overdueDate));
        $unixOverdueDateTime = strtotime($overdueDate) * 1000;

        // post to new locker prox
        $proxApi = new ProxApi;
        $changeOverdue = $proxApi->changeOverdueDateTime($parcelId,$unixOverdueDateTime);

        $audit = [
            "before"=>json_encode([
                "express_id"=>$parcelId,
                "express_number"=>$expressNumber,
                "overdue_date"=>$oldOverdue,
            ]),
            "after"=>json_encode([
                "express_id"=>$parcelId,
                "express_number"=>$expressNumber,
                "overdue_date"=>$unixOverdueDateTime
            ])
        ];

        if (empty($changeOverdue)){
            $request->session()->flash('error','Failed Change Overdue');
            return back();
        }
        if ($changeOverdue->response->code != 200){
            $request->session()->flash('error',$changeOverdue->response->message);
            return back();
        } else {
            Audit::update($data->id,'Delivery Activity Detail',$audit,'Change Overdue');
        }

        // change on locker activities
        /*$lockerActivitiesDb->overduetime = $overdueDate;
        $lockerActivitiesDb->save();*/

        $request->session()->flash('success','Success Change Overdue');
        return back();
    }

    public function downloadExcel(Request $request)
    {
        $name = $request->name;
        $locker_id = $request->locker_id;
        $type = $request->type;
        $country = $request->country;
        $availability = $request->availability;
        $activation = $request->activation;
        $zip_code = $request->zipCode;
        $province = $request->province;
        $district = $request->district;
        
        $data = LockerLocation::with(['availabilityReport','buildingType','district','district.province'])
        ->when($name,function($query) use($name){
            return $query->where('locker_locations.name','LIKE',"%$name%");
        })
        ->when($locker_id,function($query) use($locker_id){
            return $query->where('locker_locations.locker_id','LIKE',"%$locker_id%");
        })
        ->when($district,function($query) use($district){
            return $query->whereIn('locker_locations.district_id', $district);
        })
        ->when($province,function($query) use($province){
            return $query->whereHas('district', function ($q) use($province) {
                $q->whereIn('province_id', $province);
            });
        })
        ->when($zip_code,function($query) use($zip_code){
            return $query->where('locker_locations.zip_code','LIKE',"%$zip_code%");
        })
        ->when($country,function($query) use($country){
            return $query->whereHas('availabilityReport', function ($q) use($country) {
                $q->where('country', $country);
            });
        })
        ->when($availability,function($query) use($availability){
            return $query->whereHas('availabilityReport', function ($q) use($availability) {
                if($availability == "Online") {
                    $q->where('status', 'ENABLE');
                } else {
                    $q->where('status', 'EXCEPTION');
                } 
            });
        })
        ->when($activation,function($query) use($activation){
            if($activation == "Active") {
                return $query->where('isPublished', '1');
            } else {
                return $query->where('isPublished', '0');
            } 
        })
        ->when($type,function($query) use($type){
            return $query->whereHas('buildingType', function ($q) use($type) {
                $q->where('building_type', $type);
            });
        })
        ->get();
        
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        // style
        $bold = [
            'font' => [
                'bold' => true,
                'size' => 20
            ]
        ];

        // title
        $sheet->mergeCells('A1:E1');
        $sheet->setCellValue('A1', 'Locker Location List');
        $sheet->getStyle('A1')->applyFromArray($bold);

        // header
        $count = 4;
        $sheet->setCellValue('A'.$count, 'No');
        $sheet->setCellValue('B'.$count, 'Locker ID');
        $sheet->setCellValue('C'.$count, 'Name');
        $sheet->setCellValue('D'.$count, 'Country');
        $sheet->setCellValue('E'.$count, 'Address');
        $sheet->setCellValue('F'.$count, 'Address Detail');
        $sheet->setCellValue('G'.$count, 'District');
        $sheet->setCellValue('H'.$count, 'Province');
        $sheet->setCellValue('I'.$count, 'ZIP Code');
        $sheet->setCellValue('J'.$count, 'Building Type');
        $sheet->setCellValue('K'.$count, 'Operational Hours');
        $sheet->setCellValue('L'.$count, 'Capacity');
        $sheet->setCellValue('M'.$count, 'Latitude');
        $sheet->setCellValue('N'.$count, 'Longitude');
        $sheet->setCellValue('O'.$count, 'Availability');
        $sheet->setCellValue('P'.$count, 'Activation Status');

        if($data->isEmpty()) {
            // data kosong
            $count = $count+1;
            $sheet->mergeCells('A'.$count.':J'.$count);
            $sheet->setCellValue('A'.$count, 'No records found');
        } else { 
            // data ready
            $no = 1;
            foreach($data as $key => $row) {
                $count = $count+1;
                // availability
                if($row['availabilityReport']['status'] == 'ENABLE') {
                    $availability = 'Online';
                } else {
                    $availability = 'Offline';
                }

                // activation
                if($row['isPublished'] == '1') {
                    $activation = 'Active';
                } else {
                    $activation = 'Non Active';
                }

                // set data to all columns
                $sheet->setCellValue('A'.$count, $no++);
                $sheet->setCellValue('B'.$count, $row['locker_id']);
                $sheet->setCellValue('C'.$count, $row['name']);
                $sheet->setCellValue('D'.$count, $row['availabilityReport']['country']);
                $sheet->setCellValue('E'.$count, $row['address']);
                $sheet->setCellValue('F'.$count, $row['address_2']);
                $sheet->setCellValue('G'.$count, $row['district']['district']);
                $sheet->setCellValue('H'.$count, $row['district']['province']['province']);
                $sheet->setCellValue('I'.$count, $row['zip_code']);
                $sheet->setCellValue('J'.$count, $row['buildingType']['building_type']);
                $sheet->setCellValue('K'.$count, $row['operational_hours']);
                $sheet->setCellValue('L'.$count, $row['locker_capacity']);
                $sheet->setCellValue('M'.$count, $row['latitude']);
                $sheet->setCellValue('N'.$count, $row['longitude']);
                $sheet->setCellValue('O'.$count, $availability);
                $sheet->setCellValue('P'.$count, $activation);
            }
        }
        // remove column one-by-one
        $sheet->removeColumn('D');
        $sheet->removeColumn('F');
        $sheet->removeColumn('F');
        $sheet->removeColumn('F');
        $sheet->removeColumn('H');
        $sheet->removeColumn('J');
        
        $dateNow = date("ymd");
        $filename = $dateNow.' Locker Location.xlsx';
        
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xlsx");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'.$filename.'"');
        $writer->save("php://output");
        die();
    }

    public function systemInfo(Request $request) {
        $box_id = $request->box_id;
        $proxApi = new ProxApi;
        $res = $proxApi->getInfoBox($box_id); 
        
        if ($res){
            $doorLocker = $res->cabinets;
            $currentStatus = $res->currentStatus;
        }

        $lockerDetail = LockerDetailsData::where('locker_id', $box_id)->first();
        $getDataLocation = $this->getDataLocation($box_id);
        // $system_token = Box::select('token')->where('id', $id)->first()->pluck('token');
        
        $playing_tvc = $currentStatus->tvc_list;
        if ($playing_tvc != "[]" || !empty($playing_tvc)){
            $playing_tvc = str_replace("[", "", $playing_tvc);
            $playing_tvc = str_replace("]", "", $playing_tvc);
            $playing_tvc = str_replace('"', "", $playing_tvc);
            $playing_tvc = str_replace(' ', "", $playing_tvc);
            $tvc_list = explode(',', $playing_tvc);
        } else {
            $tvc_list = [] ;
        }
        
        $data = [
            'location' => $getDataLocation->location,
            'detail' => $lockerDetail,
            'door' => $doorLocker,
            'status' => $currentStatus,
            'tvcList' => $tvc_list
        ];
        return view('popbox.locker.system_detail', $data); 
    }

    public function remoteInitDb(Request $request)
    {
        $param = $request->route()->parameters();
        $proxApi = new ProxApi;
        $res = $proxApi->initDb($param['locker_id']); 

        if($res->response->code == 200) {
            $message = "Remote init DB success.";
        } else {
            $message = "You don't have permission to init the database.";
        }
        echo json_encode($message);
    }

    public function rebootLocker(Request $request)
    {
        $param = $request->route()->parameters();
        $proxApi = new ProxApi;
        $res = $proxApi->rebootLocker($param['locker_id']); 

        if($res->response->code == 200) {
            $message = "Reboot locker success.";
        } else {
            $message = "You don't have permission to reboot the locker.";
        }
        echo json_encode($message);
    }

    public function resyncLocker(Request $request)
    {
        $param = $request->route()->parameters();
        $proxApi = new ProxApi;
        $res = $proxApi->resyncLocker($param['locker_id']); 

        if($res->response->code == 200) {
            $message = "Resync locker data success.";
        } else {
            $message = "You don't have permission to resync locker data.";
        }
        echo json_encode($message);
    }
}
