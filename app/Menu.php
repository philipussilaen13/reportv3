<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    // Connect to roles table
    protected $table = 'menus';

    // menyimpan data dengan timestamps(created_at, updated_at, delete_at)
    public $timestamps = true;
}
