<?php
namespace App\Lib;
use App\Models\Audit as Model;
use DB;
use Auth;
class Audit {

    public static function create($key, $module, $data, $remark) {
        $payload = [
            'key'=>$key,
            'type'=>'create',
            'module'=>$module,
            'after'=>$data,
            'remark'=>$remark
        ];
       return SELF::save($payload);
    }

    public static function read($key, $module,$data, $remark) {
        $payload = [
            'key'=>$key,
            'type'=>'read',
            'before'=>$data,
            'module'=>$module,
            'remark'=>$remark
        ];
       return SELF::save($payload);
    }

    public static function update($key, $module, $data =[], $remark) {
        $payload = [
            'key'=>$key,
            'type'=>'update',
            'module'=>$module,
            'before'=>$data['before'],
            'after'=>$data['after'],
            'remark'=>$remark
        ];
       return SELF::save($payload);
    }

    public static function delete($key, $module, $data =[], $remark) {
        $payload = [
            'key'=>$key,
            'type'=>'delete',
            'module'=>$module,
            'before'=>$data['before'],
            'remark'=>$remark
        ];
       return SELF::save($payload);
    }

    private static function save($data) {

        if(Auth::check()) {
            $data['user_id'] = Auth::user()->id;
            $data['username'] = Auth::user()->name;
        }
        DB::beginTransaction();
        try {
            Model::create($data);
            DB::commit();
            return true;
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollback();
            return false;
        }
    }
    
}