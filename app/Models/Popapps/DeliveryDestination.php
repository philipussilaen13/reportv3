<?php

namespace App\Models\Popapps;

use Illuminate\Database\Eloquent\Model;

class DeliveryDestination extends Model
{
    protected $connection = 'popsend';
    protected $table = 'delivery_destinations';

    public static function getByDeliveryId($delivery_id) {
        $data = self::select('delivery_destinations.*')
        ->leftjoin('deliveries','deliveries.id','=','delivery_destinations.delivery_id')
        ->where('delivery_destinations.delivery_id', $delivery_id)
        ->orderBy('delivery_destinations.created_at', 'desc')
        ->get();

        return $data;
    }
}
