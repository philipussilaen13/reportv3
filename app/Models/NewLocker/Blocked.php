<?php

namespace App\Models\NewLocker;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Blocked extends Model
{
    protected $connection = 'newlocker_db';
    protected $table = 'tb_express_blocked';
}