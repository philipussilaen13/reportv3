<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentVendor extends Model
{
    protected $connection = 'payment';
    protected $table = 'payment_vendors';

    public function channel()
    {
        return $this->hasMany('App\Models\PaymentChannel', 'payment_vendors_id', 'id');
    }
}
