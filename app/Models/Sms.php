<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sms extends Model
{
    protected $table = 'nexmo_responses';
    protected $connection= 'popbox_db';

    protected $guarded = ['id'];
}
