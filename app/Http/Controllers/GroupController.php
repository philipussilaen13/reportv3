<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Group;
use App\Role;
use App\Group_role;
use Illuminate\Support\Facades\DB;

class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = Group::all();
        //var_dump($data); 
        return view('group.group', compact('list'));
        // ->with('list', $data['list']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();
        return view('group.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $data = array(
        //     'name' => $request->name
        // );
        
        // $id = DB::table('groups')->insertGetId($data);
        // foreach ($request->role_id as $key => $row)
        // {
        //     $groupRole []  = [ 
        //         'group_id' => $id,
        //         'role_id' => $row
        //     ];
        // }
        // DB::table('group_roles')->insert($groupRole);

        $validatedData = $request->validate([
            'name' => 'required|max:255'
        ]);

        if($validatedData) {
            $group = new Group;
            $group->name = $request->name;
            $group->save();
            
            $role = Role::find($request->role_id);
            $group->roles()->attach($role);
        }

		return redirect('group')->with(['success'=> 'New group has been added']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $id = $request->input('id',null);
        $data = Group::where('id', $id)->first();
        $roles = Role::all();

        $selectedRole = Group_role::where('group_id', $id)->get();
        $selected = [];
        foreach ($selectedRole as $key => $value) {
            $selected[] = $value->role_id;
        }

        return view('group.edit', compact('data', 'roles', 'selectedRole', 'selected'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id = $request->input('id',null);
        $data = Group::where('id', $id)->first();
        $data->name = $request->name;
        $data->save();

        $data->roles()->sync($request->role_id);
        // Insert, update, or delete role_route table
        // foreach ($request->role_id as $key => $row)
        // {
        //     $groupRole []  = [ 
        //         'group_id' => $data->id,
        //         'role_id' => $row,
        //     ];
        // }
        // DB::table('group_roles')->insert($groupRole);

        return redirect('group')->with(['success'=> 'Group detail has been changed']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $id = $request->input('id',null);
        Group::where('id', $id)->delete();
        return redirect('group')->with(['success'=> 'Group has been deleted']);
    }
}
