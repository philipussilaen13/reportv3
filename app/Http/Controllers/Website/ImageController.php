<?php

namespace App\Http\Controllers\Website;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Popbox\WebsiteCompany;
use App\Models\Popbox\WebsiteImage;

class ImageController extends Controller
{
    public function view(Request $request)
    {
        $data["list"] = WebsiteImage::all();
        return view('website.image.list', $data);
    }

    public function create()
    {
        $company = WebsiteCompany::all();
        $type = [
            'gallery' => 'Gallery',
            'promo' => 'Promo',
            'slider' => 'Slider',
        ];
        $data = [
            'company' => $company,
            'type' => $type
        ];
        return view('website.image.create', $data);
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'image' => 'required|image|mimes:jpg,jpeg,png|max:1024',
            'description' => 'required',
            'type' => 'required',
            'status' => 'required',
            'region' => 'required',
            'company' => 'required'
        ]);

        if( $request->hasFile('image')){ 
            $image = $request->file('image'); 
            $fileName = $image->getClientOriginalName();
            $fileExtension = $image->getClientOriginalExtension();
            $newImage = $request->type."_".time().'.'.$fileExtension;
            $image->move(public_path('uploads/images'), $newImage);
        } else {
            $newImage = NULL;
        }

        if($validatedData) {
            $data = new WebsiteImage();
            
            $data->image = $newImage;
            $data->description = $request->description;
            $data->type = $request->type;
            $data->status = $request->status;
            $data->region = $request->region;
            $data->company = $request->company;
            $data->save();
        }
		return redirect('website/image')->with(['success'=> 'New image has been added']);
    }

    public function edit(Request $request)
    {
        $id = $request->id;
        $image = WebsiteImage::where('id', $id)->first();
        if($image == NULL) {
            return redirect('website/image');
        } else {
            $company = WebsiteCompany::all();
            $type = [
                'gallery' => 'Gallery',
                'promo' => 'Promo',
                'slider' => 'Slider',
            ];
            $data = [
                'company' => $company,
                'type' => $type,
                'data' => $image
            ];
            return view('website.image.edit', $data);
        }
    }

    public function update(Request $request)
    {
        $id = $request->id;
        $validatedData = $request->validate([
            'image' => 'image|mimes:jpg,jpeg,png|max:1024',
            'description' => 'required',
            'type' => 'required',
            'status' => 'required',
            'region' => 'required',
            'company' => 'required'
        ]);

        if( $request->hasFile('image')){ 
            $image = $request->file('image'); 
            $fileName = $image->getClientOriginalName();
            $fileExtension = $image->getClientOriginalExtension();
            $newImage = $request->type."_".time().'.'.$fileExtension;
            $image->move(public_path('uploads/images'), $newImage);
        } else {
            $newImage = $request->old_image;
        }

        $update = [
            'image' => $newImage,
            'description' => $request->description,
            'type' => $request->type,
            'status' => $request->status,
            'region' => $request->region,
            'company' => $request->company
        ];
        
        $query = WebsiteImage::where('id', $id)->update($update);
        if($query) {
            return redirect('website/image/edit/'.$id)->with(['success'=> 'Image has been updated']);
        } else {
            return redirect('website/image/edit/'.$id)->with(['error'=> 'Updating image failed']);
        }
    }

    public function destroy(Request $request)
    {
        $id = $request->id;
        WebsiteImage::where('id', $id)->delete();
        return redirect('website/image')->with(['success'=> 'Image has been deleted']);
    }
}
