<?php

namespace App\Models\Popbox;

use Illuminate\Database\Eloquent\Model;

class LockerDetailsData extends Model
{
    protected $connection = 'popbox_db';
    protected $table = 'locker_details_data';
}