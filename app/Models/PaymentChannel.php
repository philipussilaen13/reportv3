<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentChannel extends Model
{
    protected $connection = 'payment';
    protected $table = 'payment_channels';

    protected $guarded = ['id'];

    public function vendor()
    {
        return $this->belongsTo('App\Models\PaymentVendor', 'payment_vendors_id');
    }

    public function client()
    {
        return $this->hasMany('App\Models\ClientTransaction', 'payment_channels_id', 'local_key');
    }

}
