<?php

namespace App\Models\Popapps;

use Illuminate\Database\Eloquent\Model;

class PaymentTransactionHistory extends Model
{
    protected $connection = 'popsend';
    protected $table = 'payment_transaction_histories';

    public static function getByPaymentTransactionId($payment_transaction_id) {
        $data = self::select('*','payment_transaction_histories.status as history_status','payment_transaction_histories.created_at as history_date')
        ->leftjoin('payment_transactions','payment_transactions.id','=','payment_transaction_histories.payment_transaction_id')
        ->join('users','users.id', '=', 'payment_transactions.user_id')
        ->where('payment_transaction_histories.payment_transaction_id', $payment_transaction_id)
        ->orderBy('payment_transaction_histories.created_at', 'desc')
        ->get();

        return $data;
    }
}
