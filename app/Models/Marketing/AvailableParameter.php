<?php

namespace App\Models\Marketing;

use Illuminate\Database\Eloquent\Model;

class AvailableParameter extends Model
{
    protected $connection = 'popsend';
    protected $table = 'available_parameters';
}
