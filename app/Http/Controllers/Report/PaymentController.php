<?php

namespace App\Http\Controllers\Report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PaymentChannel;
use App\Models\CompanyAccessToken;
use App\Models\ClientTransactionDetail;
use App\Models\TokenChannelPrivilege;
use App\Http\Helpers\Helper;
use App\Http\Resources\Payment\ChannelResource;
use App\Http\Resources\Payment\ClientResource;
use App\Http\Resources\Payment\CompanyAccessResource;
use DB;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Carbon\Carbon;
class PaymentController extends Controller
{
    public function index() {
        $payment_channel = PaymentChannel::all();
        return view('report.payment.transaction', compact('payment_channel'));
    }

    public function channel() {
        return view('report.payment.channel');
    }

    public function client() {
        return view('report.payment.client');
    }

    public function clientDetail($id) {
        $data = CompanyAccessToken::with('channels')->find($id);
        if($data){
            return view('report.payment.client_detail',compact('data'));
        }
        abort('404');
    }

    public function detailChannel($id) {

        $detail = PaymentChannel::with('vendor')->where('code',$id)->first();
        if(!$detail) {
            abort('404');
        }
        return view('report.payment.detail_channel', compact('detail'));
    }

    public function updateChannel(Request $request, $id) {
        
        $vendor = $request->vendor;
        $code = $request->code;
        $type = $request->type;
        $name = $request->name;
        $status = $request->status;
        $text = $request->text;
        $description = $request->description;
        $imageFile = $request->file('image');
        $data = PaymentChannel::find($id);
        if (!$data){
            $request->session()->flash('error','Channel Payment Not Found');
            return back();
        }
        $data->type = $type;
        $data->name = $name;
        $data->text = $text;
        $data->description = $description;
        $data->status = $status;
        if (!empty($imageFile)){
            $ftpHost = env('SERVER_PAYMENT_IP');
            $ftpUser = env('SERVER_PAYMENT_USER');
            $ftpPass = env('SERVER_PAYMENT_PASS');
            // filename
            $extension = $imageFile->getClientOriginalExtension();
            $fileName = $data->code.".$extension";
            $imageUrl = "files/channel/".$fileName;
            $remotePath = env('PAYMENT_PATH').$imageUrl;
            $connection = ssh2_connect($ftpHost,22);
            if ($connection === false){
                $request->session()->flash('error','Failed to Connect FTP');
                return back();
            }

            $state = ssh2_auth_password($connection,$ftpUser,$ftpPass);
            if ($state === false){
                $request->session()->flash('error','Failed to Authenticate FTP');
                return back();
            }

            $state = ssh2_scp_send($connection,$imageFile,$remotePath,0777);
            if ($state === false){
                $request->session()->flash('error','Failed to Upload FTP');
                return back();
            }
            $data->image_url = $imageUrl;
        }
        $data->save();
        $request->session()->flash('success','Success Update');
        return redirect('report/payment/channel');
    }
    // source controller

    public function sourceChannel(Request $request) {
        $limit = isset($request->length) ? $request->length : 10; 
        $page =  ( $request->start / $limit ) + 1;
        $total = PaymentChannel::all()->count();
        $result = PaymentChannel::paginate($limit,['*'],'draw',$page);
        $data = ChannelResource::collection($result);
        $collection = [
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => $total,  
            "recordsFiltered" => $result->total(), 
            "data"            => $data   
        ];
        return response()->json($collection, 200);
    }

    public function sourceTransaction(Request $request) {
        $limit = isset($request->length) ? $request->length : 10; 
        $page =  ( $request->start / $limit ) + 1;
        $rangeDate = $request->date;
        $startDate = null;
        $endDate = null;
        
        if (!empty($rangeDate)) {
            $date = explode(" - ", $rangeDate);
            $startDate = $date[0].' 00:00:00';
            $endDate = $date[1].' 23:59:59';
        }
        
        $payment_id = $request->payment_id;
        $sub_payment_id = $request->sub_payment_id;
        $status = $request->status;
        $type = $request->type;
        $va_number = $this->_getIdTransaction($request->va_number);
        $company_access_tokens_id = $request->company_access_tokens_id;
        $customer_phone = $request->customer_phone;
        $customer_email = $request->customer_email;
        $filterDescription = $request->description;
        $customerName = $request->customer_name;
        $clientTransactionId = $request->client_trans_id;
        $paymentChannelId = $request->payment_channel;
        $total = ClientTransactionDetail::all()->count();
        $db = ClientTransactionDetail::with('parent')
        ->when($sub_payment_id, function ($query) use ($sub_payment_id){
            return $query->where('sub_payment_id','LIKE',"%$sub_payment_id%");
        })
        ->when($status, function ($query) use ($status){
            return $query->where('status', $status);
        })
        ->when($va_number, function ($query) use ($va_number){
            return $query->where('client_transactions_id','LIKE',"%$va_number%");
        })
        ->when($rangeDate, function ($query) use ($startDate, $endDate){
            return $query->whereBetween('created_at',[$startDate, $endDate]);
        })
        ->whereHas('parent', function ($q) use($payment_id, $filterDescription, $customerName, $customer_phone, $customer_email, $clientTransactionId, $paymentChannelId, $type, $company_access_tokens_id) {
            $q->when($payment_id, function($query) use($payment_id){
                return $query->where('payment_id','LIKE', "%$payment_id%");
            });
            $q->when($clientTransactionId, function($query) use($clientTransactionId){
                return $query->where('transaction_id','LIKE', "%$clientTransactionId%");
            });
            $q->when($paymentChannelId, function($query) use($paymentChannelId){
                return $query->where('payment_channels_id', $paymentChannelId);
            });
            $q->when($customerName, function($query) use($customerName){
                return $query->where('customer_name','LIKE', "%$customerName%");
            });
            $q->when($customer_phone, function($query) use($customer_phone){
                return $query->where('customer_phone','LIKE', "%$customer_phone%");
            });
            $q->when($customer_email, function($query) use($customer_email){
                return $query->where('customer_email','LIKE', "%$customer_email%");
            });
            $q->when($filterDescription, function($query) use($filterDescription){
                return $query->where('description','LIKE', "%$filterDescription%");
            });
            $q->when($type, function($query) use($type){
                return $query->where('payment_type', $type);
            });
            $q->when($company_access_tokens_id, function($query) use($company_access_tokens_id){
                return $query->where('company_access_tokens_id', $company_access_tokens_id);
            });
        })
        ->orderBy('created_at','desc')
        ->paginate($limit,['*'],'draw',$page);
        $data = ClientResource::collection($db);
        $collection = [
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => $total,  
            "recordsFiltered" => $db->total(), 
            "data"            => $data   
        ];
        return response()->json($collection, 200);
    }

    public function sourceClient(Request $request) {
        $limit = isset($request->length) ? $request->length : 10; 
        $page =  ( $request->start / $limit ) + 1;
        $total = CompanyAccessToken::all()->count();
        $result = CompanyAccessToken::with('channels')->paginate($limit,['*'],'draw',$page);
        $data = CompanyAccessResource::collection($result);
        $collection = [
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => $total,  
            "recordsFiltered" => $result->total(), 
            "data"            => $data   
        ];
        return response()->json($collection, 200);
    }

    public function detail($subPaymentId) {
        
        $detail = ClientTransactionDetail::with('parent')->where('sub_payment_id',$subPaymentId)->first();
        $classBadge = [
            "CREATED"=>'badge badge-secondary',
            "PAID"=>'badge badge-success',
            "FAILED"=>'badge badge-danger',
            "PENDING"=>'badge badge-info',
            "EXPIRED"=>'badge badge-primary',
        ];
        $repush = false;
        
        if($detail){
            $helper = new Helper;
            $check = DB::connection('payment')->table('client_push_callbacks')->where('client_transaction_details_id',$detail->id)->first();
            if($check) {
                $status = $check->status;
                if($status !== 'SUCCESS' && $check->retry > 1) {
                    $repush = true;
                }
            }
            return view('report.payment.detail', compact('helper','detail','classBadge','repush'));
        }
        abort('404');
    }
    public function calculateAmount() {
        $firstDate = date('Y-m-01');
        $lastDate = date('Y-m-t');
        $result = ClientTransactionDetail::with('parent')->orderBy('created_at','desc')
                ->whereBetween('created_at',[$firstDate, $lastDate])->get();
        $amount['id'] = [];
        $amount['my'] = [];
        $count_id = 0;
        $count_my = 0;
       foreach($result as $row) {
           $vendor = $row->parent->channel->payment_vendors_id;
           if($vendor === 3) {
                array_push($amount['my'],$row->amount);
                $count_my++;
           }else{
                array_push($amount['id'],$row->amount);
                $count_id++;
           }
       }
       $data = [
           "id"=>array_sum($amount['id']),
           "my"=>array_sum($amount['my']),
           "count_id" => $count_id,
           "count_my" => $count_my
       ];
        return response()->json($data, 200);
    }

    private function _getIdTransaction($vaNumber) {
        if(is_null($vaNumber)) {
            return null;
        }
        $bniVa = DB::connection('payment')->table('bni_virtual_accounts')->select('client_transactions_id')->where('virtual_account_number',$vaNumber)->first();
        $dokuVa = DB::connection('payment')->table('doku_transactions')->select('client_transactions_id')->where('virtual_number',$vaNumber)->first();
        if($bniVa) {
            return $bniVa->client_transactions_id;
        }
        if($dokuVa) {
            return $dokuVa->client_transactions_id;
        }
        return null;
    }

    public function getClient(Request $request) {
        if ($request->has('search')) {
                $search = $request->input('search');
                $clients = DB::connection('payment')->table('company_access_tokens')->where('name', 'like', '%' . $search . '%')->get();
        }else{
            $clients = DB::connection('payment')->table('company_access_tokens')->get();
        }
        $list [] = [
            "id"=>'',
            "text"=>'All'
        ];
        foreach($clients as $client) {
            $list[$client->id] = [
                "id"=>$client->id,
                "text"=>$client->name
            ];
        }
        return $list;
    }

    public function transExcel(Request $request) {
        $rangeDate = $request->input('date-range');
        $startDate = null;
        $endDate = null;
        
        if (!empty($rangeDate)) {
            $date = explode(" - ", $rangeDate);
            $startDate = $date[0].' 00:00:00';
            $endDate = $date[1].' 23:59:59';
        }
        
        $payment_id = $request->payment_id;
        $sub_payment_id = $request->sub_payment_id;
        $status = $request->status;
        $type = $request->type;
        $va_number = $this->_getIdTransaction($request->va_number);
        $company_access_tokens_id = $request->company_access_tokens_id;
        $customer_phone = $request->customer_phone;
        $customer_email = $request->customer_email;
        $filterDescription = $request->description;
        $customerName = $request->customer_name;
        $clientTransactionId = $request->client_trans_id;
        $paymentChannelId = $request->payment_channel;
        $total = ClientTransactionDetail::all()->count();
        $db = ClientTransactionDetail::with('parent')->when($sub_payment_id, function ($query) use ($sub_payment_id){
            return $query->where('sub_payment_id','LIKE',"%$sub_payment_id%");
        })
        ->when($status, function ($query) use ($status){
            return $query->where('status', $status);
        })
        ->when($va_number, function ($query) use ($va_number){
            return $query->where('client_transactions_id','LIKE',"%$va_number%");
        })
        ->when($rangeDate, function ($query) use ($startDate, $endDate){
            return $query->whereBetween('created_at',[$startDate, $endDate]);
        })
        ->whereHas('parent', function ($q) use($payment_id, $filterDescription, $customerName, $customer_phone, $customer_email, $clientTransactionId, $paymentChannelId, $type, $company_access_tokens_id) {
            $q->when($payment_id, function($query) use($payment_id){
                return $query->where('payment_id','LIKE', "%$payment_id%");
            });
            $q->when($clientTransactionId, function($query) use($clientTransactionId){
                return $query->where('transaction_id','LIKE', "%$clientTransactionId%");
            });
            $q->when($paymentChannelId, function($query) use($paymentChannelId){
                return $query->where('payment_channels_id', $paymentChannelId);
            });
            $q->when($customerName, function($query) use($customerName){
                return $query->where('customer_name','LIKE', "%$customerName%");
            });
            $q->when($customer_phone, function($query) use($customer_phone){
                return $query->where('customer_phone','LIKE', "%$customer_phone%");
            });
            $q->when($customer_email, function($query) use($customer_email){
                return $query->where('customer_email','LIKE', "%$customer_email%");
            });
            $q->when($filterDescription, function($query) use($filterDescription){
                return $query->where('description','LIKE', "%$filterDescription%");
            });
            $q->when($type, function($query) use($type){
                return $query->where('payment_type', $type);
            });
            $q->when($company_access_tokens_id, function($query) use($company_access_tokens_id){
                return $query->where('company_access_tokens_id', $company_access_tokens_id);
            });
        })
        ->orderBy('created_at','desc');
        $data = $db->get();
        
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        // style
        $bold = [
            'font' => [
                'bold' => true,
                'size' => 20
            ]
        ];

        // title
        $sheet->mergeCells('A1:E1');
        $sheet->setCellValue('A1', 'Payment| Transaction List');
        $sheet->getStyle('A1')->applyFromArray($bold);
        $sheet->mergeCells('A2:E2');
        if (!empty($transactionDate)) {
        $sheet->setCellValue('A2', $start.' - '.$end);
        }
         // header
        $count = 4;
        $sheet->setCellValue('A'.$count, 'No');
        $sheet->setCellValue('B'.$count, 'Channel');
        $sheet->setCellValue('C'.$count, 'Payment ID');
        $sheet->setCellValue('D'.$count, 'Client Transaction');
        $sheet->setCellValue('E'.$count, 'VA Number');
        $sheet->setCellValue('F'.$count, 'Customer Name');
        $sheet->setCellValue('G'.$count, 'Customer Paid Amount');
        $sheet->setCellValue('H'.$count, 'Customer Received');
        $sheet->setCellValue('I'.$count, 'Status');
        $sheet->setCellValue('J'.$count, 'Description');
        $sheet->setCellValue('K'.$count, 'Created At');

        if($data->count() > 0) {
            $no = 1;
            foreach($data as $row) {
                $count = $count+1;
                $sheet->setCellValue('A'.$count, $no++);
                $sheet->setCellValue('B'.$count, $row['parent']['channel']->name);
                $sheet->setCellValue('C'.$count, $row['parent']['payment_id']);
                $sheet->setCellValue('D'.$count, $row['parent']['transaction_id']);
                $sheet->setCellValue('E'.$count, $row['parent']['virtual_number']);
                $sheet->setCellValue('F'.$count, $row['parent']['customer_name']);
                $sheet->setCellValue('G'.$count, number_format($row['paid_amount']));
                $sheet->setCellValue('H'.$count, number_format($row['customer_received']));
                $sheet->setCellValue('I'.$count, $row['status']);
                $sheet->setCellValue('J'.$count, $row['description']);
                $sheet->setCellValue('K'.$count, $row['created_at']);
            } 
        }else{
            $count = $count+1;
            $sheet->mergeCells('A'.$count.':Q'.$count);
            $sheet->setCellValue('A'.$count, 'No records found');
        }

        $dateNow = date("ymd");
        $filename = $dateNow.' payment-transaction-list.xlsx';
        
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xlsx");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'.$filename.'"');
        $writer->save("php://output");
        die();
    }

    public function channelExcel(Request $request) {

        $data = PaymentChannel::with('vendor')->get();
        
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        // style
        $bold = [
            'font' => [
                'bold' => true,
                'size' => 20
            ]
        ];

        // title
        $sheet->mergeCells('A1:E1');
        $sheet->setCellValue('A1', 'Payment| Transaction List');
        $sheet->getStyle('A1')->applyFromArray($bold);
        $sheet->mergeCells('A2:E2');
        if (!empty($transactionDate)) {
        $sheet->setCellValue('A2', $start.' - '.$end);
        }
         // header
        $count = 4;
        $sheet->setCellValue('A'.$count, 'No');
        $sheet->setCellValue('B'.$count, 'Vendor');
        $sheet->setCellValue('C'.$count, 'Code');
        $sheet->setCellValue('D'.$count, 'Name');
        $sheet->setCellValue('E'.$count, 'Description');
        $sheet->setCellValue('F'.$count, 'Status');
        $sheet->setCellValue('G'.$count, 'Created At');

        if($data->count() > 0) {
            $no = 1;
            foreach($data as $row) {
                $count = $count+1;
                $sheet->setCellValue('A'.$count, $no++);
                $sheet->setCellValue('B'.$count, $row['vendor']['name']);
                $sheet->setCellValue('C'.$count, $row['code']);
                $sheet->setCellValue('D'.$count, $row['name']);
                $sheet->setCellValue('E'.$count, $row['description']);
                $sheet->setCellValue('F'.$count, $row['status']);
                $sheet->setCellValue('G'.$count, $row['created_at']);
            } 
        }else{
            $count = $count+1;
            $sheet->mergeCells('A'.$count.':Q'.$count);
            $sheet->setCellValue('A'.$count, 'No records found');
        }

        $dateNow = date("ymd");
        $filename = $dateNow.' payment-channel.xlsx';
        
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xlsx");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'.$filename.'"');
        $writer->save("php://output");
    }

    public function updateDetail(Request $request) {
        $id = $request->id;
        $token = TokenChannelPrivilege::find($id);
        if($token){
            $currentStatus = $token->status;
            if ($currentStatus == 'OPEN'){
                $token->status = 'CLOSE';
            } else {
                $token->status = 'OPEN';
            }
            $token->save();
            return response()->json('Success', 200);
        }

        return response()->json('Something went wrong', 403);
    }

    public function clientExcel(Request $request) {

        $data = CompanyAccessToken::with('channels')->get();

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        // style
        $bold = [
            'font' => [
                'bold' => true,
                'size' => 20
            ]
        ];

        // title
        $sheet->mergeCells('A1:E1');
        $sheet->setCellValue('A1', 'Payment| Client List');
        $sheet->getStyle('A1')->applyFromArray($bold);
        $sheet->mergeCells('A2:E2');
        if (!empty($transactionDate)) {
        $sheet->setCellValue('A2', $start.' - '.$end);
        }
         // header
        $count = 4;
        $sheet->setCellValue('A'.$count, 'No');
        $sheet->setCellValue('B'.$count, 'Vendor');
        $sheet->setCellValue('C'.$count, 'Opened Channel');

        if($data->count() > 0) {
            $no = 1;
            foreach($data as $row) {
                $count = $count+1;
                $total = $row->channels->count();
                $sheet->setCellValue('A'.$count, $no++);
                $sheet->setCellValue('B'.$count, $row['name']);
                $sheet->setCellValue('C'.$count, $total);
            } 
        }else{
            $count = $count+1;
            $sheet->mergeCells('A'.$count.':Q'.$count);
            $sheet->setCellValue('A'.$count, 'No records found');
        }

        $dateNow = date("ymd");
        $filename = $dateNow.' payment-client.xlsx';
        
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xlsx");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'.$filename.'"');
        $writer->save("php://output");
    }

   public function repush(Request $request) {
       $paymentId = $request->id;
       try {
            $session = $this->_createSession();
            $resultSession = json_decode($session);
            $sessionId = $resultSession->data;
            $sessionId = $sessionId[0]->session_id;
            
            $repush  = $this->_repush($paymentId, $sessionId);

            $response = json_decode($repush);
            $code = $response->response->code;
            $data = $response->data;
            $msg = $data[0]->message;
            return response()->json($msg, $code);
       } catch (\Exception $e) {
           return response()->json($e->getMessage(), 500);
       }
       
       
   }

   private function _createSession() {
        $urlSession = env('URL_CREATE_SESSION');
        $token = env('PAYMENT_TOKEN');
        $data = array("client_id" => "001", "token" => $token);                                                                    
        $json = json_encode($data);                                                                        
        $ch = curl_init($urlSession);                                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($json))                                                                       
        );                                                                                                                   
        $result = curl_exec($ch);
        return $result;
   }

   private function _repush($paymentId, $sessionId) {
        $url = env('URL_PUSH_CALLBACK');
        $token = env('PAYMENT_TOKEN');
        $data = array("session_id" => $sessionId, "token" => $token, 'payment_id'=>$paymentId);                                                                    
        $json = json_encode($data);                                                                    
        $ch = curl_init($url);                                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($json))                                                                       
        );                                                                                                                   
        $result = curl_exec($ch);
        return $result;
   }
}
