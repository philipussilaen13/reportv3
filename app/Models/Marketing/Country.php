<?php

namespace App\Models\Marketing;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $connection = 'popsend';
    protected $table = 'countries';
}
