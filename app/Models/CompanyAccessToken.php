<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyAccessToken extends Model
{
    protected $connection = 'payment';
    protected $table = 'company_access_tokens';

    public function channels(){
        return $this->belongsToMany('App\Models\PaymentChannel','token_channel_privileges','company_access_tokens_id','payment_channels_id')->withPivot('status','id');
    }
}
