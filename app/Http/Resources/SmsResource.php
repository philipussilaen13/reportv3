<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SmsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'phone'=>$this->msisdn,
            'message_id'=>$this->message_id,
            'code'=>$this->error_code,
            'message_time'=>$this->message_timestamp,
            'scts'=>$this->scts,
            'status'=>ucfirst($this->status),
            'network'=>$this->network_code
        ];
    }
   
}
