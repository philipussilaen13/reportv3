<?php
namespace App\Library;
use Illuminate\Support\Facades\Redis;
class Client
{
    public function __construct($id, $menu)
    {
       $this->id      = $id;
       $this->menu    = $menu;
    }

    public function store()
    {
        Redis::hmset('client:' . $this->id, [
            'id'      => $this->id,
            'menu'    => $this->menu,
        ]);
    }

    public static function find($id)
    {
        $key = 'client:' . $id;
        $stored = Redis::hgetall($key);
        if (!empty($stored)) {
            return new Client($stored['id'], $stored['menu']);
        }
        return false;
    }
}