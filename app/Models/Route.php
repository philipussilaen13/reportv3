<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Route extends Model
{
    // Connect to roles table
    protected $table = 'routes';

    // menyimpan data dengan timestamps(created_at, updated_at, delete_at)
    public $timestamps = true;

    public static function getAllMenu()
    {
        $menu = DB::table('routes')
            // ->select('name', 'route_name', 'icon', 'type', 'parent_id')
            ->where('type', '=', 'menu')
            ->get();

        return $menu;
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'role_routes');
    }
}
