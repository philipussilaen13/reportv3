<?php

namespace App\Lib;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;

class Whatsapp {
    protected $token;
    protected $responses;
    protected $client;
    protected $headers;

    public function __construct($token) {
        $this->token = $token;
        $this->headers = [
            'Accept' => 'application/json',
            'Authorization' => $this->token,
        ];
        $this->client = new Client([
            'base_uri'=>'https://wablas.com/api/'
        ]);
    }
    /** 
     * $data['phone'] = target phone number
     * $data['message'] = message content 
     **/
    public function send($data = []) {

        try {
            $fetch = $this->client->request('POST', 'send-message', [
                RequestOptions::HEADERS => $this->headers,
                RequestOptions::FORM_PARAMS=> $data,
           ]);
           $code = $fetch->getStatusCode();
           $this->responses = [
               'code'=>$fetch->getStatusCode(),
               'body'=>json_decode($fetch->getBody()->getContents())
           ];

        } catch (\Throwable $t) {
            throw new \Exception($t->getMessage(), $t->getCode());
        }
        return $this;
    }

    public function response() {
        return $this->responses;
    }
}