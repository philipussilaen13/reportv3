<?php

namespace App\Models\NewLocker;

use Illuminate\Database\Eloquent\Model;

class Box extends Model
{
    const UPDATED_AT = 'last_update';
    // const CREATED_AT = 'last_update';

    // set connection and table
    protected $connection = 'newlocker_db';
    protected $table = 'tb_newlocker_box';
    protected $primaryKey = 'id';
    public $incrementing = false;
    public $timestamps = false;

    public static function getAllLocker(){
        $boxDb = Box::join('tb_newlocker_machinestat','tb_newlocker_machinestat.locker_id','=','tb_newlocker_box.id')
            ->where('deleteFlag','=','0')->where('tb_newlocker_box.activationFlag', '=', '1')
            ->get();
        dd($boxDb->toArray());
        $lockerList = [];

        foreach ($boxDb as $locker){
            $tmp = new \stdClass();
            $tmp->lockerId = $locker->id;
            $tmp->lockerName = $locker->name;
            $tmp->status = $locker->conn_status;
            $tmp->statusText = 'offline';
            if ($locker->conn_status == 1) $tmp->statusText = 'online';

            $lockerList[] = $tmp;
        }

        return $lockerList;
    }

    public function location()
    {
        return $this->hasOne(Location::class, 'locker_id');
    }

    public function machineStat()
    {
        return $this->hasOne(MachineStat::class, 'locker_id');
    }

    public function express(){
        return $this->hasMany(Express::class,'box_id','id');
    }
}
