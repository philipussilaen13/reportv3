@extends('layout.main')
{{-- Do Not Edit this file. This is Base File for View --}}

@section('title')
	Edit a Route
@endsection

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/libs/select2/dist/css/select2.min.css') }}">
@endsection

@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ url('route') }}">Route</a></li>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="material-card card">
            <div class="card-body">
                <h4 class="card-title">Form Route</h4>
            </div>
            <hr>
            <form id="add-new-route" method="post" action="{{ url('route/update').'?id='.$data->id }}">
                {{ csrf_field() }}
                <div class="card-body">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="name" value="{{ $data->name }}">
                    </div>
                    <div class="form-group">
                        <label for="name">Route</label>
                        <input type="text" class="form-control" id="route" name="route" placeholder="route" value="{{ $data->route }}">
                        <small class="form-text text-muted">ex : route/create</small>
                    </div>
                    <div class="form-group">
                        <label for="route_name">Route Name</label>
                        <input type="text" class="form-control" id="route_name" name="route_name" placeholder="route name" value="{{ $data->route_name }}">
                        <small class="form-text text-muted">ex : route.create</small>
                    </div>
                    <div class="form-group">
                        <label for="icon">Icon</label>
                        <input type="text" class="form-control" id="icon" name="icon" placeholder="icon" value="{{ $data->icon }}">
                    </div>
                    
                    <div class="form-group">
                        <label for="route_name">Type</label>
                        <div class="custom-control custom-radio">
                            <input type="radio" id="type1" name="type" value="menu" class="custom-control-input" @if($data->type == 'menu') checked @else  @endif>
                            <label class="custom-control-label" for="type1">Menu</label>
                        </div>
                        <div class="custom-control custom-radio">
                            <input type="radio" id="type2" name="type" value="route" class="custom-control-input" @if($data->type == 'route') checked @else  @endif>
                            <label class="custom-control-label" for="type2">Route</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="icon">Description</label>
                        <textarea class="form-control" id="description" name="description" rows="3" placeholder="Description">{{ $data->description }}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="parent_id">Parent ID</label>
                        <!-- <input type="text" class="form-control" id="parent_id" name="parent_id" placeholder="parent id" value="{{ $data->parent_id }}"> -->
                        <select name="parent_id" class="select2 form-control custom-select" style="width: 100%; height:36px;">
                                    <optgroup label="Set as Parent">
                                        <option value="0">Parent</option>
                                    </optgroup>
                                    <optgroup label="Set as Child">
                                        @foreach ($menu as $row)
                                            @if($row->id == $data->parent_id)
                                            <option value="{{ $row->id }}" selected>{{ $row->name }}</option>
                                            @else
                                            <option value="{{ $row->id }}">{{ $row->name }}</option>
                                            @endif
                                        @endforeach
                                    </optgroup>
                        </select>
                    </div>
                </div>
                <hr>
                <div class="card-body">
                    <div class="form-group mb-0 text-right">
                        <button type="submit" class="btn btn-info waves-effect waves-light">Save</button>
                        <button type="reset" class="btn btn-dark waves-effect waves-light">Cancel</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('js')
<!-- <script src="{{ asset('assets/extra-libs/DataTables/datatables.min.js') }}"></script> -->
<script src="{{ asset('assets/libs/select2/dist/js/select2.full.min.js') }}"></script>
<script src="{{ asset('assets/libs/select2/dist/js/select2.min.js') }}"></script>
<script type="text/javascript">
jQuery(document).ready(function($) {
    //url: '{{ url("route/getAllRoute") }}',
    $(".select2").select2({
        placeholder: "Select Parent Menu"
    });
});
</script>
@endsection