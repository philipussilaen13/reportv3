<?php

namespace App\Http\Resources\Payment;

use Illuminate\Http\Resources\Json\JsonResource;

class ChannelResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'name'=>$this->name,
            'code'=>$this->code,
            'status'=>$this->status,
            'vendor'=>$this->vendor->name,
            'description'=>$this->description,
            'created_at'=>is_null($this->created_at) ? null : $this->created_at->toDateTimeString(),
        ];
    }
}
