<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark" href="{{ url('home') }}" aria-expanded="false">
                    <i class="fas fa-home"></i>
                    <span class="hide-menu">Home</span>
                    </a>
                </li>
                @php
                $menu = \App\Models\AccessMenu::hasAccessMenu();
                //print_r($menu->data);
                @endphp
                @foreach ($menu->data as $group)
                    @php
                    if($group->child == true) { 
                        $groupHasArrow = 'has-arrow'; 
                    } else { 
                        $groupHasArrow = '';
                    }
                    @endphp
                    <li class="sidebar-item">
                        <a href="{{ ($group->child == true) ? 'javascript:void(0)' : url($group->route) }}" class="sidebar-link {{ $groupHasArrow }} waves-effect waves-dark" aria-expanded="false">
                            <i class="{{ $group->icon }}"></i>
                            <span class="hide-menu">{{ $group->name }}</span> 
                        </a>
                        @if ($group->child == true)
                            <!-- has child -->
                            <ul aria-expanded="false" class="collapse first-level">
                            @foreach ($group->child as $child)
                                @php
                                if($child->child == true) { 
                                    $childHasArrow = 'has-arrow';
                                } else { 
                                    $childHasArrow = '';
                                }
                                @endphp
                                <li class="sidebar-item">
                                    <a href="{{ ($child->child == true) ? 'javascript:void(0)' : url($child->route) }}" class="sidebar-link {{ $childHasArrow }}">
                                        <i class="{{ $child->icon }}"></i>
                                        <span class="hide-menu">{{ $child->name }}</span>
                                    </a>
                                    @if ($child->child == true)
                                        <ul aria-expanded="false" class="collapse second-level">
                                        @foreach ($child->child as $grandChild)
                                        <li class="sidebar-item">
                                            <a href="{{ url($grandChild->route) }}" class="sidebar-link">
                                                <i class="{{ $grandChild->icon }}"></i>
                                                <span class="hide-menu">{{ $grandChild->name }}</span>
                                            </a>
                                        </li>
                                        @endforeach
                                        </ul>
                                    @else
                                        <!-- blank -->
                                    @endif
                                </li>
                            @endforeach
                            </ul>
                        @else
                            <!-- no child -->
                        @endif
                    </li>
                @endforeach
                <div class="devider"></div>
                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="#" aria-expanded="false">
                    <i class="mdi mdi-adjust text-success"></i>
                    <span class="hide-menu">FAQs</span>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>