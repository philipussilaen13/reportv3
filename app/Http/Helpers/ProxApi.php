<?php
/**
 * Created by PhpStorm.
 * User: arief
 * Date: 07/12/2017
 * Time: 15.01
 */

namespace App\Http\Helpers;


use Illuminate\Support\Facades\DB;

class ProxApi
{
    private $id = null;

    /**
     * @param string $request
     * @param array $param
     * @return mixed
     */
    private function cUrl($request, $param = [], $method='POST')
    {
        if (empty($this->id)) $this->id = uniqid();
        $unique = $this->id;
        $host = env('PROX_URL');
        $token = env('PROX_TOKEN');
        
        $url = $host . '/' . $request;
        $json = json_encode($param);

        $date = date('Y.m.d');
        $time = date('H:i:s');
        $msg = "$unique > $time Request : $url : $json\n";
        $f = fopen(storage_path() . '/logs/api/prox.' . $date . '.log', 'a');
        fwrite($f, $msg);
        fclose($f);

        $headers = [
            'Content-Type:application/json',
            "userToken:$token"
        ];

        $ch = curl_init();
        // 2. set the options, including the url
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, count($param));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $output = curl_exec($ch);
        curl_close($ch);
        
        $time = date('H:i:s');
        $msg = "$unique > $time Response : $output\n";
        $f = fopen(storage_path() . '/logs/api/prox.' . $date . '.log', 'a');
        fwrite($f, $msg);
        fclose($f);

        DB::table('companies_response')
            ->insert([
                'api_url' => $url,
                'api_send_data' => $json,
                'api_response' => $output,
                'response_date' => date("Y-m-d H:i:s")
            ]);

        return $output;
    }

    /**
     * Get SMS History By Parcel ID
     * @param $parcelId
     * @return mixed
     */
    public function getSMSHistoryByParcelId($parcelId)
    {
        $url = 'task/sms/history';
        $param = [];
        $param['id'] = $parcelId;
        $result = $this->cUrl($url, $param);
        $result = json_decode($result);
        return $result;
    }

    /**
     * Change Phone Number by Parcel Id
     * @param $parcelId
     * @param $phone
     * @return mixed
     */
    public function changePhoneNumber($parcelId,$phone){
        $url = 'express/modifyPhoneNumber';
        $param = [];
        $param['id'] = $parcelId;
        $param['takeUserPhoneNumber'] = $phone;
        $result = $this->cUrl($url, $param);
        $result = json_decode($result);
        return $result;
    }

    /**
     * re Send SMS
     * @param $parcelId
     * @return mixed
     */
    public function reSendSMSbyParcelId($parcelId)
    {
        $url = 'task/express/resendSMS';
        $param = [];
        $param['id'] = $parcelId;
        $result = $this->cUrl($url, $param);
        $result = json_decode($result);
        return $result;
    }

    /**
     * Change Overdue Time by Parcel ID
     * @param $parcelId
     * @param $unixOverdueDateTime
     * @return mixed
     */
    public function changeOverdueDateTime($parcelId, $unixOverdueDateTime){
        $url = 'task/express/resetExpress';
        $param = [];
        $param['id'] = $parcelId;
        $param['overdueTime'] = $unixOverdueDateTime;
        $result = $this->cUrl($url, $param);
        $result = json_decode($result);
        return $result;
    }

    /**
     * Remote Locker Door by Parcel Ids
     * @param $parcelId
     * @return mixed
     */
    public function remoteUnlockByExpress($parcelId){
        $url = 'task/mouth/remoteUnlockByExpress';
        $param = [];
        $param['id'] = $parcelId;
        $result = $this->cUrl($url, $param);
        $result = json_decode($result);
        return $result;
    }

    /**
     * Delete Imported Data
     * @param $expressId
     * @return mixed
     */
    public function deleteImport($expressId){
        $url = 'express/deleteImportedExpress/'.$expressId;
        $param = [];
        $result = $this->cUrl($url, $param);
        $result = json_decode($result);
        return $result;
    }

    public function getInfoBox($boxId)
    {
        $url = 'box/info/'.$boxId;
        $param = [];
        $result = $this->cUrl($url, $param, 'GET');
        $result = json_decode($result);
        return $result;
    }

    /**
     * Remote Locker Door by Mouth Ids
     * @param $mouthId
     * @return mixed
     */
    public function remoteUnlock($mouthId){
        $url = 'task/mouth/remoteUnlock';
        $param = [];
        $param['id'] = $mouthId;
        $result = $this->cUrl($url, $param);
        $result = json_decode($result);
        return $result;
    }

    /**
     * Remote Locker Door by Mouth Ids
     * @param $mouthId
     * @return mixed
     */
    public function changeDoorSize($mouthId, $size){
        $url = 'task/box/mouth/update';
        $param = [];
        $param['id'] = $mouthId;
        $param['changeSize'] = $size;
        $result = $this->cUrl($url, $param);
        $result = json_decode($result);
        return $result;
    }

    /**
     * Remote Locker Door by Mouth Ids
     * @param $mouthId
     * @return mixed
     */
    public function changeDoorStatus($mouthId, $status){
        $url = 'task/box/mouth/update';
        $param = [];
        $param['id'] = $mouthId;
        $param['status'] = $status;
        $result = $this->cUrl($url, $param);
        $result = json_decode($result);
        return $result;
    }

    /**
     * Blocking Parcel 
     * @param $express_id
     * @param $validate_code
     * @return mixed
     */
    public function blockingParcel($express_id, $validate_code, $is_blocking){
        $url = 'express/block';
        $param = [];
        $param['parcel_id'] = $express_id;
        $param['validate_code'] = $validate_code;
        $param['is_block'] = $is_blocking;
        $result = $this->cUrl($url, $param);
        $result = json_decode($result);
        return $result;
    }

    /**
     * Remote Init DB by Locker Id
     * @param $lockerId
     * @return mixed
     */
    public function initDb($lockerId){
        $url = 'task/box/forceInit';
        $param = [];
        $param['id'] = $lockerId;
        $result = $this->cUrl($url, $param);
        $result = json_decode($result);
        return $result;
    }

    /**
     * Reboot Locker by Locker Id
     * @param $lockerId
     * @return mixed
     */
    public function rebootLocker($lockerId){
        $url = 'task/remoteReboot';
        $param = [];
        $param['id'] = $lockerId;
        $param['password'] = '22cc0a50e199494682a9fc2ee2e88294';
        $result = $this->cUrl($url, $param);
        $result = json_decode($result);
        return $result;
    }

    /**
     * Resync Locker Data by Locker Id
     * @param $lockerId
     * @return mixed
     */
    public function resyncLocker($lockerId){
        $url = 'task/forceResyncAll';
        $param = [];
        $param['id'] = $lockerId;
        $param['password'] = '22cc0a50e199494682a9fc2ee2e88294';
        $result = $this->cUrl($url, $param);
        $result = json_decode($result);
        return $result;
    }
}