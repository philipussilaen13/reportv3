<?php

namespace App\Models\NewLocker;

use Illuminate\Database\Eloquent\Model;

class SmsHistory extends Model
{
    protected $connection = 'newlocker_db';
    protected $table = 'tb_newlocker_smslog';

    public static function countSMS($expressId)
    {
        $data = self::where('express_id', $expressId)->where('sms_status', 'SUCCESS')->get();
        $total = count($data);
        return $total;
    }
}
