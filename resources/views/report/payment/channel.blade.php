@extends('layout.main')

@section('title')
	Payment Channel
@endsection

@section('css')
<link href="{{ asset('assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css') }}" rel="stylesheet">
@endsection

@section('content')
{{-- counter --}}

{{-- filter box --}}

{{-- content table --}}
<div class="row">
    <div class="col-12">
        <div class="material-card card">
            <div class="card-body row">
                <div class="col-sm-12 col-md-6">
                    <h4 class="card-title">Payment Channel</h4>
                    <h6 class="card-subtitle">Payment Channel List</h6>
                </div>
                <div class="col-sm-12 col-md-6">
                    <button id="popExcel" class="btn btn-primary mb-3" style="float:right;"> Download </button>
                </div>
                <div class="table-responsive">
                    <table id="source-table" class="table table-striped border text-inputs-searching" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Vendor</th>
                                    <th>Code</th>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Status</th>
                                    <th>Created At</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Vendor</th>
                                    <th>Code</th>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Status</th>
                                    <th>Created At</th>
                                </tr>
                            </tfoot>
                        </table>
                </div>
            </div>
        </div>
    </div>
</div>
        
@endsection

@section('js')
<script src="{{ asset('assets/libs/moment/moment.js') }}"></script>
<script src="{{ asset('assets/extra-libs/DataTables/datatables.min.js') }}"></script>
<script src="{{ asset('js/common.js') }}"></script>

<script>
$(document).ready(function() {
    getList({
        "_token" : "{{ csrf_token() }}"
    })
})
function getList(param = {}) {
    
    return $('#source-table').DataTable( {
          processing: true,
          serverSide: true,
          bFilter: false,
          ajax: {
              url: "{{ url('report/payment/source-channel') }}",
              dataType: "json",
              type: "POST",
              data: param
          },
          columns: [
            { 
            "data": "vendor",
            "name": "vendor",
            "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            { 
            "data": "code",
            "name": "code",
            "render": function ( data, type, full, meta ) {
                 return "<a href='{{ url('report/payment/channel/') }}/"+data+"'>"+data+"</a>";
                }
            },
            { 
            "data": "name",
            "name": "name",
            "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            { 
            "data": "description",
            "name": "description",
            "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            { 
            "data": "status",
            "name": "status",
            "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            { 
            "data": "created_at",
            "name": "created_at",
            "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
          ]
          
      } );
  }
  $('#popExcel').click(function () {
    window.location.href = "{{ url('report/payment/channel-excel/') }}?";
});
</script>
@endsection
