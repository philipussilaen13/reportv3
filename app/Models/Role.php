<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    // Connect to table
    protected $table = 'roles';

    // menyimpan data dengan timestamps(created_at, updated_at, delete_at)
    public $timestamps = true;

    public function groups()
    {
        return $this->belongsToMany(Group::class, 'group_roles');
    }

    public function routes()
    {
        return $this->belongsToMany(Route::class, 'role_routes');
    }
}
