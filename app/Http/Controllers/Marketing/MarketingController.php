<?php

namespace App\Http\Controllers\Marketing;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Marketing\Campaign;
use App\Models\Marketing\CampaignParameter;
use App\Models\Marketing\AvailableParameter;
use App\Models\Marketing\Vouchers;

use App\Models\Marketing\City;
use App\Models\Marketing\Province;
use App\Models\Marketing\Country;

use App\Models\Popbox\LockerLocation;
use App\Models\Popbox\BuildingType;

use App\Models\TokenChannelPrivilege;
use App\Models\PaymentChannel;

use App\Http\Resources\MarketingCampaignResource;
use App\Http\Requests\MarketingCampaignRequest;


use DB;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Carbon\Carbon;
class MarketingController extends Controller
{
    public function campaign() {
        return view('marketing.campaign');
    }

    public function sourceCampaign(Request $request) {
        $limit = isset($request->length) ? $request->length : 10; 
        $page =  ( $request->start / $limit ) + 1;
        $rangeDate = $request->date;
        $startDate = null;
        $endDate = null;
        if (!empty($rangeDate)) {
            $date = explode(" - ", $rangeDate);
            $startDate = $date[0].' 00:00:00';
            $endDate = $date[1].' 23:59:59';
        }
        $status = '';
        if($request->status) {
            $status = ($request->status === 'enable') ? '1' : "0";
        }
        $filter = [
            'promo_type'=>$request->type
        ];
        $filter = array_filter($filter);
        $total = Campaign::all()->count();
        $db = Campaign::where($filter)->orderBy('start_time','desc');
            if(!empty($rangeDate)) {
                $db->whereBetween('start_time',[$startDate, $endDate]);
            };
            if($request->name){
                $db->where('name', 'like', '%' . $request->name . '%');
            }
            if($request->status){
                $db->where('status', $status);
            }
            if($request->voucher){
                $db->where('voucher_type', 'like', '%' . $request->voucher . '%');
            }
        $result = $db->paginate($limit,['*'],'page',$page);
        $data = MarketingCampaignResource::collection($result);
        $collection = [
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => $total,  
            "recordsFiltered" => $result->total(), 
            "data"            => $data   
        ];
        return response()->json($collection, 200);
    }

    public function excel(Request $request) {
        $rangeDate = $request->input('date-range');
        $startDate = null;
        $endDate = null;
        if (!empty($rangeDate)) {
            $date = explode(" - ", $rangeDate);
            $startDate = $date[0].' 00:00:00';
            $endDate = $date[1].' 23:59:59';
        }
        $status = '';
        if($request->status) {
            $status = ($request->status === 'enable') ? '1' : "0";
        }
        $filter = [
            'promo_type'=>$request->type
        ];
        $filter = array_filter($filter);
        $total = Campaign::all()->count();
        $db = Campaign::where($filter)->orderBy('start_time','desc');
            if(!empty($rangeDate)) {
                $db->whereBetween('start_time',[$startDate, $endDate]);
            };
            if($request->name){
                $db->where('name', 'like', '%' . $request->name . '%');
            }
            if($request->status){
                $db->where('status', $status);
            }
            if($request->voucher){
                $db->where('voucher_type', 'like', '%' . $request->voucher . '%');
            }
        $data = $db->get();
        // excel
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        // style
        $bold = [
            'font' => [
                'bold' => true,
                'size' => 20
            ]
        ];

        // title
        $sheet->mergeCells('A1:E1');
        $sheet->setCellValue('A1', 'Campaign List');
        $sheet->getStyle('A1')->applyFromArray($bold);
        $sheet->mergeCells('A2:E2');
        if (!empty($transactionDate)) {
        $sheet->setCellValue('A2', $start.' - '.$end);
        }
         // header
        $count = 4;
        $sheet->setCellValue('A'.$count, 'No');
        $sheet->setCellValue('B'.$count, 'Name');
        $sheet->setCellValue('C'.$count, 'Description');
        $sheet->setCellValue('D'.$count, 'Start Date');
        $sheet->setCellValue('E'.$count, 'End Date');
        $sheet->setCellValue('F'.$count, 'Status');
        $sheet->setCellValue('G'.$count, 'Type');
        $sheet->setCellValue('H'.$count, 'Amount');
        $sheet->setCellValue('I'.$count, 'Category');
        $sheet->setCellValue('J'.$count, 'Voucher/Limit Type');
        $sheet->setCellValue('K'.$count, 'Limit Usage');

        if($data->count() > 0) {
            $no = 1;
            foreach($data as $row) {
                $count = $count+1;
                $sheet->setCellValue('A'.$count, $no++);
                $sheet->setCellValue('B'.$count, $row->name);
                $sheet->setCellValue('C'.$count, $row->description);
                $sheet->setCellValue('D'.$count, $row->start_time);
                $sheet->setCellValue('E'.$count, $row->end_time);
                $sheet->setCellValue('F'.$count, $row->status_name);
                $sheet->setCellValue('G'.$count, $row->type);
                $sheet->setCellValue('H'.$count, $row->amount);
                $sheet->setCellValue('I'.$count, $row->category);
                $sheet->setCellValue('J'.$count, $row->voucher_type);
                $sheet->setCellValue('K'.$count, $row->limit_usage);
            } 
        }else{
            $count = $count+1;
            $sheet->mergeCells('A'.$count.':Q'.$count);
            $sheet->setCellValue('A'.$count, 'No records found');
        }

        $dateNow = date("ymd");
        $filename = $dateNow.' Campaign-List.xlsx';
        
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xlsx");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'.$filename.'"');
        $writer->save("php://output");
    }

    public function edit(Request $request, $id) {
        $detail = Campaign::with('parameters')->find($id);
        if(!$detail) {
            abort('Data not found');
        }
        $startDate = Carbon::createFromFormat('Y-m-d H:i:s', $detail->start_time)->format('d-m-Y H:i:s');
        $endDate = Carbon::createFromFormat('Y-m-d H:i:s', $detail->end_time)->format('d-m-Y H:i:s');
        $totalParameter = $detail->parameters->count();
        $operatorList = array('<'=>'Less','<='=>'Less or Same','='=>'Same','>='=>'More or Same','>'=>'More','between'=>'Between','exist'=>'Exist','except'=>'Not Exist');
        
        $parameter = AvailableParameter::all();
        $availableParamList = [];
        $paramCategory = [];
        foreach ($parameter as $param){
            if (!isset($paramCategory[$param->param_name]))
            {
                $i = count($paramCategory);
                $paramCategory[$param->param_name] = $i;
            }
            $i = $paramCategory[$param->param_name];

            $availableParamList[$i]['paramCategory'] = utf8_encode($param->param_name);
            $availableParamList[$i]['paramName'][] = array("id"=>$param->id,"name" => $param->name, "description" => ucfirst(utf8_encode($param->description)), "type"=> $param->type);
        }

        return view('marketing.edit',compact('detail','startDate','endDate','totalParameter','operatorList','availableParamList'));
    }

    public function buildHtml(Request $request) {
        $counter = $request->counter;
        $operatorList = array('<'=>'Less','<='=>'Less or Same','='=>'Same','>='=>'More or Same','>'=>'More','between'=>'Between','exist'=>'Exist','except'=>'Not Exist');
        $parameter = AvailableParameter::all();
        $availableParamList = [];
        $paramCategory = [];
        foreach ($parameter as $param){
            if (!isset($paramCategory[$param->param_name]))
            {
                $i = count($paramCategory);
                $paramCategory[$param->param_name] = $i;
            }
            $i = $paramCategory[$param->param_name];

            $availableParamList[$i]['paramCategory'] = utf8_encode($param->param_name);
            $availableParamList[$i]['paramName'][] = array("id"=>$param->id,"name" => $param->name, "description" => ucfirst(utf8_encode($param->description)), "type"=> $param->type);
        }
        // dd($parameter,$availableParamList, $paramCategory);
        $html = "<div class='row' id='counter-".$counter."'><div class='col-lg-12'><hr>";
        // form group parameter
        $html.="<div class='form-group'>";
        $html.="<label>Parameter</label>";
        $html.=" <select name='rules[".$counter."][parameter]' rule='".$counter."' id='parameter-".$counter."' class='form-control select2 rule-parameter'>";
            foreach($availableParamList as $group){
               $html.=" <optgroup label='".$group['paramCategory']."'>";
                foreach ($group['paramName'] as $element){
                   $html.="<option value='".$element['id']."' name='".$element['name']."'>".$element['description']." (".$element['name'].")</option>";
                }
            $html.="</optgroup>";
            }
        
        // endforeach
        $html.="</select></div>";
        // form group operator
        $html.="<div class='form-group'>";
        $html.="<label>Operator</label>";
        $html.=" <select name='rules[".$counter."][operator]' id='operators-".$counter."' rule='".$counter."' class='form-control select2 rule-parameter'>";
            foreach($operatorList as $key=>$val){
                $html.="<option value='".$key."'>".$val."</option>";
            }
        $html.="</select></div>";
        // value helper
        $html.="<div class='form-group'>";
        $html.="<label>Value</label>";
        $html.="<input  type='text' class='form-control' name='rules[".$counter."][value]'  id='value-".$counter."'><br>";
        $html.="<a href='javascript:void(1)' class='btn btn-success float-right helper-rule' id='helper-".$counter."'>Helper</a>";
        $html.="</div>";
        // delete rule
        $html.="<div class='form-group'>";
        $html.="<a href='javascript:void(1)' class='btn btn-danger float-lef delete-rule' id='delete-".$counter."'>Delete Rule</a>";
        $html.="</div>";
        $html.="</div></div>";
        return $html;
    }

    public function helper(Request $request) {
        $type = $request->type;
        $code = 200;
        switch ($type){
            case 'origin_country':
            case 'origin_locker_country':
                $country = Country::get()->pluck('country_name','country_name');
                // get from country
                $response = $country;
                break;
            case 'origin_province':
            case 'origin_locker_province':
                $province = Province::get()->pluck('province_name','province_name');
                // get from province
                $response =  $province;
                break;
            case 'origin_city':
            case 'origin_locker_city':
                $city = City::get()->pluck('city_name','city_name');
                $response =  $city;
                break;
            case 'origin_locker_name':
                $name = LockerLocation::get()->pluck('name','name');
                $response = $name;
                break;
            case 'origin_locker_category':
                $name =BuildingType::get()->pluck('building_type','building_type');
                $response =  $name;
                break;
            case 'pickup_type':
                $pickupType = ['locker'=>'locker','address'=>'address'];
                $response = $pickupType;
                break;
            case 'topup_method' :
                // get payment list
                $param = $this->getPaymentMethod();
                $response = $param;
                break;
            case 'transaction_month' :
                $monthList = [
                    '01' => 'January',
                    '02' => 'February',
                    '03' => 'March',
                    '04' => 'April',
                    '05' => 'May',
                    '06' => 'June',
                    '07' => 'July',
                    '08' => 'August',
                    '09' => 'September',
                    '10' => 'October',
                    '11' => 'November',
                    '12' => 'December'
                ];
                $response = $monthList;
                break;
            case 'transaction_day' :
                $dayList = [
                    '1' => 'Monday',
                    '2' => 'Tuesday',
                    '3' => 'Wednesday',
                    '4' => 'Thursday',
                    '5' => 'Friday',
                    '6' => 'Saturday',
                    '7' => 'Sunday',
                ];
                $response = $dayList;
                break;
            case 'transaction_year' :
                $currentYear = date('Y');
                $yearList = [];
                for ($i=0;$i<10;$i++){
                    $yearList[$currentYear+$i] = $currentYear+$i;
                }
                $response = $yearList;
                break;
            case 'transaction_type':
                $pickupType = ['popsend'=>'popsend','popsafe'=>'popsafe'];
                $response = $pickupType;
                break;
            default :
                $response = 'Failed. Undefined Type';
                $code = 400;
                break;
        }

        return response()->json($response, $code);
    }

    public function update(Request $request) {
        
        $date = $request->date;
        $build = explode(' - ',$date);
        $start_time = date('Y-m-d H:i:s', strtotime($build[0]));
        $end_time = date('Y-m-d H:i:s', strtotime($build[1]));
        DB::connection('popsend')->beginTransaction();
        try {
            $detail = Campaign::with('parameters')->find($request->id);
            $parameterOld = $detail->parameters;
            $detail->name = $request->name;
            $detail->description = $request->description;
            $detail->start_time = $start_time;
            $detail->end_time = $end_time;
            $detail->promo_type = $request->campaign_type;
            $detail->status = ($request->status == 'active' ) ? '1' : '0';
            $detail->type = $request->amount_type;
            $detail->category = $request->category;
            $detail->voucher_required = ($request->voucher_required == 'yes') ? '1' : '0';
            $detail->voucher_type =$request->voucher_type;
            $detail->amount = $request->amount;
            $detail->max_amount = $request->maximum_amount;
            $detail->min_amount = $request->minimal_amount;
            $detail->limit_usage = $request->limit_usage;
            $detail->priority = $request->priority;
            $detail->save();

            //hapus dulu campaignparameter
            $campaignParameter = CampaignParameter::where('campaign_id',$request->id)->delete();
            //ambil rules
            $rules = $request->rules;
            foreach($rules as $item) {
                $operator = $item['operator'];
                $parameter = $item['parameter'];
                $value = $this->buildValue($operator,$item['value']);
                CampaignParameter::create([
                    'campaign_id'=>$request->id,
                    'available_parameter_id'=>$parameter,
                    'operator'=>$operator,
                    'value'=>$value
                ]);
            }
            DB::connection('popsend')->commit();
            return redirect('marketing/campaign/list');
        } catch (\Exception $e) {
            DB::connection('popsend')->rollback();
            return redirect()->back()->withErrors(['msg', 'Terjadi kesalahan dalam komunikasi ke server']);
        }
        
    }

    public function voucher($id) {
        $detail = Campaign::find($id);
        $listVoucher = Vouchers::where('campaign_id', $id)->get();
        return view('marketing.voucher',compact('detail','listVoucher'));
    }

    public function createVoucher(Request $request) {
        $format = $request->format;
        $length = $request->length;
        $total = $request->total;
        $index =1;
        $result = [];
        $code = 200;
        DB::connection('popsend')->beginTransaction();
        try {
            while ($index <= $total) {
                // array_push($result,$index);
                $voucher = $this->buildVoucher($format,$length, $index);
                Vouchers::create([
                    'campaign_id'=>$request->campaign_id,
                    'code'=>$voucher,
                    'usage'=>0
                ]);
                array_push($result,$voucher);
                $index++;
            }
            DB::connection('popsend')->commit();
        } catch (\Exception $e) {
            $code=400;
            DB::connection('popsend')->rollback();
        }
        
        return response()->json($result, $code);
    }
    function buildVoucher($format, $length, $index = 1) {
        $exploded = explode('-',$format);
        $result = '';
        foreach($exploded as $row){
            if($row === '{ordered_number}') {
                $result.=$this->buildOrder($index, $length);
            }elseif ($row === '{random_number}') {
                $result.=$this->buildRandomNumber($length);
            }elseif ($row === '{random_alpha}') {
                $result.=$this->buildRandomAlpha($length);
            }else{
                $result.=$row;
            }
        }
        return $result;
    }

    function buildOrder($index, $length) {
       return str_pad($index, $length, '0', STR_PAD_LEFT);
    }

    function buildRandomNumber($length) {
        return rand(pow(10, $length-1), pow(10, $length)-1);
    }

    function buildRandomAlpha($length) {
        $permitted_chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZ';
        return substr(str_shuffle($permitted_chars), 0, $length);
    }

    function getPaymentMethod() {
        // 1 is popbox
        $id = 7;
        $tokenPrivileges = TokenChannelPrivilege::where('company_access_tokens_id',$id)->get()->pluck('payment_channels_id','payment_channels_id');
        $channel = PaymentChannel::whereIn('id', $tokenPrivileges)->pluck('name','name');
        $results = [];
        foreach ($channel as $key => $value) {
            $results[str_replace(" ","-",$key)] = $value;
        }
        return $results;
    }

    function buildValue($operator,$value) {
        $singleValue = array('<','<=','=','>=','>');
        if(in_array($operator,$singleValue)) {
            return $value;
        }
        $newval = explode(',',$value);
        return json_encode($newval);
    }
}
