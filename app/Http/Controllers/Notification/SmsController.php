<?php

namespace App\Http\Controllers\Notification;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Sms;
use App\Http\Resources\SmsResource;
use DB;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Carbon\Carbon;
class SmsController extends Controller
{
    public function index() {
        $status = [
            'accepted',
            'delivered',
            'expired',
            'failed',
        ];
        $legend =[
            [
                "code"=>'Message Id',
                "description"=>"Nexmo ID untuk pesan terkait",
            ],
            [
                "code"=>'Status',
                "description"=>"Status untuk pesan terkait, 0 untuk sukses, 2 untuk gagal, untuk informasi lebih detail klik informasi error di bawah",
            ],
            [
                "code"=>'SCTS',
                "description"=>"Waktu saat pesan diterima oleh 'carrier' Nexmo",
            ],
            [
                "code"=>'Network Code',
                "description"=>"Kode dari provider( mobile country code)",
            ],
            [
                "code"=>'Message Timestamp',
                "description"=>"Waktu saat nexmo mengirim informasi ke server nusantara",
            ]
        ];

        $error = [
            [
                "code"=>"0",
                "description"=>"Terkirim"
            ],
            [
                "code"=>"1",
                "description"=>"Unknown (Pesan tidak dapat dikirim dikarenakan alasan yang tidak diketahui)"
            ],
            [
                "code"=>"2",
                "description"=>"Absent Subscriber- Temporary (Pesan tidak dapat dikirim karena subscriber/target tidak dapat dijangkau, bisa dicoba untuk dikirim ulang )"
            ],
            [
                "code"=>"3",
                "description"=>"Absent Subscriber - Permanent ( Pesan tidak dapat dikirim karena subscriber sudah tidak aktif, disarankan untuk dihapus dari target pengiriman )"
            ],
            [
                "code"=>"4",
                "description"=>"Call barred by User ( Nomor tujuan tidak dapat menerima SMS bisa karena pulsa target tidak adam black list oleh carrier, dll )"
            ],
            [
                "code"=>"5",
                "description"=>"Portability Error ( Jika mendapatkan error ini laporkan ke Nexmo dan/atau Network Provider)"
            ],
            [
                "code"=>"6",
                "description"=>"Anti-Spam Rejection ( Konten pesan terindikasi spam dan terkena anti-spam filter dari provider ataupun user)"
            ],
            [
                "code"=>"7",
                "description"=>"Handset Busy (Target tidak dapat dihubungi saat pesan sedang dikirim, bisa diulang di lain waktu)"
            ],
            [
                "code"=>"8",
                "description"=>"Network Error ( Masalah terkait jaringan dan dapat dicoba diulangi di lain waktu)"
            ],
            [
                "code"=>"9",
                "description"=>"Illegal Number ( Pesan dikirimkan kepada nomor yang sudah terdaftar di dalam Blacklist)"
            ],
            [
                "code"=>"10",
                "description"=>"Invalid Message ( KEsalahan dari konten pesan seperti encoding)"
            ],
            [
                "code"=>"11",
                "description"=>"Unroutable (Pesan gagal dikirimkan ke tujuan karena gagal diarahkan dalam 'route' nexmo, segera contact support@nexmo.com)"
            ],
            [
                "code"=>"12",
                "description"=>"Destination Un-Reachable (Route dari nexmo tidak menemukan jalur mengirim ke nomor target)"
            ],
            [
                "code"=>"13",
                "description"=>"Subscriber Age Restriction (Target tidak dapat menerima karena ketentuan umur)"
            ],
            [
                "code"=>"14",
                "description"=>"Number block by carrier (Target tidak memiliki layanan sms terhadap provider)"
            ],
            [
                "code"=>"15",
                "description"=>"Pre-Paid Insufficient Funds (Target yang menggunakan paket pre-paid tidak memiliki pulsa untuk menerima sms)"
            ],
            [
                "code"=>"99",
                "description"=>"General Error ( Segera Hubungi support@nexmo.com )"
            ],
        ];
        return view('notification.sms', compact('status','legend','error'));
    }

    public function source(Request $request) {
        $limit = isset($request->length) ? $request->length : 10; 
        $page =  ( $request->start / $limit ) + 1;
        $rangeDate = $request->date;
        $startDate = null;
        $endDate = null;
        if (!empty($rangeDate)) {
            $date = explode(" - ", $rangeDate);
            $startDate = $date[0].' 00:00:00';
            $endDate = $date[1].' 23:59:59';
        }
        $filter = [
            "msisdn"=>$request->phone,
            "status"=>$request->status,
        ];
        $filter = array_filter($filter);
        $total = Sms::all()->count();
        $db = Sms::where($filter)->orderBy('message_timestamp','desc');
            if(!empty($rangeDate)) {
                $db->whereBetween('message_timestamp',[$startDate, $endDate]);
            };
        $result =    $db->paginate($limit,['*'],'page',$page);
        $data = SmsResource::collection($result);
        $collection = [
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => $total,  
            "recordsFiltered" => $result->total(), 
            "data"            => $data   
        ];
        return response()->json($collection, 200);
    }

    public function getCount() {
        $group =Sms::select('status', DB::raw('count(*) as total'))
                 ->groupBy('status')
                 ->get();
        return response()->json($group, 200);
    }

    public function excel(Request $request) {
        $filter = [
            "msisdn"=>$request->get('phone'),
            "status"=>$request->get('status'),
        ];
        $filter = array_filter($filter);
        $data = Sms::where($filter)->get();
        
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        // style
        $bold = [
            'font' => [
                'bold' => true,
                'size' => 20
            ]
        ];

        // title
        $sheet->mergeCells('A1:E1');
        $sheet->setCellValue('A1', 'Nexmo Response');
        $sheet->getStyle('A1')->applyFromArray($bold);
        $sheet->mergeCells('A2:E2');
        if (!empty($transactionDate)) {
        $sheet->setCellValue('A2', $start.' - '.$end);
        }
         // header
        $count = 4;
        $sheet->setCellValue('A'.$count, 'No');
        $sheet->setCellValue('B'.$count, 'Phone Number');
        $sheet->setCellValue('C'.$count, 'Status');
        $sheet->setCellValue('D'.$count, 'Message Id');
        $sheet->setCellValue('E'.$count, 'Error Code');
        $sheet->setCellValue('F'.$count, 'SCTS');
        $sheet->setCellValue('G'.$count, 'Network Code');
        $sheet->setCellValue('H'.$count, 'Message Timestamp');

        if($data->count() > 0) {
            $no = 1;
            foreach($data as $row) {
                $count = $count+1;
                $sheet->setCellValue('A'.$count, $no++);
                $sheet->setCellValue('B'.$count, $row['msisdn']);
                $sheet->setCellValue('C'.$count, ucfirst($row['status']));
                $sheet->setCellValue('D'.$count, $row['message_id']);
                $sheet->setCellValue('E'.$count, $row['error_code']);
                $sheet->setCellValue('F'.$count, $row['scts']);
                $sheet->setCellValue('G'.$count, $row['network_code']);
                $sheet->setCellValue('H'.$count, $row['message_timestamp']);
            } 
        }else{
            $count = $count+1;
            $sheet->mergeCells('A'.$count.':Q'.$count);
            $sheet->setCellValue('A'.$count, 'No records found');
        }

        $dateNow = date("ymd");
        $filename = $dateNow.' Nexmo-Response.xlsx';
        
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xlsx");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'.$filename.'"');
        $writer->save("php://output");
    }
}
