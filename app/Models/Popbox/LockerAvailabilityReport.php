<?php

namespace App\Models\Popbox;

use Illuminate\Database\Eloquent\Model;

class LockerAvailabilityReport extends Model
{
    protected $connection = 'popbox_db';
    protected $table = 'tb_locker_availability_report';
}
