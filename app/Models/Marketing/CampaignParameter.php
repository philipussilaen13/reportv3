<?php

namespace App\Models\Marketing;

use Illuminate\Database\Eloquent\Model;

class CampaignParameter extends Model
{
    protected $connection = 'popsend';
    protected $table = 'campaign_parameters';
    protected static $singleValue = array('<','<=','=','>=','>');
    protected static $doubleValue = array('between');
    protected static $multipleValue = array('exist','except');
    protected $guarded = ['id'];
     /*Relationship*/
     public function parameter(){
        return $this->belongsTo('App\Models\Marketing\AvailableParameter','available_parameter_id','id');
    }

    public function campaign()
    {
        return $this->belongsTo('App\Models\Marketing\Campaign', 'campaign_id');
    }
}
