<?php

namespace App\Models\NewLocker;

use Illuminate\Database\Eloquent\Model;

class DetailExpress extends Model
{
    protected $connection = 'newlocker_db';
    protected $table = 'tb_detail_express';
}
