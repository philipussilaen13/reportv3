<?php

namespace App\Models\Popapps;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Helpers\Helper;

class CampaignUsage extends Model
{
    protected $connection = 'popsend';
    protected $table = 'campaign_usages';

    public function transaction(){
        return $this->belongsTo(Transaction::class);
    }

    public function campaign(){
        return $this->belongsTo(Campaign::class);
    }
}