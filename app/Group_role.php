<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group_role extends Model
{
    // Connect to roles table
    protected $table = 'group_roles';

    // menyimpan data dengan timestamps(created_at, updated_at, delete_at)
    public $timestamps = true;
}
