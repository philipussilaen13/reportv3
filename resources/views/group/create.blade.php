@extends('layout.main')
{{-- Do Not Edit this file. This is Base File for View --}}

@section('title')
	Add a New Group
@endsection

@section('css')
	{{-- expr --}}
@endsection

@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ url('group') }}">Group</a></li>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="material-card card">
            <div class="card-body">
                <h4 class="card-title">Form Group</h4>
            </div>
            <hr>
                            <form id="add-new-role" method="post" action="{{ url('group/store') }}">
                            @csrf
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="name">Group Name</label>
                                            <input type="text" class="form-control" id="name" name="name">
                                    </div>
                                </div>
                                <hr>
                                <div class="card-body">
                                    <h4 class="card-title">List of Roles</h4>
                                    <div class="form-group row pt-3">
                                        <div class="col-sm-4">
                                            @foreach ($roles as $key => $row)
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="check{{ $key }}" name="role_id[]" value="{{ $row->id }}">
                                                    <label class="custom-control-label" for="check{{ $key }}">{{ $row->name }}</label>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="card-body">
                                    <div class="form-group mb-0 text-right">
                                        <button type="submit" class="btn btn-info waves-effect waves-light">Save</button>
                                        <button type="reset" class="btn btn-dark waves-effect waves-light">Cancel</button>
                                    </div>
                                </div>
                            </form>
		</div>
	</div>
</div>
@endsection

@section('js')
	{{-- expr --}}
@endsection