<?php

namespace App\Models\NewLocker;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    const UPDATED_AT = 'last_update';
    const CREATED_AT = 'last_update';

    // set connection and table
    protected $connection = 'newlocker_db';
    protected $table = 'tb_newlocker_user';
    protected $primaryKey = 'id_user';
    public $incrementing = false;
    public $timestamps = false;

    public function express(){
        return $this->hasMany(Express::class,'storeUser_id','id_user');
    }
}
