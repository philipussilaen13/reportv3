@extends('layout.main')

@section('title')
	Payment
@endsection

@section('css')
<link href="{{ asset('assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css') }}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/libs/daterangepicker/daterangepicker.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/libs/select2/dist/css/select2.min.css') }}">
@endsection

@section('content')
{{-- counter --}}
<div class="row">
        <div class="col-sm-6 col-md-6">
            <a id="created" href="javascript:void(0)">
                <div class="card bg-dark">
                    <div class="card-body text-white">
                        <div class="d-flex no-block align-items-center">
                            <div>
                            <h2 id="paid_id" class="count font-medium mb-0">0</h2>
                            <h5 class="filterVal">Total Paid Transactions : <span id="count_id">0</span></h5>
                            </div>
                            <div class="ml-auto">
                                <span class="text-white display-6"><i class="flag-icon flag-icon-id"></i></span>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-sm-6 col-md-6">
            <a id="created" href="javascript:void(0)">
                <div class="card bg-dark">
                    <div class="card-body text-white">
                        <div class="d-flex no-block align-items-center">
                            <div>
                            <h2 id="paid_my" class="count font-medium mb-0">0</h2>
                            <h5 class="filterVal">Total Paid Transactions : <span id="count_my">0</span></h5>
                            </div>
                            <div class="ml-auto">
                                <span class="text-white display-6"><i class="flag-icon flag-icon-my"></i></span>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
</div>
{{-- filter box --}}
<div class="row">
    <div class="col-12">
        <div class="material-card card">
                <form id="form-filter" class="form-horizontal">
                        <div class="card-body">
                            <h4 class="card-title">Filter 
                                <a id="linkFilter" data-toggle="collapse" href="#collapseFilter" role="button" aria-expanded="false" aria-controls="collapseFilter" style="float:right;"> <button id="btnCollapse" class="btn btn-secondary"> SHOW FILTER <i class="fas fa-chevron-right"></i> </button> </a>
                            </h4>
                            <div class="row button-group mb-4">
                                <div class="col-12" id="info-search"> 
                                    <button id="btnDate" type="button" class="btn btn-outline-secondary" style="display:none"></button>
                                    <button id="btnPaymentId" type="button" class="btn btn-outline-secondary filter" style="display:none"></button>
                                    <button id="btnSubPaymentId" type="button" class="btn btn-outline-secondary filter" style="display:none"></button>
                                    <button id="btnClientTransactionId" type="button" class="btn btn-outline-secondary filter" style="display:none"></button>
                                    <button id="btnChannel" type="button" class="btn btn-outline-secondary filter" style="display:none"></button>
                                    <button id="btnVA" type="button" class="btn btn-outline-secondary filter" style="display:none"></button>
                                    <button id="btnType" type="button" class="btn btn-outline-secondary filter" style="display:none"></button>
                                    <button id="btnClient" type="button" class="btn btn-outline-secondary filter" style="display:none"></button>
                                    <button id="btnStatus" type="button" class="btn btn-outline-secondary filter" style="display:none"></button>
                                    <button id="btnName" type="button" class="btn btn-outline-secondary filter" style="display:none"></button>
                                    <button id="btnEmail" type="button" class="btn btn-outline-secondary filter" style="display:none"></button>
                                    <button id="btnPhone" type="button" class="btn btn-outline-secondary filter" style="display:none"></button>
                                    <button id="btnDescription" type="button" class="btn btn-outline-secondary filter" style="display:none"></button>
                                </div>
                            </div>
                            <div class="row button-group mb-4">
                                <div class="col-12">
                                </div>
                            </div>
                            <div class="collapse" id="collapseFilter">
                            <hr>
                            <div class="row">
                                <div class="col-sm-12 col-lg-4">
                                    <div class="form-group">
                                        <label for="payment_id" class="text-right control-label col-form-label">Payment ID</label>
                                        <input type="text" class="form-control" id="payment_id" name="payment_id">
                                    </div>
                                </div>
                                <div class="col-sm-12 col-lg-4">
                                    <div class="form-group">
                                        <label for="sub_payment_id" class="text-right control-label col-form-label">Sub Payment ID</label>
                                        <input type="text" class="form-control" id="sub_payment_id" name="sub_payment_id">
                                    </div>
                                </div>
                                <div class="col-sm-12 col-lg-4">
                                    <div class="form-group">
                                        <label for="module" class="text-right control-label col-form-label">Client Transaction ID</label>
                                        <input type="text" class="form-control" id="client_trans_id" name="client_trans_id">
                                    </div>
                                </div>
                                <div class="col-sm-12 col-lg-4">
                                    <div class="form-group">
                                        <label for="payment_channel" class="text-right control-label col-form-label">Payment Channel</label>
                                        <select class="form-control select2" id="payment_channel" name="payment_channel" style="width: 100%; height:36px;">
                                            <option value="">All Channel</option>
                                            @foreach($payment_channel as $channel)
                                            <option value="{{ $channel->id }}">{{ $channel->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-lg-4">
                                        <div class="form-group">
                                            <label for="module" class="text-right control-label col-form-label">VA Number</label>
                                            <input type="text" class="form-control" id="va_number" name="va_number">
                                        </div>
                                </div>
                                <div class="col-sm-12 col-lg-4">
                                    <div class="form-group">
                                        <label for="type" class="text-right control-label col-form-label">Type</label>
                                        <select class="form-control select2" id="type" name="type" style="width: 100%; height:36px;">
                                            <option value="">All Type</option>
                                            <option value="fixed">FIXED</option>
                                            <option value="open">OPEN</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-lg-4">
                                        <div class="form-group">
                                            <label for="type" class="text-right control-label col-form-label">Client</label>
                                            <select class="form-control select2" id="client" name="client" style="width: 100%; height:36px;">
                                                <option value="">All</option>
                                            </select>
                                        </div>
                                </div>

                                <div class="col-sm-12 col-lg-4">
                                        <div class="form-group">
                                            <label for="type" class="text-right control-label col-form-label">Status</label>
                                            <select class="form-control select2" id="status" name="status" style="width: 100%; height:36px;">
                                                <option value="">All Status</option>
                                                <option value="CREATED">CREATED</option>
                                                <option value="PAID">PAID</option>
                                                <option value="FAILED">FAILED</option>
                                                <option value="PENDING">PENDING</option>
                                                <option value="EXPIRED">EXPIRED</option>
                                            </select>
                                        </div>
                                </div>
                                <div class="col-sm-12 col-lg-4">
                                    <div class="form-group">
                                        <label for="key" class="text-right control-label col-form-label">Customer Name</label>
                                        <input type="text" class="form-control" id="customer_name" name="customer_name">
                                    </div>
                                </div>

                                <div class="col-sm-12 col-lg-4">
                                        <div class="form-group">
                                            <label for="module" class="text-right control-label col-form-label">Customer Email</label>
                                            <input type="text" class="form-control" id="customer_email" name="customer_email">
                                        </div>
                                </div>

                                <div class="col-sm-12 col-lg-4">
                                        <div class="form-group">
                                            <label for="module" class="text-right control-label col-form-label">Customer Phone</label>
                                            <input type="text" class="form-control" id="customer_phone" name="customer_phone">
                                        </div>
                                </div>

                                <div class="col-sm-12 col-lg-4">
                                        <div class="form-group">
                                            <label for="module" class="text-right control-label col-form-label">Description</label>
                                            <input type="text" class="form-control" id="description" name="description">
                                        </div>
                                </div>

                                <div class="col-sm-12 col-lg-4">
                                    <div class="form-group">
                                        <label for="date" class="text-right control-label col-form-label">Created Date</label>
                                        <input type="text" class="form-control daterange" id="daterange" name="date-range">
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group mb-0 text-right">
                                <a href="javascript:void(1);" id="btnFilter" class="btn btn-info waves-effect waves-light">Filter</a>
                                <a href="javascript:void(1);" id="btnReset" class="btn btn-dark waves-effect waves-light">Reset</a>
                            </div>
                            </div>
                        </div>
                    </form>
        </div>
    </div>
</div>

{{-- content table --}}
<div class="row">
    <div class="col-12">
        <div class="material-card card">
            <div class="card-body row">
                <div class="col-sm-12 col-md-6">
                    <h4 class="card-title">Transaction List</h4>
                    <h6 class="card-subtitle">CLient Transaction List</h6>
                </div>
                <div class="col-sm-12 col-md-6">
                    <button id="popExcel" class="btn btn-primary mb-3" style="float:right;"> Download </button>
                </div>
                <div class="table-responsive">
                    <table id="source-table" class="table table-striped border text-inputs-searching" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Channel</th>
                                    <th>Payment Id</th>
                                    <th>Client Transaction</th>
                                    <th>VA Number</th>
                                    <th>Customer Name</th>
                                    <th>Customer Paid Amount</th>
                                    <th>Customer Recieved</th>
                                    <th>Status</th>
                                    <th>Description</th>
                                    <th>Created At</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Channel</th>
                                    <th>Payment Id</th>
                                    <th>Client Transaction</th>
                                    <th>VA Number</th>
                                    <th>Customer Name</th>
                                    <th>Customer Paid Amount</th>
                                    <th>Customer Recieved</th>
                                    <th>Status</th>
                                    <th>Description</th>
                                    <th>Created At</th>
                                </tr>
                            </tfoot>
                        </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="{{ asset('assets/libs/moment/moment.js') }}"></script>
<script src="{{ asset('assets/extra-libs/DataTables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/libs/daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('assets/libs/select2/dist/js/select2.min.js') }}"></script>
<script src="{{ asset('js/common.js') }}"></script>

<script>
    $(document).ready(function() {
    $('.select2').select2();
    var daterange = $('#daterange').val();
    if(daterange != '') { $('#btnDate').show(); $('#btnDate').text('Date : ' + daterange); }

    $('#client').select2({
        ajax: {
            url: '{{ url("report/payment/clients") }}',
            dataType: 'json',
            data: function (params) {
                var query = {
                    search: params.term,
                }
                // Query parameters will be ?search=[term]&page=[page]
                return query;
            },
            processResults: function(data) {
                return {
                        results: $.map(data, function(obj) {
                            return {
                                id: obj.id,
                                text: obj.text
                            };
                        })
                    };
            }
        }
    });

    getTable({
        "_token" : "{{ csrf_token() }}",
        "date": $('#daterange').val()
    });

    getAmount({
        "_token" : "{{ csrf_token() }}"
    });
})

function getAmount(data) {
    $.ajax({
    type: "GET",
    url: "{{ url('report/payment/amount') }}",
    data: data,
    success: function(result){
        $("#paid_id").html('Rp'+formatNumber(result.id))
        $("#paid_my").html('RM'+formatNumber(result.my))
        $("#count_id").html(result.count_id)
        $("#count_my").html(result.count_my)
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) {
        alert('Terjadi kesalahan, segera hubungi admin');
        }
    });
}
$('#btnFilter').on('click', function() {
    $('#source-table').DataTable().clear().destroy();
    const payload = {
        "_token" : "{{ csrf_token() }}",
        "date": $('#daterange').val(),
        "payment_id": $('#payment_id').val(),
        "payment_channel": $('#payment_channel').val(),
        "sub_payment_id": $('#sub_payment_id').val(),
        "client_trans_id": $('#client_trans_id').val(),
        "va_number": $('#va_number').val(),
        "type": $('#type').val(),
        "status": $('#status').val(),
        "customer_email": $('#customer_email').val(),
        "customer_phone": $('#customer_phone').val(),
        "description": $('#description').val(),
        "company_access_tokens_id": $('#client').val(),
        "customer_name":$('#customer_name').val()
    }

    var channel = $('#payment_channel').select2('data')
    var client = $('#client').select2('data')
    $('.filter').hide();
    
    if($('#daterange').val() != '') { $('#btnDate').show(); $('#btnDate').text('Date : ' + $('#daterange').val()); }
	if($('#payment_id').val() != '') { $('#btnPaymentId').show(); $('#btnPaymentId').text('Payment ID : ' + $('#payment_id').val()); }
	if($('#sub_payment_id').val() != '') { $('#btnSubPaymentId').show(); $('#btnSubPaymentId').text('Sub Payment ID : ' + $('#sub_payment_id').val()); }
	if($('#client_trans_id').val() != '') { $('#btnClientTransactionId').show(); $('#btnClientTransactionId').text('Client Transaction ID : ' + $('#client_trans_id').val()); }
	if($('#payment_channel').val() != '') { $('#btnChannel').show(); $('#btnChannel').text('Channel : ' + channel[0].text) }
    if($('#va_number').val() != '') { $('#btnVA').show(); $('#btnVA').text('VA Number: ' + $('#va_number').val()); }
    if($('#type').val() != '') { $('#btnType').show(); $('#btnType').text('Type : ' + $('#type').val()); }
    if($('#client').val() != '') { $('#btnClient').show(); $('#btnClient').text('Client : ' + client[0].text) }
    if($('#status').val() != '') { $('#btnStatus').show(); $('#btnStatus').text('Status : ' + $('#status').val()); }
    if($('#customer_name').val() != '') { $('#btnName').show(); $('#btnName').text('Name : ' + $('#customer_name').val()); }
	if($('#customer_email').val() != '') { $('#btnEmail').show(); $('#btnEmail').text('Email : ' + $('#customer_email').val()); }
	if($('#customer_phone').val() != '') { $('#btnPhone').show(); $('#btnPhone').text('Phone : ' + $('#customer_phone').val()); }
	if($('#description').val() != '') { $('#btnDescription').show(); $('#btnDescription').text('Description : ' + $('#description').val()); }

    getTable(payload)
})

$('#btnReset').on('click', function() {
    $('#source-table').DataTable().clear().destroy();
    $('.filter').hide();
    resetDate();
    $('#payment_id').val('');
    $('#sub_payment_id').val('');
    $('#client_trans_id').val('');
    $('#payment_channel').val('').trigger('change');
    $('#va_number').val('');
    $('#type').val('').trigger('change');
    $('#status').val('').trigger('change');
    $('#client').val('').trigger('change');
    $('#customer_email').val('');
    $('#customer_name').val('');
    $('#customer_phone').val('');
    $('#description').val('');
    const payload = {
        "_token" : "{{ csrf_token() }}",
        "date": $('#daterange').val()
    }
    $('.filter').hide();
    $('#btnCollapse').html('SHOW FILTER <i class="fas fa-chevron-right"></i>');
    $('#collapseFilter').removeClass('show');
    getTable(payload)
})

function resetDate() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; // January is 0

    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
    var today = yyyy + '-' + mm + '-' + dd;
    var startDate = yyyy + '-' + mm + '-' + '01';
    $('#daterange').daterangepicker({
        startDate: startDate,
        maxDate: today,
        alwaysShowCalendars: true,
        locale: {
            format: 'YYYY-MM-DD'
        }
    })
}

function getTable(param = {}) {
    
    return $('#source-table').DataTable( {
          processing: true,
          serverSide: true,
          bFilter: false,
          ajax: {
              url: "{{ url('report/payment/source-transaction') }}",
              dataType: "json",
              type: "POST",
              data: param
          },
          columns: [
            { 
            "data": "channel",
            "name": "channel",
            "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            { 
            "data": "payment_id",
            "name": "payment_id",
            "render": function ( data, type, full, meta ) {
                    return "<a href='{{ url('report/payment') }}/"+full.sub_payment_id+"' style='color:#313131'>"+'<strong>'+data+'</strong></br>'+full.sub_payment_id+"</a>";
                }
            },
            { 
            "data": "transaction",
            "name": "transaction",
            "render": function ( data, type, full, meta ) {
                 return data;
                }
            },
            { 
            "data": "va_number",
            "name": "va_number",
            "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            { 
            "data": "customer_name",
            "name": "customer_name",
            "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            { 
            "data": "customer_paid_amount",
            "name": "customer_paid_amount",
            "render": function ( data, type, full, meta ) {
                    if(full.country == 'Indonesia') {    
                        return 'Rp'+formatNumber(data);
                    } else {
                        return 'RM'+formatNumber(data);
                    }
                }
            },
            { 
            "data": "customer_received",
            "name": "customer_received",
            "render": function ( data, type, full, meta ) {
                    if(full.country == 'Indonesia') {    
                        return 'Rp'+formatNumber(data);
                    } else {
                        return 'RM'+formatNumber(data);
                    }
                }
            },
            { 
            "data": "status",
            "name": "status",
            "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            { 
            "data": "description",
            "name": "description",
            "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            { 
            "data": "created_at",
            "name": "created_at",
            "render": function ( data, type, full, meta ) {
                    return formatDate(data);
                }
            },
          ]
          
      } );
}

$('#popExcel').click(function () {
    var search = $('#form-filter').serialize();
    window.location.href = "{{ url('report/payment/transaction-excel/') }}?"+search;
});
</script>
@endsection
