@extends('layout.main')

@section('title')
	Home
@endsection

@section('css')
	{{-- expr --}}
@endsection

@section('breadcrumb')
    {{-- expr --}}
@endsection

@section('content')
<!-- ============================================================== -->
<!-- First Cards Row  -->
<!-- ============================================================== -->
@php
setlocale(LC_MONETARY,"en_ID");
$daily = 1000000;
$weekly = 6946005;
$monthly = 25780015;
$yearly = 25780015;
@endphp
<div class="row">
    <div class="col-md-6 col-lg-3">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title text-uppercase">Daily Sales</h5>
                <div class="text-right">
                    <span class="text-muted">Today's Income</span>
                    <h2 class="mt-2 display-7"><sup><i class="ti-arrow-up text-success"></i></sup>Rp{{ number_format($daily) }}</h2>
                </div>
                <span class="text-success">20%</span>
                <div class="progress">
                    <div class="progress-bar bg-success" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-lg-3">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title text-uppercase">Weekly Sales</h5>
                <div class="text-right">
                    <span class="text-muted">Weekly Income</span>
                    <h2 class="mt-2 display-7"><sup><i class="ti-arrow-down text-danger"></i></sup>Rp{{ number_format($weekly) }}</h2>
                </div>
                <span class="text-success">30%</span>
                <div class="progress">
                    <div class="progress-bar bg-danger" role="progressbar" style="width: 30%" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-lg-3">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title text-uppercase">Monthly Sales</h5>
                <div class="text-right">
                    <span class="text-muted">Monthly Income</span>
                    <h2 class="mt-2 display-7"><sup><i class="ti-arrow-up text-info"></i></sup>Rp{{ number_format($monthly) }}</h2>
                </div>
                <span class="text-info">60%</span>
                <div class="progress">
                    <div class="progress-bar bg-info" role="progressbar" style="width: 60%" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-lg-3">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title text-uppercase">Yearly Sales</h5>
                <div class="text-right">
                    <span class="text-muted">Yearly Income</span>
                    <h2 class="mt-2 display-7"><sup><i class="ti-arrow-up text-inverse"></i></sup>Rp{{ number_format($yearly) }}</h2>
                </div>
                <span class="text-inverse">20%</span>
                <div class="progress">
                    <div class="progress-bar bg-inverse" role="progressbar" style="width: 80%" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- Country Visit, Weather cards Row  -->
<!-- ============================================================== -->
<div class="row">
    <div class="col-md-12 col-lg-6">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title text-uppercase">Top Earning</h5>
                <div id="basic-line" style="height:400px;"></div>
            </div>
        </div>
    </div>
    <div class="col-md-12 col-lg-6">
        <div class="card">
            <div class="p-3">
                <div class="d-flex align-items-center">
                    <div>
                        <h4 class="card-title text-uppercase">Notification</h4>
                        <h5 class="card-subtitle mb-0">Last 5 Days</h5>
                    </div>
                    <div class="ml-auto">
                        <select class="form-control">
                            <option>January 2018</option>
                            <option>February 2018</option>
                            <option>March 2018</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="message-box p-3 bg-light">
                <ul class="list-style-none">
                    <!-- Message -->
                    <li class="d-flex py-3">
                        <div class="user-img">
                            <span class="round bg-danger">
                            <i class="fas fa-box-open"></i>
                            </span>
                        </div>
                        <div class="mail-contnet ml-3">
                            <h5 class="mb-0">PopBox @ Park 51 Block D /h5>
                            <span class="mail-desc d-block text-muted">060301643818557</span>
                            <span class="time d-block font-medium">09:10 AM</span>
                        </div>
                    </li>
                    <!-- Message -->
                    <li class="d-flex py-3">
                        <div class="user-img">
                            <span class="round bg-success">
                            <i class="fas fa-shopping-cart"></i>
                            </span>
                        </div>
                        <div class="mail-contnet ml-3">
                            <h5 class="mb-0">WARUNG BABE</h5>
                            <span class="mail-desc d-block text-muted">Good Day Mocacino 20 gr x 10 pcs 21,800</span>
                            <span class="time d-block font-medium">09:08 AM</span>
                        </div>
                    </li>
                    <!-- Message -->
                    <li class="d-flex py-3">
                        <div class="user-img">
                            <span class="round">
                            <i class="fas fa-dollar-sign"></i>
                            </span>
                        </div>
                        <div class="mail-contnet ml-3">
                            <h5 class="mb-0">POPWARUNG MAMA TEGAR</h5>
                            <span class="mail-desc d-block text-muted">Transaction Indosat Rp 5.000</span>
                            <span class="time d-block font-medium">08:46 AM</span>
                        </div>
                    </li>
                    <!-- Message -->
                    <li class="d-flex py-3">
                        <div class="user-img">
                            <span class="round bg-primary">
                            <i class="fas fa-credit-card"></i>
                            </span>
                        </div>
                        <div class="mail-contnet ml-3">
                            <h5 class="mb-0">POPWARUNG HESTI BRO</h5>
                            <span class="mail-desc d-block text-muted">Top Up Cash by Ramdani Anshori Muslim Ref:AT1901220339R4D</span>
                            <span class="time d-block font-medium">07:30 AM</span>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- Country Visit, Weather cards Row  -->
<!-- ============================================================== -->
<div class="row">
    <div class="col-md-12 col-lg-6">
        <div class="card">
            <div class="card-body">
                <div class="d-flex align-items-center">
                    <div>
                        <h4 class="card-title text-uppercase">Top Selling</h4>
                        <h5 class="card-subtitle mb-0">Last 5 Days</h5>
                    </div>
                    <div class="ml-auto">
                        <select class="form-control">
                            <option>January 2018</option>
                            <option>February 2018</option>
                            <option>March 2018</option>
                        </select>
                    </div>
                </div>
                <div id="morris-line-chart" style="height: 390px;"></div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="../../assets/libs/chartist/dist/chartist.min.js"></script>
<script src="../../dist/js/pages/chartist/chartist-plugin-tooltip.js"></script>
<script src="../../assets/libs/raphael/raphael.min.js"></script>
<script src="{{ asset('assets/libs/morris.js/morris.min.js') }}"></script>
<script src="../../assets/libs/echarts/dist/echarts-en.min.js"></script>
<script src="{{ asset('dist/js/pages/dashboards/dashboard6.js') }}"></script>
@endsection