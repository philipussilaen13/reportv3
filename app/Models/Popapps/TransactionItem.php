<?php

namespace App\Models\Popapps;

use Illuminate\Database\Eloquent\Model;

class TransactionItem extends Model
{
    protected $connection = 'popsend';
    protected $table = 'transaction_items';
}
