<?php

namespace App\Http\Controllers\Popapps;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Popapps\Popsafe;
use App\Models\Popapps\PopsafeHistory;
use App\Models\Popapps\Transaction;
use App\Models\Popapps\TransactionHistory;
use App\Models\Popapps\Delivery;
use App\Models\Popapps\PaymentTransaction;
use App\Models\Popapps\PaymentTransactionHistory;
use App\Models\Popapps\PaymentMethod;
use App\Models\Popbox\Country;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $groupName = 'POPBOX';
        $status = Transaction::orderBy('status')->distinct()->get(['status'])->pluck('status','status');
        $location = Popsafe::distinct()->orderBy('locker_name','asc')->get(['locker_name'])->pluck('locker_name');
        $paymentMethod = PaymentMethod::orderBy('name','asc')->get()->toArray();
        $countries = Country::all();
        $transactionType = Transaction::orderBy('transaction_type')->distinct()->get(['transaction_type'])->pluck('transaction_type');

        return view('popapps.transactions.transaction', compact('groupName','status','location','paymentMethod','transactionType','countries'));
    }

    public function getLandingAPS(Request $request)
    {
        $groupName = 'APS';
        $status = Transaction::orderBy('status')->distinct()->get(['status'])->pluck('status','status');
        $location = ['DPS 01', 'DPS 02', 'SRG 01']; // locker location, locker name
        $paymentMethod = PaymentMethod::all()->toArray();
        $transactionType = Transaction::orderBy('transaction_type')->distinct()->get(['transaction_type'])->pluck('transaction_type');

        return view('popapps.transactions.transaction', compact('groupName','status','location','paymentMethod','transactionType'));
    }
    
    public function getDetailByInvoiceId(Request $request, $invoice_code)
    {
        $detail = Transaction::getDetailByInvoiceId($invoice_code);
        
        return response()->json([
            'invoice_code' => $invoice_code,
            'detail' => $detail
        ]);
    }

    public function getAjaxServerTransactionList(Request $request)
    {
        $parameter = $request->route()->parameters();
        $groupName = $parameter['groupName'];
        // Columns Data
        $columns = array( 
            0 => 'no',
            1 => 'invoice_id', 
            2 => 'transaction_type',
            3 => 'transaction_date',
            4 => 'popsafe_code',
            5 => 'user',
            6 => 'name',
            7 => 'phone',
            8 => 'email',
            9 => 'transaction_description',
            10=> 'location',
            11=> 'transaction_status',
            12=> 'payment',
            13=> 'counter',
            14=> 'total_price',
            15=> 'promo_amount',
            16=> 'paid_amount',
            17=> 'code',
            18=> 'service_charge',
            19=> 'revenue',
            20=> 'settlement',
            21=> 'country'
        );
  
        $invoiceId = $request->input('transactionId', null);
        if(!empty($request->input('columns.1.search.value'))) {
            $invoiceId = $request->input('columns.1.search.value');
        }
        $transactionType = $request->input('transactionType', null);
        if(!empty($request->input('columns.2.search.value'))) {
            $transactionType = $request->input('columns.2.search.value');
        }
        $transactionDate = $request->input('transactionDate', null);
        if(!empty($request->input('columns.3.search.value'))) {
            $transactionDate = $request->input('columns.3.search.value');
        } else {
            $transactionDate = date('Y-m')."-01 - ".date('Y-m-d');
        }
        $user = $request->input('user', null);
        if(!empty($request->input('columns.6.search.value'))) {
            $user = $request->input('columns.6.search.value');
        }
        $phone = $request->input('phone', null);
        if(!empty($request->input('columns.7.search.value'))) {
            $phone = $request->input('columns.7.search.value');
        }
        $location = $request->input('location', null);
        if(!empty($request->input('columns.10.search.value'))) {
            $location = $request->input('columns.10.search.value');
        }
        $status = $request->input('status', null);
        if(!empty($request->input('columns.11.search.value'))) {
            $status = $request->input('columns.11.search.value');
        }
        $payment = $request->input('paymentMethod', null);
        if(!empty($request->input('columns.12.search.value'))) {
            $payment = $request->input('columns.12.search.value');
        }
        $code = $request->input('code', null);
        if(!empty($request->input('columns.17.search.value'))) {
            $code = $request->input('columns.17.search.value');
        }
        $country = $request->input('country', null);
        if(!empty($request->input('columns.21.search.value'))) {
            $country = $request->input('columns.21.search.value');
        }

        $startDate = null;
        $endDate = null;
        if (!empty($transactionDate)) {
            $date = explode(" - ", $transactionDate);
            $startDate = $date[0]." 00:00:00";
            $endDate = $date[1]." 23:59:59";
        }

        $allData = Transaction::select('transactions.invoice_id','transactions.transaction_type','transactions.description as transaction_description','transactions.status as transaction_status','transactions.created_at as transaction_date','transactions.total_price','transactions.paid_amount','transactions.promo_amount','popsafes.invoice_code as popsafe_code','popsafes.locker_name','v.code','deliveries.invoice_code as delivery_code','deliveries.origin_name','u.name','u.phone','u.member_id','u.email','payment_methods.name as payment_method', 'transactions.service_charge','popsafes.number_of_extend')
        ->leftJoin('popsafes', 'transactions.transaction_id_reference', '=', 'popsafes.id')
        ->leftJoin('deliveries', 'transactions.transaction_id_reference', '=', 'deliveries.id')
        ->leftJoin('payments', 'transactions.id', '=', 'payments.transactions_id')
        ->leftJoin('payment_methods', 'payments.payment_methods_id', '=', 'payment_methods.id')
        ->leftjoin('users as u','transactions.user_id','=','u.id')
        ->leftJoin('campaign_usages as cu', 'cu.transaction_id', '=', 'transactions.id')
        ->leftJoin('campaigns as c', 'c.id', '=', 'cu.campaign_id')
        ->leftJoin('vouchers as v', 'v.id', '=', 'cu.voucher_id')
        ->when($groupName == 'APS', function ($query) use($groupName) {
            return $query->whereRaw("(`popsafes`.`locker_name` LIKE 'DPS %' or `popsafes`.`locker_name` LIKE 'SRG %')")
            ->where('transactions.transaction_type', 'popsafe');
        })
        ->get();
        $totalData = count($allData);

        // kondisi limit ketika show all data
        if($request->input('length') == -1) {
            $limit = $totalData;
        } else {
            $limit = $request->input('length');
        }

        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')) && empty($request->input('columns.1.search.value')) && empty($request->input('columns.2.search.value')) && empty($request->input('columns.3.search.value')) && empty($request->input('columns.6.search.value')) && empty($request->input('columns.7.search.value')) && empty($request->input('columns.10.search.value')) && empty($request->input('columns.11.search.value')) && empty($request->input('columns.12.search.value')) && empty($request->input('columns.17.search.value')) && empty($request->input('columns.21.search.value')))
        {            
            $posts = Transaction::select('transactions.invoice_id','transactions.transaction_type','transactions.description as transaction_description','transactions.status as transaction_status','transactions.created_at as transaction_date','transactions.total_price','transactions.paid_amount','transactions.promo_amount','popsafes.invoice_code as popsafe_code','popsafes.locker_name','v.code','deliveries.invoice_code as delivery_code','deliveries.origin_name','u.name','u.phone','u.member_id','u.email','payment_methods.name as payment_method', 'transactions.service_charge','popsafes.number_of_extend')
            ->leftJoin('popsafes', 'transactions.transaction_id_reference', '=', 'popsafes.id')
            ->leftJoin('deliveries', 'transactions.transaction_id_reference', '=', 'deliveries.id')
            ->leftJoin('payments', 'transactions.id', '=', 'payments.transactions_id')
            ->leftJoin('payment_methods', 'payments.payment_methods_id', '=', 'payment_methods.id')
            ->leftjoin('users as u','transactions.user_id','=','u.id')
            ->leftJoin('campaign_usages as cu', 'cu.transaction_id', '=', 'transactions.id')
            ->leftJoin('campaigns as c', 'c.id', '=', 'cu.campaign_id')
            ->leftJoin('vouchers as v', 'v.id', '=', 'cu.voucher_id')
            ->when($groupName == 'APS', function ($query) use($groupName) {
                return $query->whereRaw("(`popsafes`.`locker_name` LIKE 'DPS %' or `popsafes`.`locker_name` LIKE 'SRG %')")
                ->where('transactions.transaction_type', 'popsafe');
            })
            ->when($startDate != null && $endDate != null, function ($query) use ($startDate, $endDate){
                return $query->whereBetween('popsafes.created_at', [$startDate, $endDate]);
            })
            ->limit($limit)
            ->offset($start)
            ->orderBy($order, $dir)
            ->get();

            $allFiltered = Transaction::select('transactions.invoice_id','transactions.transaction_type','transactions.description as transaction_description','transactions.status as transaction_status','transactions.created_at as transaction_date','transactions.total_price','transactions.paid_amount','transactions.promo_amount','popsafes.invoice_code as popsafe_code','popsafes.locker_name','v.code','deliveries.invoice_code as delivery_code','deliveries.origin_name','u.name','u.phone','u.member_id','u.email','payment_methods.name as payment_method', 'transactions.service_charge','popsafes.number_of_extend')
            ->leftJoin('popsafes', 'transactions.transaction_id_reference', '=', 'popsafes.id')
            ->leftJoin('deliveries', 'transactions.transaction_id_reference', '=', 'deliveries.id')
            ->leftJoin('payments', 'transactions.id', '=', 'payments.transactions_id')
            ->leftJoin('payment_methods', 'payments.payment_methods_id', '=', 'payment_methods.id')
            ->leftjoin('users as u','transactions.user_id','=','u.id')
            ->leftJoin('campaign_usages as cu', 'cu.transaction_id', '=', 'transactions.id')
            ->leftJoin('campaigns as c', 'c.id', '=', 'cu.campaign_id')
            ->leftJoin('vouchers as v', 'v.id', '=', 'cu.voucher_id')
            ->when($groupName == 'APS', function ($query) use($groupName) {
                return $query->whereRaw("(`popsafes`.`locker_name` LIKE 'DPS %' or `popsafes`.`locker_name` LIKE 'SRG %')")
                ->where('transactions.transaction_type', 'popsafe');
            })
            ->when($startDate != null && $endDate != null, function ($query) use ($startDate, $endDate){
                return $query->whereBetween('transactions.created_at', [$startDate, $endDate]);
            })
            ->orderBy($order, $dir)
            ->get();
            $totalFiltered = count($allFiltered);
        }
        else {
            $search = $request->input('search.value'); 

            $posts = Transaction::select('transactions.invoice_id','transactions.transaction_type','transactions.description as transaction_description','transactions.status as transaction_status','transactions.created_at as transaction_date','transactions.total_price','transactions.paid_amount','transactions.promo_amount','popsafes.invoice_code as popsafe_code','popsafes.locker_name','v.code','deliveries.invoice_code as delivery_code','deliveries.origin_name','u.name','u.phone','u.member_id','u.email','u.country','payment_methods.name as payment_method', 'transactions.service_charge','popsafes.number_of_extend')
            ->leftJoin('popsafes', 'transactions.transaction_id_reference', '=', 'popsafes.id')
            ->leftJoin('deliveries', 'transactions.transaction_id_reference', '=', 'deliveries.id')
            ->leftJoin('payments', 'transactions.id', '=', 'payments.transactions_id')
            ->leftJoin('payment_methods', 'payments.payment_methods_id', '=', 'payment_methods.id')
            ->leftjoin('users as u','transactions.user_id','=','u.id')
            ->leftJoin('campaign_usages as cu', 'cu.transaction_id', '=', 'transactions.id')
            ->leftJoin('campaigns as c', 'c.id', '=', 'cu.campaign_id')
            ->leftJoin('vouchers as v', 'v.id', '=', 'cu.voucher_id')
            ->when($groupName == 'APS', function ($query) use($groupName) {
                return $query->whereRaw("(`popsafes`.`locker_name` LIKE 'DPS %' or `popsafes`.`locker_name` LIKE 'SRG %')")
                ->where('transactions.transaction_type', 'popsafe');
            })
            ->when($invoiceId != null, function ($query) use($invoiceId) {
                return $query->where('transactions.invoice_id','LIKE','%'.$invoiceId.'%');
            })
            ->when($transactionType != null, function ($query) use($transactionType) {
                return $query->where('transactions.transaction_type','=', $transactionType);
            })
            ->when($location != null, function ($query) use($location) {
                return $query->where('popsafes.locker_name','=', $location);
            })
            ->when($status != null, function ($query) use($status) {
                return $query->where('transactions.status','=', $status);
            })
            ->when($payment != null, function ($query) use($payment) {
                return $query->where('payment_methods.name','=', $payment);
            })
            ->when($transactionDate, function ($query) use ($startDate, $endDate){
                return $query->whereBetween('transactions.created_at', [$startDate, $endDate]);
            })
            ->when($user != null, function ($query) use($user) {
                return $query->where('u.name', 'LIKE', '%'.$user.'%');
            })
            ->when($phone != null, function ($query) use($phone) {
                return $query->where('u.phone', 'LIKE', '%'.$phone.'%');
            })
            ->when($code != null, function ($query) use($code) {
                return $query->where('v.code', 'LIKE', '%'.$code.'%');
            })
            ->when($country != null, function ($query) use($country) {
                if($country == "Indonesia") {
                    return $query->where('u.country','ID');
                } else {
                    return $query->where('u.country','MY');
                }
            })
            ->when($search != null, function ($query) use ($search){
                return $query->where('transactions.invoice_id', 'LIKE', '%'.$search.'%')
                ->orWhere('transactions.transaction_type', 'LIKE', '%'.$search.'%')
                ->orWhere('u.name', 'LIKE', '%'.$search.'%')
                ->orWhere('u.phone', 'LIKE', '%'.$search.'%')
                ->orWhere('u.email', 'LIKE', '%'.$search.'%')
                ->orWhere('transactions.status', 'LIKE', '%'.$search.'%')
                ->orWhere('payment_methods.name', 'LIKE', '%'.$search.'%')
                ->orWhere('popsafes.locker_name', 'LIKE', '%'.$search.'%')
                ->orWhere('v.code', 'LIKE', '%'.$search.'%');
            })
            ->limit($limit)
            ->offset($start)
            ->orderBy($order,$dir)
            ->get();
            // ->paginate($limit);
            // dd($posts->currentPage());

            $totalFiltered = Transaction::select('transactions.invoice_id','transactions.transaction_type','transactions.description as transaction_description','transactions.status as transaction_status','transactions.created_at as transaction_date','transactions.total_price','transactions.paid_amount','transactions.promo_amount','popsafes.invoice_code as popsafe_code','popsafes.locker_name','v.code','deliveries.invoice_code as delivery_code','deliveries.origin_name','u.name','u.phone','u.member_id','u.email','u.country','payment_methods.name as payment_method', 'transactions.service_charge','popsafes.number_of_extend')
            ->leftJoin('popsafes', 'transactions.transaction_id_reference', '=', 'popsafes.id')
            ->leftJoin('deliveries', 'transactions.transaction_id_reference', '=', 'deliveries.id')
            ->leftJoin('payments', 'transactions.id', '=', 'payments.transactions_id')
            ->leftJoin('payment_methods', 'payments.payment_methods_id', '=', 'payment_methods.id')
            ->leftjoin('users as u','transactions.user_id','=','u.id')
            ->leftJoin('campaign_usages as cu', 'cu.transaction_id', '=', 'transactions.id')
            ->leftJoin('campaigns as c', 'c.id', '=', 'cu.campaign_id')
            ->leftJoin('vouchers as v', 'v.id', '=', 'cu.voucher_id')
            ->when($groupName == 'APS', function ($query) use($groupName) {
                return $query->whereRaw("(`popsafes`.`locker_name` LIKE 'DPS %' or `popsafes`.`locker_name` LIKE 'SRG %')")
                ->where('transactions.transaction_type', 'popsafe');
            })
            ->when($invoiceId != null, function ($query) use($invoiceId) {
                return $query->where('transactions.invoice_id','LIKE','%'.$invoiceId.'%');
            })
            ->when($transactionType != null, function ($query) use($transactionType) {
                return $query->where('transactions.transaction_type','=', $transactionType);
            })
            ->when($location != null, function ($query) use($location) {
                return $query->where('popsafes.locker_name','=', $location);
            })
            ->when($status != null, function ($query) use($status) {
                return $query->where('transactions.status','=', $status);
            })
            ->when($payment != null, function ($query) use($payment) {
                return $query->where('payment_methods.name','=', $payment);
            })
            ->when($transactionDate, function ($query) use ($startDate, $endDate){
                return $query->whereBetween('transactions.created_at', [$startDate, $endDate]);
            })
            ->when($user != null, function ($query) use($user) {
                return $query->where('u.name', 'LIKE', '%'.$user.'%');
            })
            ->when($phone != null, function ($query) use($phone) {
                return $query->where('u.phone', 'LIKE', '%'.$phone.'%');
            })
            ->when($code != null, function ($query) use($code) {
                return $query->where('v.code', 'LIKE', '%'.$code.'%');
            })
            ->when($country != null, function ($query) use($country) {
                if($country == "Indonesia") {
                    return $query->where('u.country','ID');
                } else {
                    return $query->where('u.country','MY');
                }
            })
            ->when($search != null, function ($query) use ($search){
                return $query->where('transactions.invoice_id', 'LIKE', '%'.$search.'%')
                ->orWhere('transactions.transaction_type', 'LIKE', '%'.$search.'%')
                ->orWhere('u.name', 'LIKE', '%'.$search.'%')
                ->orWhere('u.phone', 'LIKE', '%'.$search.'%')
                ->orWhere('u.email', 'LIKE', '%'.$search.'%')
                ->orWhere('transactions.status', 'LIKE', '%'.$search.'%')
                ->orWhere('payment_methods.name', 'LIKE', '%'.$search.'%')
                ->orWhere('popsafes.locker_name', 'LIKE', '%'.$search.'%')
                ->orWhere('v.code', 'LIKE', '%'.$search.'%');
            })
            ->count();
        }

        $data = array();
        if(!empty($posts))
        {
            $no = 1;
            foreach ($posts as $post)
            {
                // check transaction type
                if($post->transaction_type == 'popsafe') {
                    $url = url('popsafe/detail')."/".$post->popsafe_code."/".$post->invoice_id;
                    $nestedData['popsafe_code'] = $post->popsafe_code;
                    $nestedData['location'] = $post->locker_name;
                    $nestedData['counter'] = $post->number_of_extend;
                } elseif ($post->transaction_type == 'delivery') {
                    $url = url('delivery/detail')."/".$post->delivery_code."/".$post->invoice_id;
                    $nestedData['popsafe_code'] = null;
                    $nestedData['location'] = $post->origin_name;
                    $nestedData['counter'] = null;
                } else {
                    $url = url('transaction/detail')."/".$post->invoice_id;
                    $nestedData['popsafe_code'] = null;
                    $nestedData['location'] = null;
                    $nestedData['counter'] = null;
                }
                $nestedData['url'] = $url;
                $nestedData['invoice_id'] = $post->invoice_id;
                $nestedData['transaction_type'] = $post->transaction_type;
                $nestedData['name'] = $post->name;
                $nestedData['phone'] = $post->phone;
                $nestedData['email'] = $post->email;
                $nestedData['member_id'] = $post->member_id;
                $nestedData['transaction_description'] = $post->transaction_description;
                $nestedData['transaction_status'] = $post->transaction_status;
                $nestedData['transaction_date'] = $post->transaction_date;
                $nestedData['total_price'] = $post->total_price;
                $nestedData['promo_amount'] = $post->promo_amount;
                $nestedData['paid_amount'] = $post->paid_amount;
                $nestedData['code'] = $post->code;
                $nestedData['payment'] = $post->payment_method;
                $nestedData['service_charge'] = $post->service_charge;
                $nestedData['no'] = $no++;
                $nestedData['country'] = $post->country;

                $data[] = $nestedData;
            }
        }
          
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );
            
        echo json_encode($json_data); 
    }

    public function getDetail(Request $request, $invoice_id)
    {
        $data = Transaction::getByInvoiceId($invoice_id);
        if($data->transaction_id_reference == 0) {
            // topup, refund, deduct from dashboard
            $payment = $data;
            $histories = TransactionHistory::getByTransactionId($payment->id);
        } else {
            if($data->transaction_type == 'topup') {
                $payment = PaymentTransaction::getByInvoiceId($invoice_id);
                $histories = PaymentTransactionHistory::getByPaymentTransactionId($payment->id);
            } else {
                // transaction_type = refund
                $transaction = Transaction::checkTransactionIdReference($data->transaction_id_reference);
                // get refund transaction
                $payment = $data;
                
                if($transaction->transaction_type == 'popsafe') {
                    $histories = PopsafeHistory::getByPopsafeId($data->popsafe_id);
                } else if($transaction->transaction_type == 'delivery') {
                    $histories = Transaction::getByDeliveryId($payment->transaction_id_reference);
                } else {
                    $histories = TransactionHistory::getByTransactionId($payment->id);
                }
            }
        }
        // dd($data, $histories);
        return view('popapps.transactions.detail', compact('data','payment','histories'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function refundTransaction(Request $request)
    {
        // validation
        $rules = [
            'invoiceId' => 'required|exists:popsend.transactions,invoice_id'
        ];

        $this->validate($request,$rules);

        $invoice_id = $request->input('invoiceId');
        $remarks = $request->input('remarks',null);

        // get transaction
        $transaction = Transaction::getByInvoiceId($invoice_id);
        if (!$transaction){
            $request->session()->flash('error','Invalid Transaction Invoice ID');
            return back();
        }
        $transaction_id = $transaction->id;
        
        // refund rules
        $allowedRefund = ['delivery','popsafe'];
        // get transaction type
        $transactionType = $transaction->transaction_type;
        $transactionStatus = $transaction->status;
        if (!in_array($transactionType,$allowedRefund)){
            $request->session()->flash('error','Transaction Not Allowed to Refund');
            return back();
        }
        if ($transactionStatus!='PAID'){
            $request->session()->flash('error','Transaction Not PAID. Status: '.$transactionStatus);
            return back();
        }

        // kalo transaction bisa di model ga sih naronya?
        DB::connection('popsend')->beginTransaction();
        if ($transactionType == 'popsafe'){
            $transactionModel = new Transaction();
            $refundTransaction = $transactionModel->refundTransaction($transaction_id,$remarks);
            if (!$refundTransaction->isSuccess){
                DB::connection('popsend')->rollback();
                $request->session()->flash('error',$refundTransaction->errorMsg);
                return back();
            }

            $popSafeModel = new Popsafe();
            $cancelPopSafe = $popSafeModel->cancelOrder($transaction_id,$remarks);
            if (!$cancelPopSafe->isSuccess){
                DB::connection('popsend')->rollback();
                $request->session()->flash('error',$cancelPopSafe->errorMsg);
                return back();
            }
            $request->session()->flash('success','Success Refund');
        }
        elseif ( $transactionType == 'delivery'){
            // cancel first
            $deliveryModel = new Delivery();
            $cancelDelivery = $deliveryModel->cancelOrder($transaction_id,$remarks);
            if (!$cancelDelivery->isSuccess){
                DB::connection('popsend')->rollback();
                $request->session()->flash('error',$cancelDelivery->errorMsg);
                return back();
            }
            $refundAmount = $cancelDelivery->refundAmount;
            $refundList = $cancelDelivery->refundList;
            $totalDestinations = $cancelDelivery->totalDestinations;

            if ($refundAmount == 0){
                DB::connection('popsend')->rollback();
                $failedRefund = "";
                foreach ($refundList as $item) {
                    $failedRefund .= "$item->subInvoice - $item->errorMsg, ";
                }
                $request->session()->flash('error','Failed Refund '.$failedRefund);
                return back();
            }

            $transactionModel = new Transaction();
            $refundDelivery = $transactionModel->refundDelivery($transaction_id,$refundAmount,$refundList,$remarks);
            if (!$refundDelivery->isSuccess){
                DB::connection('popsend')->rollback();
                $request->session()->flash('error',$refundDelivery->errorMsg);
                return back();
            }
            $successRefund = "";
            $failedRefund = "";

            foreach ($refundList as $item) {
                if ($item->isRefund){
                    $successRefund .= "$item->subInvoice, ";
                } else {
                    $failedRefund .= "$item->subInvoice - $item->errorMsg";
                }
            }

            $request->session()->flash('success','Success Refund '.$successRefund);
            $request->session()->flash('error','Failed Refund '.$failedRefund);
        }
        else {
            $request->session()->flash('error','Invalid Type');
            DB::connection('popsend')->rollback();
        }

        DB::connection('popsend')->commit();

        return back();
    }

    public function downloadExcel (Request $request, $invoiceId = null)
    {
        // $fileContents = Storage::disk('local')->get($pathToTheFile);
        // $response = Response::make($fileContents, 200);
        // $response->header('Content-Type', Storage::disk('local')->mimeType($pathToTheFile));
        // return $response;
        $parameter = $request->route()->parameters();
        // param
        $invoiceId = $request->input('transactionId', null);
        $transactionType = $request->input('transactionType', null);
        $transactionDate = $request->input('transactionDate', null);
        $startDate = '';
        $endDate = '';
        $user = $request->input('user', null);
        $phone = $request->input('phone', null);
        $location = $request->input('location', null);
        $status = $request->input('status', null);
        $payment = $request->input('paymentMethod', null);

        if (!empty($transactionDate)) {
            $date = explode(" - ", $transactionDate);
            $startDate = $date[0]." 00:00:00";
            $endDate = $date[1]." 23:59:59";
            $start = date("d M Y", strtotime($date[0]));
            $end = date("d M Y", strtotime($date[1]));
        }
        
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        // style
        $bold = [
            'font' => [
                'bold' => true,
                'size' => 20
            ]
        ];
        
        // title
        $sheet->mergeCells('A1:E1');
        $sheet->setCellValue('A1', 'Transaction List');
        $sheet->getStyle('A1')->applyFromArray($bold);
        $sheet->mergeCells('A2:E2');
        if (!empty($transactionDate)) {
        $sheet->setCellValue('A2', $start.' - '.$end);
        }
        // header
        $count = 4;
        $sheet->setCellValue('A'.$count, 'No');
        $sheet->setCellValue('B'.$count, 'Transaction ID');
        $sheet->setCellValue('C'.$count, 'Transaction Type');
        $sheet->setCellValue('D'.$count, 'Transaction Date');
        $sheet->setCellValue('E'.$count, 'Order ID');
        $sheet->setCellValue('F'.$count, 'Name');
        $sheet->setCellValue('G'.$count, 'Phone');
        $sheet->setCellValue('H'.$count, 'Email');
        $sheet->setCellValue('I'.$count, 'Description');
        $sheet->setCellValue('J'.$count, 'Location');
        $sheet->setCellValue('K'.$count, 'Status');
        $sheet->setCellValue('L'.$count, 'Payment');
        $sheet->setCellValue('M'.$count, 'Payment ID');
        $sheet->setCellValue('N'.$count, 'Counter');
        $sheet->setCellValue('O'.$count, 'Price');
        $sheet->setCellValue('P'.$count, 'MDR');
        $sheet->setCellValue('Q'.$count, 'Revenue Sharing');
        $sheet->setCellValue('R'.$count, 'Settlement Amount');

        $data = Transaction::select('transactions.invoice_id','transactions.transaction_type','transactions.description as transaction_description','transactions.status as transaction_status','transactions.created_at as transaction_date','transactions.total_price','transactions.paid_amount','transactions.promo_amount','popsafes.invoice_code as popsafe_code','popsafes.locker_name','u.name','u.phone','u.member_id','u.email','payment_methods.name as payment_method', 'transactions.service_charge','popsafes.number_of_extend','payment_transactions.payment_reference')
        ->leftJoin('popsafes', 'transactions.transaction_id_reference', '=', 'popsafes.id')
        // ->leftJoin('deliveries', 'transactions.transaction_id_reference', '=', 'deliveries.id')
        ->leftJoin('payments', 'transactions.id', '=', 'payments.transactions_id')
        ->leftJoin('payment_methods', 'payments.payment_methods_id', '=', 'payment_methods.id')
        ->leftJoin('payment_transactions', 'payments.payment_transactions_id', '=', 'payment_transactions.id')
        ->leftjoin('users as u','transactions.user_id','=','u.id')
        // ->leftJoin('campaign_usages as cu', 'cu.transaction_id', '=', 'transactions.id')
        // ->leftJoin('campaigns as c', 'c.id', '=', 'cu.campaign_id')
        // ->leftJoin('vouchers as v', 'v.id', '=', 'cu.voucher_id')
        ->when($parameter['groupName'] == 'APS', function ($query) {
            return $query->whereRaw("(`popsafes`.`locker_name` LIKE 'DPS %' or `popsafes`.`locker_name` LIKE 'SRG %')")
            ->where('transactions.transaction_type', 'popsafe');
        })
        ->when($invoiceId != null, function ($query) use($invoiceId) {
            return $query->where('transactions.invoice_id','LIKE','%'.$invoiceId.'%');
        })
        ->when($transactionType != null, function ($query) use($transactionType) {
            return $query->where('transactions.transaction_type','=', $transactionType);
        })
        ->when($location != null, function ($query) use($location) {
            return $query->where('popsafes.locker_name','=', $location);
        })
        ->when($status != null, function ($query) use($status) {
            return $query->where('transactions.status','=', $status);
        })
        ->when($payment != null, function ($query) use($payment) {
            return $query->where('payment_methods.id','=', $payment);
        })
        ->when($transactionDate != null , function ($query) use ($startDate, $endDate){
            return $query->whereBetween('transactions.created_at', [$startDate, $endDate]);
        })
        ->when($user != null, function ($query) use($user) {
            return $query->where('u.name', 'LIKE', '%'.$user.'%');
        })
        ->when($phone != null, function ($query) use($phone) {
            return $query->where('u.phone', 'LIKE', '%'.$phone.'%');
        })
        // belum ditambahin kondisi search by promo code dan country
        ->orderBy('transactions.created_at','desc')
        ->get();
        // dd($data);
        if($data->isEmpty()) {
            // data kosong
            $count = $count+1;
            $sheet->mergeCells('A'.$count.':Q'.$count);
            $sheet->setCellValue('A'.$count, 'No records found');
        } else { 
            // data ready
            $no = 1;
            foreach($data as $key => $row) {
                $count = $count+1;
                if($row['service_charge'] == null) {
                    $mdr = '-';
                    $revenue = '-';
                    $settlement = '-';
                } else {
                    $mdr = $row['service_charge'];
                    $revenue = round(($row['total_price'] - $row['service_charge'])*5/100);
                    $settlement = $row['total_price'] - $row['service_charge'] - $revenue;
                }
                if($row['transaction_type'] != 'popsafe' && $row['transaction_type'] != 'delivery') {
                    $locker_name = '';
                } else {
                    $locker_name = $row['locker_name'];
                }

                $sheet->setCellValue('A'.$count, $no++);
                $sheet->setCellValue('B'.$count, $row['invoice_id']);
                $sheet->setCellValue('C'.$count, $row['transaction_type']);
                $sheet->setCellValue('D'.$count, $row['transaction_date']);
                $sheet->setCellValue('E'.$count, $row['popsafe_code']);
                $sheet->setCellValue('F'.$count, $row['name']);
                $sheet->setCellValue('G'.$count, $row['phone']);
                $sheet->setCellValue('H'.$count, $row['email']);
                $sheet->setCellValue('I'.$count, $row['transaction_description']);
                $sheet->setCellValue('J'.$count, $locker_name);
                $sheet->setCellValue('K'.$count, $row['transaction_status']);
                $sheet->setCellValue('L'.$count, $row['payment_method']);
                $sheet->setCellValue('M'.$count, $row['payment_reference']);
                $sheet->setCellValue('N'.$count, $row['number_of_extend']);
                $sheet->setCellValue('O'.$count, $row['total_price']);
                $sheet->setCellValue('P'.$count, $mdr);
                $sheet->setCellValue('Q'.$count, $revenue);
                $sheet->setCellValue('R'.$count, $settlement);
            }
        }

        $dateNow = date("ymd");
        $filename = $dateNow.' Transaction List.xlsx';
        
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xlsx");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'.$filename.'"');
        $writer->save("php://output");
    }

    public function getAjaxUser(Request $request)
    {
        $parameter = $request->route()->parameters();
        $groupName = $parameter['groupName'];
        // param
        $invoiceId = $request->input('transactionId', null);
        if(!empty($request->input('columns.1.search.value'))) {
            $invoiceId = $request->input('columns.1.search.value');
        }
        $transactionType = $request->input('transactionType', null);
        if(!empty($request->input('columns.2.search.value'))) {
            $transactionType = $request->input('columns.2.search.value');
        }
        $transactionDate = $request->input('transactionDate', null);
        if(!empty($request->input('columns.3.search.value'))) {
            $transactionDate = $request->input('columns.3.search.value');
        }
        $location = $request->input('location', null);
        if(!empty($request->input('columns.10.search.value'))) {
            $location = $request->input('columns.10.search.value');
        }
        $status = $request->input('status', null);
        if(!empty($request->input('columns.11.search.value'))) {
            $status = $request->input('columns.11.search.value');
        }
        $payment = $request->input('paymentMethod', null);
        if(!empty($request->input('columns.12.search.value'))) {
            $payment = $request->input('columns.12.search.value');
        }
        
        $startDate = null;
        $endDate = null;
        if (!empty($transactionDate)) {
            $date = explode(" - ", $transactionDate);
            $startDate = $date[0];
            $endDate = $date[1];
        }
        
        $allData = Transaction::select('u.name')
        ->leftJoin('popsafes', 'transactions.transaction_id_reference', '=', 'popsafes.id')
        ->leftJoin('deliveries', 'transactions.transaction_id_reference', '=', 'deliveries.id')
        ->leftJoin('payments', 'transactions.id', '=', 'payments.transactions_id')
        ->leftJoin('payment_methods', 'payments.payment_methods_id', '=', 'payment_methods.id')
        ->leftjoin('users as u','transactions.user_id','=','u.id')
        ->leftJoin('campaign_usages as cu', 'cu.transaction_id', '=', 'transactions.id')
        ->leftJoin('campaigns as c', 'c.id', '=', 'cu.campaign_id')
        ->leftJoin('vouchers as v', 'v.id', '=', 'cu.voucher_id')
        ->when($groupName == 'APS', function ($query) use($groupName) {
            return $query->whereRaw("(`popsafes`.`locker_name` LIKE 'DPS %' or `popsafes`.`locker_name` LIKE 'SRG %')")
            ->where('transactions.transaction_type', 'popsafe');
        })
        ->when($invoiceId != null, function ($query) use($invoiceId) {
            return $query->where('popsafes.invoice_code','LIKE','%'.$invoiceId.'%');
        })
        ->when($status != null, function ($query) use($status) {
            return $query->where('popsafes.status', $status);
        })
        ->when($location != null, function ($query) use($location) {
            return $query->where('popsafes.locker_name', 'LIKE','%'.$location.'%');
        })
        ->when($transactionDate, function ($query) use ($startDate, $endDate){
            return $query->whereBetween('popsafes.created_at', [$startDate, $endDate]);
        })
        ->groupBy('u.name')
        ->get();

        $user = array();
        if(!empty($allData))
        {
            foreach ($allData as $post)
            {
                $user[] = $post->name;
            }
        }
        
        echo json_encode($user);
    }
}
