<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
class AuditResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'module'=>$this->module,
            'key'=>$this->key,
            'type'=>ucfirst($this->type),
            'before'=>$this->before,
            'after'=>$this->after,
            'user'=>$this->username,
            'created_at'=>$this->created_at->toDateTimeString(),
        ];
    }
}
